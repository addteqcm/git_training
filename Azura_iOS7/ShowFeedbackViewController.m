//
//  ShowFeedbackViewController.m
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/22/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "ShowFeedbackViewController.h"
@interface ShowFeedbackViewController ()

@end

@implementation ShowFeedbackViewController

@synthesize leftone;
@synthesize lefttwo;
@synthesize rightone;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    leftone = false;
    lefttwo = false;
    rightone = false;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)moveLeftPanel:(id)sender {
    
    if(leftone==false){
        leftone =true;
        CGRect mframe = self.mainsubview.frame;
        mframe.origin.x=130;
        self.mainsubview.frame = mframe;
    
        CGRect tframe = self.maintableview.frame;
        tframe.origin.x=130;
        self.maintableview.frame = tframe;
        
        self.left = [[FeedbackLeftPanelViewController alloc]initWithNibName:@"FeedbackLeftPanelViewController" bundle:nil];
        self.left.view.frame = CGRectMake(self.mainview.frame.origin.x, self.maintableview.frame.origin.y, 130, self.mainview.frame.size.height);
        [self.mainview addSubview:self.left.view];
    
    }else{
        
    }

}

- (IBAction)moveRightPanel:(id)sender {
    
    if(leftone == false){
        
    }else{
        
    
        CGRect mframe = self.mainsubview.frame;
        mframe.origin.x=0;
        self.mainsubview.frame = mframe;
    
        CGRect tframe = self.maintableview.frame;
        tframe.origin.x=0;
        self.maintableview.frame = tframe;
    
    
        [self.left.view removeFromSuperview];
        leftone=false;
    }
}
@end
