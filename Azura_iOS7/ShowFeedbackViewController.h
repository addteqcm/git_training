//
//  ShowFeedbackViewController.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/22/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FeedbackLeftPanelViewController.h"

@interface ShowFeedbackViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *mainview;
@property (strong, nonatomic) IBOutlet UITableView *maintableview;
@property (strong, nonatomic) IBOutlet UIView *mainsubview;
@property (strong, nonatomic) FeedbackLeftPanelViewController *left;

@property BOOL leftone;
@property BOOL lefttwo;
@property BOOL rightone;

- (IBAction)moveLeftPanel:(id)sender;
- (IBAction)moveRightPanel:(id)sender;

@end
