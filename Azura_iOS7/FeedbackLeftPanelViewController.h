//
//  FeedbackLeftPanelViewController.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/29/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackLeftPanelViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
