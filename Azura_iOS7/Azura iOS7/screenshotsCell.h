//
//  screenshotsCell.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 1/15/14.
//  Copyright (c) 2014 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface screenshotsCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *middleImage;
@end
