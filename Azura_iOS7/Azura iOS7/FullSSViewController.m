//
//  FullSSViewController.m
//  Azura iOS7
//
//  Created by HeeJinChoi on 3/13/14.
//  Copyright (c) 2014 addteq. All rights reserved.
//

#import "FullSSViewController.h"

@interface FullSSViewController ()
@property (nonatomic) int current;
@end

@implementation FullSSViewController
@synthesize current;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIImage *originImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.openImage]]];
    NSLog(@"image url %@", self.openImage);
    [self.fullImage setImage:originImage];
    
    for(int i=0; i<[self.imageLists count]; i++){
        NSString *tmp =[self.imageLists objectAtIndex:i];
        if([tmp isEqualToString:self.openImage]){
            current = i;
            i = [self.imageLists count];
        }
    }
    
    
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)swipeLeft:(id)sender {
    int tmp = current+1;
    if(tmp < [self.imageLists count]){
        self.openImage = [self.imageLists objectAtIndex:tmp];
        UIImage *originImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.openImage]]];
        
        [self.fullImage setImage:originImage];
        current = tmp;
    }
    
}

- (IBAction)swipeRight:(id)sender {
    
    int tmp = current-1;
    if(tmp>=0){
        self.openImage = [self.imageLists objectAtIndex:tmp];
        UIImage *originImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.openImage]]];
        [self.fullImage setImage:originImage];
        current = tmp;
    }else{
        NSLog(@"%d", tmp);
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}
@end
