
#import "ACEDrawingTools.h"

typedef enum {
    ACEDrawingToolTypePen,
    ACEDrawingToolTypeLine,
    ACEDrawingToolTypeRectagleStroke,
    ACEDrawingToolTypeRectagleFill,
    ACEDrawingToolTypeEllipseStroke,
    ACEDrawingToolTypeEllipseFill,
    ACEDrawingToolTypeArrow,
    ACEDrawingArrowToolTypeText,
    ACEDrawingToolTypeCrop,
    ACEDRawingToolTypeMove
} ACEDrawingToolType;

CGRect makeFrameFromPoints(CGPoint start, CGPoint end){
    
    CGFloat originx;
    CGFloat originy;
    CGFloat width;
    CGFloat height;
    if(start.x < end.x){
        width = end.x - start.x;
        originx = start.x;
    }else{
        
        width = start.x - end.x;
        originx = end.x;
        if(width==0){
            width = 5.0f;
        }
    }
    
    if(start.y < end.y){
        height =end.y - start.y;
        originy = start.y;
    }else{
        height = start.y - end.y;
        originy = end.y;
        if(height==0){
            height = 5.0f;
        }
    }
    
    return CGRectMake(originx, originy, width, height);
}

CGPoint midPoint(CGPoint p1, CGPoint p2)
{
    return CGPointMake((p1.x + p2.x) * 0.5, (p1.y + p2.y) * 0.5);
}

#pragma mark - ACEDrawingPenTool

@implementation ACEDrawingPenTool
@synthesize prevImage;
@synthesize lineColor = _lineColor;
@synthesize lineAlpha = _lineAlpha;
@synthesize toolFrame;

@synthesize type;
@synthesize moveArray;
@synthesize begin;
@synthesize max;
@synthesize min;

- (id)init
{
    self = [super init];
    if (self != nil) {
        self.lineCapStyle = kCGLineCapRound;
        self.begin = true;
        self.moveArray = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)setInitialPoint:(CGPoint)firstPoint
{
    [self removeAllPoints];
    [self moveToPoint:firstPoint];
    max = firstPoint;
    min = firstPoint;
}

- (void)moveFromPoint:(CGPoint)startPoint toPoint:(CGPoint)endPoint
{
    [self addQuadCurveToPoint:midPoint(endPoint, startPoint) controlPoint:startPoint];
    if(self.begin){
        [self.moveArray addObject:[NSValue valueWithCGPoint:startPoint]];
        [self.moveArray addObject:[NSValue valueWithCGPoint:endPoint]];
    }
    [self maxAndMinStartPoint:startPoint endPoint:endPoint];
    self.toolFrame = makeFrameFromPoints(min, max);
}

// calculating frame size for drag-and-drop function
-(void) maxAndMinStartPoint:(CGPoint) s endPoint:(CGPoint) e{
    if(s.x < e.x){
        if(s.x < min.x){
            min.x = s.x;
        }
        if(e.x > max.x){
            max.x = e.x;
        }
    }else{
        if(e.x < min.x){
            min.x = e.x;
        }
        if(s.x > max.x){
            max.x = s.x;
        }
    }
    if(s.y < e.y){
        if(s.y < min.y){
            min.y = s.y;
        }
        if(e.y > max.y){
            max.y = e.y;
        }
    }else{
        if(e.y < min.y){
            min.y = e.y;
        }
        if(s.y >max.y){
            max.y = s.y;
        }
    }
}

- (void)draw
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGSize          myShadowOffset = CGSizeMake (3,  3);// 2
    CGContextSetShadow(context, myShadowOffset, 3);
    
    
    
    // set the line properties
   
    
    
    
    
    
    
    [self.lineColor setStroke];
    [self strokeWithBlendMode:kCGBlendModeNormal alpha:self.lineAlpha];
    
    
}


#if !ACE_HAS_ARC

- (void)dealloc
{
    self.lineColor = nil;
    [super dealloc];
}

#endif

@end



#pragma mark - ACEDrawingLineTool

@interface ACEDrawingLineTool ()
@property (nonatomic, assign) CGPoint firstPoint;
@property (nonatomic, assign) CGPoint lastPoint;
@end

#pragma mark -

@implementation ACEDrawingLineTool
@synthesize prevImage;
@synthesize toolFrame;
@synthesize lineColor = _lineColor;
@synthesize lineAlpha = _lineAlpha;
@synthesize lineWidth = _lineWidth;
@synthesize begin;
@synthesize type;
@synthesize moveArray;

- (id) init{
    
    self = [super init];
    if (self != nil) {
        self.moveArray = [[NSMutableArray alloc] init];
        self.begin = true;
    }
    
    
    return self;
}

- (void)setInitialPoint:(CGPoint)firstPoint
{
    self.firstPoint = firstPoint;
}

- (void)moveFromPoint:(CGPoint)startPoint toPoint:(CGPoint)endPoint
{
    self.lastPoint = endPoint;
    if(self.begin){
        [self.moveArray addObject:[NSValue valueWithCGPoint:startPoint]];
        [self.moveArray addObject:[NSValue valueWithCGPoint:endPoint]];
    }
    
    self.toolFrame = makeFrameFromPoints(self.firstPoint, self.lastPoint);
    
}

- (void)draw
{
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGSize          myShadowOffset = CGSizeMake (3,  3);// 2
    CGContextSetShadow(context, myShadowOffset, 3);

    
    // set the line properties
    CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineWidth(context, self.lineWidth);
    CGContextSetAlpha(context, self.lineAlpha);
    //NSLog(@"width of line %f",self.lineWidth);
    // draw the line
    CGContextMoveToPoint(context, self.firstPoint.x, self.firstPoint.y);
    CGContextAddLineToPoint(context, self.lastPoint.x, self.lastPoint.y);
    CGContextStrokePath(context);
}

#if !ACE_HAS_ARC

- (void)dealloc
{
    self.lineColor = nil;
    [super dealloc];
}

#endif

@end

#pragma mark - ACEDrawingRectangleTool

@interface ACEDrawingRectangleTool ()
@property (nonatomic, assign) CGPoint firstPoint;
@property (nonatomic, assign) CGPoint lastPoint;
@end

#pragma mark -

@implementation ACEDrawingRectangleTool
@synthesize prevImage;
@synthesize toolFrame;
@synthesize lineColor = _lineColor;
@synthesize lineAlpha = _lineAlpha;
@synthesize lineWidth = _lineWidth;

@synthesize begin;
@synthesize type;
@synthesize moveArray;

- (id) init{
    
    self = [super init];
    if (self != nil) {
        self.moveArray = [[NSMutableArray alloc] init];
        self.begin = true;
    }
    
    
    return self;
}


- (void)setInitialPoint:(CGPoint)firstPoint
{
    self.firstPoint = firstPoint;
}

- (void)moveFromPoint:(CGPoint)startPoint toPoint:(CGPoint)endPoint
{
    self.lastPoint = endPoint;
    if(self.begin){
        [self.moveArray addObject:[NSValue valueWithCGPoint:startPoint]];
        [self.moveArray addObject:[NSValue valueWithCGPoint:endPoint]];
    }
    self.toolFrame = makeFrameFromPoints(self.firstPoint, self.lastPoint);

}

- (void)draw
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGSize          myShadowOffset = CGSizeMake (3,  3);// 2
    CGContextSetShadow(context, myShadowOffset, 3);
    // set the properties
    CGContextSetAlpha(context, self.lineAlpha);
    
    // draw the rectangle
    CGRect rectToFill = CGRectMake(self.firstPoint.x, self.firstPoint.y, self.lastPoint.x - self.firstPoint.x, self.lastPoint.y - self.firstPoint.y);    
    if (self.fill) {
        CGContextSetFillColorWithColor(context, self.lineColor.CGColor);
        CGContextFillRect(UIGraphicsGetCurrentContext(), rectToFill);
        
    } else {
        CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
        CGContextSetLineWidth(context, self.lineWidth);
        CGContextStrokeRect(UIGraphicsGetCurrentContext(), rectToFill);        
    }
}

#if !ACE_HAS_ARC

- (void)dealloc
{
    self.lineColor = nil;
    [super dealloc];
}

#endif

@end

#pragma mark - ACEDrawingEllipseTool

@interface ACEDrawingEllipseTool ()
@property (nonatomic, assign) CGPoint firstPoint;
@property (nonatomic, assign) CGPoint lastPoint;
@end

#pragma mark -

@implementation ACEDrawingEllipseTool
@synthesize prevImage;
@synthesize toolFrame;
@synthesize lineColor = _lineColor;
@synthesize lineAlpha = _lineAlpha;
@synthesize lineWidth = _lineWidth;

@synthesize begin;
@synthesize type;
@synthesize moveArray;

-(id) init{
    self = [super init];
    if(self !=nil){
        self.begin = true;
        self.moveArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)setInitialPoint:(CGPoint)firstPoint
{
    self.firstPoint = firstPoint;
}

- (void)moveFromPoint:(CGPoint)startPoint toPoint:(CGPoint)endPoint
{
    self.lastPoint = endPoint;
    if(self.begin){
        [self.moveArray addObject:[NSValue valueWithCGPoint:startPoint]];
        [self.moveArray addObject:[NSValue valueWithCGPoint:endPoint]];
    }
    self.toolFrame = makeFrameFromPoints(self.firstPoint, self.lastPoint);

}

- (void)draw
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGSize          myShadowOffset = CGSizeMake (3,  3);// 2
    CGContextSetShadow(context, myShadowOffset, 3);
    
    // set the properties
    CGContextSetAlpha(context, self.lineAlpha);
    
    // draw the ellipse
    CGRect rectToFill = CGRectMake(self.firstPoint.x, self.firstPoint.y, self.lastPoint.x - self.firstPoint.x, self.lastPoint.y - self.firstPoint.y);
    if (self.fill) {
        CGContextSetFillColorWithColor(context, self.lineColor.CGColor);
        CGContextFillEllipseInRect(UIGraphicsGetCurrentContext(), rectToFill);
        
    } else {
        CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
        CGContextSetLineWidth(context, self.lineWidth);
        CGContextStrokeEllipseInRect(UIGraphicsGetCurrentContext(), rectToFill);
    }
}

#if !ACE_HAS_ARC

- (void)dealloc
{
    self.lineColor = nil;
    [super dealloc];
}

#endif

@end


#pragma mark - ACEDrawingArrowTool
@interface ACEDrawingArrowTool ()
@property (nonatomic, assign) CGPoint firstPoint;
@property (nonatomic, assign) CGPoint lastPoint;
@end

#pragma mark -

@implementation ACEDrawingArrowTool
@synthesize lineColor = _lineColor;
@synthesize lineAlpha = _lineAlpha;
@synthesize lineWidth = _lineWidth;
@synthesize prevImage;
@synthesize toolFrame;
@synthesize begin;
@synthesize type;
@synthesize moveArray;

-(id) init{
    self = [super init];
    if(self !=nil){
        self.begin = true;
        self.moveArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)setInitialPoint:(CGPoint)firstPoint
{
    self.firstPoint = firstPoint;
}

- (void)moveFromPoint:(CGPoint)startPoint toPoint:(CGPoint)endPoint
{
    self.lastPoint = endPoint;
    if(self.begin){
        [self.moveArray addObject:[NSValue valueWithCGPoint:startPoint]];
        [self.moveArray addObject:[NSValue valueWithCGPoint:endPoint]];
    }
    self.toolFrame = makeFrameFromPoints(self.firstPoint, self.lastPoint);

}

- (void) draw
{
    
    double slopy, cosy, siny;
    // Arrow size
    double length = 10.0;
    double width = self.lineWidth;
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGSize          myShadowOffset = CGSizeMake (3,  3);// 2
    CGContextSetShadow(context, myShadowOffset, 3);
    
    slopy = atan2((self.firstPoint.y - self.lastPoint.y), (self.firstPoint.x - self.lastPoint.x));
    cosy = cos(slopy);
    siny = sin(slopy);
    
    //draw a line between the 2 endpoint
    CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
    CGContextSetLineCap(context, kCGLineCapSquare);
    CGContextSetLineWidth(context, self.lineWidth);
    CGContextSetAlpha(context, self.lineAlpha);
    CGContextMoveToPoint(context, self.firstPoint.x - length * cosy, self.firstPoint.y - length * siny );
    CGContextAddLineToPoint(context, self.lastPoint.x + length * cosy, self.lastPoint.y + length * siny);
    
    //paints a line along the current path
    CGContextStrokePath(context);
    
   
    //a total of 6 lines drawn to make the arrow shape
//    CGContextMoveToPoint(context, self.firstPoint.x, self.firstPoint.y);
//    CGContextAddLineToPoint(context,
//                            self.firstPoint.x + ( - length * cosy - ( width / 2.0 * siny )),
//                            self.firstPoint.y + ( - length * siny + ( width / 2.0 * cosy )));
//    CGContextAddLineToPoint(context,
//                            self.firstPoint.x + (- length * cosy + ( width / 2.0 * siny )),
//                            self.firstPoint.y - (width / 2.0 * cosy + length * siny ) );
//    CGContextClosePath(context);
//    CGContextStrokePath(context);
    
    /*/-------------similarly the the other end-------------/*/
    
    CGContextMoveToPoint(context, self.lastPoint.x, self.lastPoint.y);
    CGContextAddLineToPoint(context,
                            self.lastPoint.x +  (length * cosy - ( width / 2.0 * siny )),
                            self.lastPoint.y +  (length * siny + ( width / 2.0 * cosy )) );
    CGContextAddLineToPoint(context,
                            self.lastPoint.x +  (length * cosy + width / 2.0 * siny),
                            self.lastPoint.y -  (width / 2.0 * cosy - length * siny) );
    CGContextClosePath(context);
    CGContextStrokePath(context);
}

#if !ACE_HAS_ARC

- (void)dealloc
{
    self.lineColor = nil;
    [super dealloc];
}

#endif

@end

/////

#pragma mark - ACEDrawingLineTool

@interface ACEDrawingTextTool ()
@property (nonatomic, assign) CGPoint firstPoint;
@property (nonatomic, assign) CGPoint lastPoint;
@end

#pragma mark -

@implementation ACEDrawingTextTool
@synthesize prevImage;
@synthesize toolFrame;
@synthesize lineColor = _lineColor;
@synthesize lineAlpha = _lineAlpha;
@synthesize lineWidth = _lineWidth;

@synthesize begin;
@synthesize type;
@synthesize moveArray;

- (void)setInitialPoint:(CGPoint)firstPoint
{
    self.firstPoint = firstPoint;
}

- (void)moveFromPoint:(CGPoint)startPoint toPoint:(CGPoint)endPoint
{
    self.lastPoint = endPoint;
}

- (void)draw{
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // set the properties
    CGContextSetAlpha(context, self.lineAlpha);
    
    // draw the rectangle
    CGRect rectToFill = CGRectMake(self.firstPoint.x, self.firstPoint.y, self.lastPoint.x - self.firstPoint.x, self.lastPoint.y - self.firstPoint.y);
         
        CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
        CGContextSetLineWidth(context, self.lineWidth);
       CGContextStrokeRect(UIGraphicsGetCurrentContext(), rectToFill);
       
//    CALayer *layer = [CALayer layer];
//    [layer drawInContext:context];
//    [layer convertRect:rectToFill toLayer:layer];
//    [layer setRasterizationScale:0.25];
//    [layer setShouldRasterize:YES];
    

}


- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)context {
    CGContextSetLineWidth(context, 1.0);
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    CGRect rect = CGRectMake(0, 0, 25, 25);
    CGContextAddEllipseInRect(context, rect);
    CGContextStrokePath(context);
    CGContextFillEllipseInRect(context, rect);
}

//Add text to UIImage
-(UIImage *)addText:(UIImage *)img text:(NSString *)text1{
    /*
    int w = img.size.width;
    int h = img.size.height;
    //lon = h - lon;
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
    
    CGContextDrawImage(context, CGRectMake(0, 0, w, h), img.CGImage);
    CGContextSetRGBFillColor(context, 0.0, 0.0, 1.0, 1);
	
    char* text	= (char *)[text1 cStringUsingEncoding:NSASCIIStringEncoding];// "05/05/09";
    CGContextSelectFont(context, "Arial", 18, kCGEncodingMacRoman);
    
    
    CGContextSetTextDrawingMode(context, kCGTextFill);
    CGContextSetRGBFillColor(context, 255, 255, 255, 1);
	
    
    //rotate text
    CGContextSetTextMatrix(context, CGAffineTransformMakeRotation( -M_PI/4 ));
	
    CGContextShowTextAtPoint(context, 4, 52, text, strlen(text));
	
	
    CGImageRef imageMasked = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
	
    return [UIImage imageWithCGImage:imageMasked];
    */
    return nil;
}



#if !ACE_HAS_ARC

- (void)dealloc
{
    self.lineColor = nil;
    [super dealloc];
}

#endif

@end


#pragma mark - ACEDrawingMoveTool

@interface ACEDrawingMoveTool ()
@property (nonatomic, assign) CGPoint firstPoint;
@property (nonatomic, assign) CGPoint lastPoint;

@property (nonatomic) CGFloat diffx;
@property (nonatomic) CGFloat diffy;

@end

#pragma mark -

@implementation ACEDrawingMoveTool
@synthesize prevImage;
@synthesize toolFrame;
@synthesize lineColor = _lineColor;
@synthesize lineAlpha = _lineAlpha;
//@synthesize lineWidth = _lineWidth;

@synthesize begin;
@synthesize pathArrayIndex;
@synthesize moveArray;
@synthesize type;
@synthesize diffx;
@synthesize diffy;
@synthesize prevArray;

- (id)init{
    self = [super init];
    if (self != nil) {
        self.lineCapStyle = kCGLineCapRound;
        self.diffx = 0;
        self.diffy = 0;
        self.moveArray = [[NSMutableArray alloc] init];
        self.prevArray = [[NSMutableArray alloc] init];
        self.lineWidth=5;
    }
    
    return self;
}


- (void)setInitialPoint:(CGPoint)firstPoint
{
    self.firstPoint = firstPoint;
}

- (void)moveFromPoint:(CGPoint)startPoint toPoint:(CGPoint)endPoint
{
    self.lastPoint = endPoint;
    self.diffx = self.lastPoint.x - self.firstPoint.x;
    self.diffy = self.lastPoint.y - self.firstPoint.y;
}

- (void)draw
{
    if([self.moveArray count]!=0){
        CGPoint startP = [[self.prevArray objectAtIndex:0] CGPointValue];
        CGPoint endP = [[self.prevArray objectAtIndex:([self.prevArray count]-1)] CGPointValue];
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGSize          myShadowOffset = CGSizeMake (3,  3);// 2
        
        // shadow
        //CGContextSetShadow(context, myShadowOffset, 3);
        CGContextSetShadowWithColor(context, myShadowOffset,6,[[UIColor colorWithRed:179/255.0f green:179/255.0f blue:179/255.0f alpha:0.1f] CGColor]);
        
        // set the properties
        CGContextSetAlpha(context, self.lineAlpha);
        
        if(self.type == ACEDrawingToolTypeLine){
            // set the line properties
            CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
            CGContextSetLineCap(context, kCGLineCapRound);
            CGContextSetLineWidth(context, self.lineWidth);
            // draw the line
            CGContextMoveToPoint(context, startP.x+self.diffx, startP.y+self.diffy);
            CGContextAddLineToPoint(context, endP.x+self.diffx, endP.y+self.diffy);
            CGContextStrokePath(context);
            
            CGPoint movedStartP = CGPointMake(startP.x+self.diffx, startP.y+self.diffy);
            CGPoint movedendP = CGPointMake(endP.x+self.diffx, endP.y+self.diffy);
            
            [self.moveArray replaceObjectAtIndex:0 withObject:[NSValue valueWithCGPoint:movedStartP]];
            [self.moveArray replaceObjectAtIndex:([self.moveArray count]-1) withObject:[NSValue valueWithCGPoint:movedendP]];
            
        }else if(self.type == ACEDrawingToolTypeArrow){
            
            double slopy, cosy, siny;
            // Arrow size
            double length = 10.0;
            double width = self.lineWidth;
            slopy = atan2(startP.y - endP.y, startP.x - endP.x);
            cosy = cos(slopy);
            siny = sin(slopy);
            
            //draw a line between the 2 endpoint
            CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
            CGContextSetLineCap(context, kCGLineCapSquare);
            CGContextSetLineWidth(context, self.lineWidth);
            CGContextMoveToPoint(context, startP.x+self.diffx - length * cosy, startP.y+self.diffy - length * siny );
            CGContextAddLineToPoint(context, endP.x+self.diffx + length * cosy, endP.y+self.diffy + length * siny);
            
            //paints a line along the current path
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context,endP.x+self.diffx, endP.y+self.diffy);
            CGContextAddLineToPoint(context,
                                    endP.x+self.diffx +  (length * cosy - ( width / 2.0 * siny )),
                                    endP.y+self.diffy +  (length * siny + ( width / 2.0 * cosy )) );
            CGContextAddLineToPoint(context,
                                    endP.x+self.diffx +  (length * cosy + width / 2.0 * siny),
                                    endP.y+self.diffy-  (width / 2.0 * cosy - length * siny) );
            CGContextClosePath(context);
            CGContextStrokePath(context);
            
            CGPoint movedStartP = CGPointMake(startP.x+self.diffx, startP.y+self.diffy);
            CGPoint movedendP = CGPointMake(endP.x+self.diffx, endP.y+self.diffy);
            
            [self.moveArray replaceObjectAtIndex:0 withObject:[NSValue valueWithCGPoint:movedStartP]];
            [self.moveArray replaceObjectAtIndex:([self.moveArray count]-1) withObject:[NSValue valueWithCGPoint:movedendP]];
            
        }else if(self.type == ACEDrawingToolTypeRectagleStroke){
            
            // draw the rectangle
            CGRect rectToFill = CGRectMake(startP.x+self.diffx, startP.y+self.diffy, endP.x - startP.x, endP.y - startP.y);
            CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
            CGContextSetLineWidth(context, self.lineWidth);
            CGContextStrokeRect(UIGraphicsGetCurrentContext(), rectToFill);
            CGPoint movedStartP = CGPointMake(startP.x+self.diffx, startP.y+self.diffy);
            CGPoint movedendP = CGPointMake(endP.x+self.diffx, endP.y+self.diffy);
            
            [self.moveArray replaceObjectAtIndex:0 withObject:[NSValue valueWithCGPoint:movedStartP]];
            [self.moveArray replaceObjectAtIndex:([self.moveArray count]-1) withObject:[NSValue valueWithCGPoint:movedendP]];
        }else if(self.type == ACEDrawingToolTypeRectagleFill){
            // draw the rectangle
            CGRect rectToFill = CGRectMake(startP.x+self.diffx, startP.y+self.diffy, endP.x - startP.x, endP.y - startP.y);
            CGContextSetFillColorWithColor(context, self.lineColor.CGColor);
            CGContextFillRect(UIGraphicsGetCurrentContext(), rectToFill);
            CGPoint movedStartP = CGPointMake(startP.x+self.diffx, startP.y+self.diffy);
            CGPoint movedendP = CGPointMake(endP.x+self.diffx, endP.y+self.diffy);
            
            [self.moveArray replaceObjectAtIndex:0 withObject:[NSValue valueWithCGPoint:movedStartP]];
            [self.moveArray replaceObjectAtIndex:([self.moveArray count]-1) withObject:[NSValue valueWithCGPoint:movedendP]];
        }else if(self.type == ACEDrawingToolTypeEllipseStroke){
            // draw the ellipse
            CGRect rectToFill = CGRectMake(startP.x+self.diffx, startP.y+self.diffy, endP.x - startP.x, endP.y - startP.y);
            
            CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
            CGContextSetLineWidth(context, self.lineWidth);
            CGContextStrokeEllipseInRect(UIGraphicsGetCurrentContext(), rectToFill);
            CGPoint movedStartP = CGPointMake(startP.x+self.diffx, startP.y+self.diffy);
            CGPoint movedendP = CGPointMake(endP.x+self.diffx, endP.y+self.diffy);
            
            [self.moveArray replaceObjectAtIndex:0 withObject:[NSValue valueWithCGPoint:movedStartP]];
            [self.moveArray replaceObjectAtIndex:([self.moveArray count]-1) withObject:[NSValue valueWithCGPoint:movedendP]];
        }else if(self.type == ACEDrawingToolTypePen){
            [self removeAllPoints];
            for(int i=0; i<[self.moveArray count]; i=i+2){
                if([self.moveArray count] > (i+1)){
                    CGPoint sP = [[self.prevArray objectAtIndex:i] CGPointValue];
                    CGPoint eP = [[self.prevArray objectAtIndex:i+1] CGPointValue];
                    
                    sP.x += self.diffx;
                    sP.y += self.diffy;
                    eP.x += self.diffx;
                    eP.y += self.diffy;
                    
                    if(i==0){
                        [self moveToPoint:sP];
                    }
                    
                    [self addQuadCurveToPoint:midPoint(eP,sP) controlPoint:sP];
                    
                    // set the line properties
                    CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
                    [self.lineColor setStroke];
                    
                    [self strokeWithBlendMode:kCGBlendModeNormal alpha:self.lineAlpha];
                    
                    [self.moveArray replaceObjectAtIndex:i withObject:[NSValue valueWithCGPoint:sP]];
                    [self.moveArray replaceObjectAtIndex:(i+1) withObject:[NSValue valueWithCGPoint:eP]];
                }
            }
        }
        
        
    }
}

#if !ACE_HAS_ARC

- (void)dealloc
{
    self.lineColor = nil;
    [super dealloc];
}

#endif

@end



#pragma mark - ACEDrawingCropTool

@interface ACEDrawingCropTool ()

@end

#pragma mark -

@implementation ACEDrawingCropTool

@synthesize lineColor = _lineColor;
@synthesize lineAlpha = _lineAlpha;
@synthesize lineWidth = _lineWidth;
@synthesize firstPoint = _firstPoint;
@synthesize lastPoint = _lastPoint;
@synthesize prevImage;
@synthesize selfFrame;

@synthesize begin;
@synthesize type;
@synthesize moveArray;
- (void)setInitialPoint:(CGPoint)firstPoint
{
    self.firstPoint = firstPoint;
}

- (void)moveFromPoint:(CGPoint)startPoint toPoint:(CGPoint)endPoint
{
    self.lastPoint = endPoint;
}

- (void)draw
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGSize          myShadowOffset = CGSizeMake (3,  3);// 2
    CGContextSetShadow(context, myShadowOffset, 3);
    // set the properties
    CGContextSetAlpha(context, self.lineAlpha);
    
    // draw the rectangle
    CGRect rectToFill = CGRectMake(self.firstPoint.x, self.firstPoint.y, self.lastPoint.x - self.firstPoint.x, self.lastPoint.y - self.firstPoint.y);
    if (self.fill) {
        CGContextSetFillColorWithColor(context, self.lineColor.CGColor);
        CGContextFillRect(UIGraphicsGetCurrentContext(), rectToFill);
    } else {
        
        // draw gray shadow
        CGContextSetAlpha(context, 0.3);
        CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);
        
        
        if(self.lastPoint.y > self.firstPoint.y){
            
            CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, selfFrame.size.width, self.firstPoint.y));
            CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, self.lastPoint.y, selfFrame.size.width, selfFrame.size.height-self.lastPoint.y));
            
            
        }else{
            CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, selfFrame.size.width, self.lastPoint.y));
            CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, self.firstPoint.y, selfFrame.size.width, selfFrame.size.height-self.firstPoint.y));
            
        }
        
        if(self.lastPoint.x > self.firstPoint.x){
            CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, self.firstPoint.y, self.firstPoint.x, self.lastPoint.y - self.firstPoint.y));
            CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(self.lastPoint.x, self.firstPoint.y, selfFrame.size.width - self.lastPoint.x, self.lastPoint.y - self.firstPoint.y));
        }else{
            CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, self.lastPoint.y, self.lastPoint.x, self.firstPoint.y - self.lastPoint.y));
            CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(self.firstPoint.x, self.lastPoint.y, selfFrame.size.width - self.firstPoint.x, self.firstPoint.y - self.lastPoint.y));
        }
    }
}

#if !ACE_HAS_ARC

- (void)dealloc
{
    self.lineColor = nil;
    [super dealloc];
}

#endif

@end
