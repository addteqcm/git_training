

#import <UIKit/UIKit.h>

#define ACEDrawingViewVersion   1.0.0

typedef enum {
    ACEDrawingToolTypePen,
    ACEDrawingToolTypeLine,
    ACEDrawingToolTypeRectagleStroke,
    ACEDrawingToolTypeRectagleFill,
    ACEDrawingToolTypeEllipseStroke,
    ACEDrawingToolTypeEllipseFill,
    ACEDrawingToolTypeArrow,
    ACEDrawingArrowToolTypeText,
    ACEDrawingToolTypeCrop,
    ACEDRawingToolTypeMove
} ACEDrawingToolType;

@protocol ACEDrawingViewDelegate, ACEDrawingTool;

@interface ACEDrawingView : UIView

@property (nonatomic, assign) ACEDrawingToolType drawTool;
@property (nonatomic, assign) id<ACEDrawingViewDelegate> delegate;

// public properties
@property (nonatomic, assign) UIColor *lineColor;
@property (nonatomic, assign) CGFloat lineWidth;
@property (nonatomic, assign) CGFloat lineAlpha;
@property (nonatomic, assign) UIColor *defaultColor;

// get the current drawing
//@property (nonatomic, strong, readonly) UIImage *image;
@property (nonatomic, strong) NSMutableArray *imageArray;

@property (nonatomic, readonly) NSUInteger undoSteps;

@property (nonatomic, weak) UIImageView *cropImage;
@property (nonatomic, weak) UIImageView *originImage;

@property (nonatomic) CGRect currentFrame;
@property (nonatomic, weak) UIView *cropView;


// erase all
- (void)clear;

// undo / redo
- (BOOL)canUndo;
- (void)undoLatestStep;

- (BOOL)canRedo;
- (void)redoLatestStep;

-(void) deleteOne:(NSUInteger)imageId;
-(void) setting:(UIImageView  *) ownImage;

@end

#pragma mark -

@protocol ACEDrawingViewDelegate <NSObject>

@optional
- (void)drawingView:(ACEDrawingView *)view willBeginDrawUsingTool:(id<ACEDrawingTool>)tool;
- (void)drawingView:(ACEDrawingView *)view didEndDrawUsingTool:(id<ACEDrawingTool>)tool;

@end
