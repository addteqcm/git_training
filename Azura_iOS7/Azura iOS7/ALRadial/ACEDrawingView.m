

#import "ACEDrawingView.h"
#import "ACEDrawingTools.h"

#import <QuartzCore/QuartzCore.h>

#define kDefaultLineColor       [UIColor blackColor]
#define kDefaultLineWidth       5.0f;
#define kDefaultLineAlpha       1.0f

// experimental code
#define PARTIAL_REDRAW          0

@interface ACEDrawingView ()
@property (nonatomic, strong) NSMutableArray *pathArray;
@property (nonatomic, strong) NSMutableArray *bufferArray;
@property (nonatomic, strong) id<ACEDrawingTool> currentTool;
@property (nonatomic, strong) UIImage *image;

@property (nonatomic) BOOL cropped;
@property (nonatomic, strong) UIImage *croppedImage;
@property (nonatomic) CGRect croppedFrame;

@end

#pragma mark -

@implementation ACEDrawingView
@synthesize cropImage;
@synthesize currentFrame;
@synthesize originImage;
- (id)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        [self configure];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self configure];
    }
    return self;
}

- (void)configure
{
    // init the private arrays
    self.pathArray = [NSMutableArray array];
    self.bufferArray = [NSMutableArray array];
                       
    // set the default values for the public properties
    self.lineColor = kDefaultLineColor;
    self.lineWidth = kDefaultLineWidth;
    self.lineAlpha = kDefaultLineAlpha;
    
    // set the transparent background
    
    self.backgroundColor = [UIColor clearColor];
    [self setCropped:false];
}


#pragma mark - Drawing

- (void)drawRect:(CGRect)rect
{
#if PARTIAL_REDRAW
    // TODO: draw only the updated part of the image
    [self drawPath];
    
#else
    [self.image drawInRect:self.bounds];
    [self.currentTool draw];
#endif
}

-(void) skipOne:(int) index{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);
    self.image = nil;
    
    // I need to redraw all the lines
    if([self.pathArray count]>0){
        
        for (int i=0; i<[self.pathArray count]; i++)
        {
            if(index != i){
                id<ACEDrawingTool> tool = [self.pathArray objectAtIndex:i];
                if(![tool isMemberOfClass:[ACEDrawingCropTool class]] && ![tool isMemberOfClass:[ACEDrawingMoveTool class]]){
                    [tool draw];
                }
            }
        }
    }
    // store the image
    self.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
}

- (void)updateCacheImage:(BOOL)redraw
{
    // init a context
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);
    
    if (redraw) {
        // erase the previous image
        self.image = nil;
        
        // I need to redraw all the lines
        for (id<ACEDrawingTool> tool in self.pathArray) {
            if(![tool isMemberOfClass:[ACEDrawingCropTool class]] && ![tool isMemberOfClass:[ACEDrawingMoveTool class]]){
                [tool draw];
            }
            
        }
        
    } else {
        // set the draw point
        [self.image drawAtPoint:CGPointZero];
        [self.currentTool draw];
    }
    
    // store the image
    self.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

- (id<ACEDrawingTool>)toolWithCurrentSettings
{
    switch (self.drawTool) {
        case ACEDrawingToolTypePen:
        {
            ACEDrawingPenTool *tool =ACE_AUTORELEASE([ACEDrawingPenTool new]);
            tool.type = ACEDrawingToolTypePen;
            tool.begin = true;
            return tool;
        }
            
        case ACEDrawingToolTypeLine:
        {
            return ACE_AUTORELEASE([ACEDrawingLineTool new]);
            ACEDrawingLineTool *tool = ACE_AUTORELEASE([ACEDrawingLineTool new]);
            tool.type = ACEDrawingToolTypeLine;
            tool.begin = true;
            return tool;
        }
            
        case ACEDrawingToolTypeRectagleStroke:
        {
            ACEDrawingRectangleTool *tool = ACE_AUTORELEASE([ACEDrawingRectangleTool new]);
            tool.fill = NO;
            tool.type = ACEDrawingToolTypeRectagleStroke;
            tool.begin = true;
            return tool;
        }
            
        case ACEDrawingToolTypeRectagleFill:
        {
            ACEDrawingRectangleTool *tool = ACE_AUTORELEASE([ACEDrawingRectangleTool new]);
            tool.fill = YES;
            tool.type = ACEDrawingToolTypeRectagleFill;
            tool.begin = true;
            return tool;
        }
            
        case ACEDrawingToolTypeEllipseStroke:
        {
            ACEDrawingEllipseTool *tool = ACE_AUTORELEASE([ACEDrawingEllipseTool new]);
            tool.fill = NO;
            tool.type = ACEDrawingToolTypeEllipseStroke;
            tool.begin = true;
            return tool;
        }
            
        case ACEDrawingToolTypeEllipseFill:
        {
            ACEDrawingEllipseTool *tool = ACE_AUTORELEASE([ACEDrawingEllipseTool new]);
            tool.fill = YES;
            tool.type = ACEDrawingToolTypeEllipseFill;
            tool.begin = true;
            return tool;
        }
            case ACEDrawingToolTypeArrow:
        {
            ACEDrawingArrowTool *tool = ACE_AUTORELEASE([ACEDrawingArrowTool new]);
            tool.type = ACEDrawingToolTypeArrow;
            tool.begin = true;
            return tool;

        }
        case ACEDrawingArrowToolTypeText:
        {
            
            ACEDrawingTextTool *tool = ACE_AUTORELEASE([ACEDrawingTextTool new]);
            tool.fill=YES;
            return tool;
        }
        case ACEDrawingToolTypeCrop:
        {
            ACEDrawingCropTool *tool = ACE_AUTORELEASE([ACEDrawingCropTool new]);
            tool.fill = NO;
            tool.type = ACEDrawingToolTypeCrop;
            tool.selfFrame = self.frame;
            return tool;
        }
        case ACEDRawingToolTypeMove:
        {
            ACEDrawingMoveTool *tool = ACE_AUTORELEASE([ACEDrawingMoveTool new]);
            tool.type = ACEDRawingToolTypeMove;
            return tool;
        }
    }
}


- (void)crop:(CGRect) frame
{
    self.cropImage = self.originImage;
    CGImageRef tmp = CGImageRetain(CGImageCreateWithImageInRect([self.cropImage.image CGImage], frame));
    self.cropImage.image = [UIImage imageWithCGImage:tmp];
    self.cropImage.frame =  CGRectMake(0, 0, frame.size.width, frame.size.height);
    self.cropView.frame =self.cropImage.frame;
    self.frame = self.cropImage.frame;
    
}



#pragma mark - Touch Methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // init the bezier path
    self.currentTool = [self toolWithCurrentSettings];
    self.currentTool.lineWidth = self.lineWidth;
    self.currentTool.lineColor = self.lineColor;
    self.currentTool.lineAlpha = self.lineAlpha;
    
    // add the first touch
    UITouch *touch = [touches anyObject];
    [self.currentTool setInitialPoint:[touch locationInView:self]];
    
    
    if([self.currentTool isMemberOfClass:[ACEDrawingCropTool class]]){
        self.currentTool.prevImage = [[UIImageView alloc] initWithImage:self.originImage.image];
    }else if([self.currentTool isMemberOfClass:[ACEDrawingMoveTool class]]){
        
        ACEDrawingMoveTool *mtool = (ACEDrawingMoveTool *) self.currentTool;
        mtool.pathArrayIndex = -1;
        if([self.pathArray count]>0){
            for(int i=0; i<[self.pathArray count]; i++){
                id<ACEDrawingTool>tool = [self.pathArray objectAtIndex:i];
                
                if(CGRectContainsPoint(tool.toolFrame, [touch locationInView:self])){
                    [mtool setPathArrayIndex:i];
                }
            }
            if(mtool.pathArrayIndex != -1){
                id<ACEDrawingTool>tool = [self.pathArray objectAtIndex:mtool.pathArrayIndex];
                mtool.moveArray = tool.moveArray;
                mtool.prevArray = [tool.moveArray copy];
                mtool.type = tool.type;
                mtool.lineColor = tool.lineColor;
                [self skipOne:mtool.pathArrayIndex];
            }
        }
    }
    
    [self.pathArray addObject:self.currentTool];
    
    // call the delegate
    if ([self.delegate respondsToSelector:@selector(drawingView:willBeginDrawUsingTool:)]) {
        [self.delegate drawingView:self willBeginDrawUsingTool:self.currentTool];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    // save all the touches in the path
    UITouch *touch = [touches anyObject];
    
    // add the current point to the path
    CGPoint currentLocation = [touch locationInView:self];
    CGPoint previousLocation = [touch previousLocationInView:self];
    [self.currentTool moveFromPoint:previousLocation toPoint:currentLocation];
    
#if PARTIAL_REDRAW
    // calculate the dirty rect
    CGFloat minX = fmin(previousLocation.x, currentLocation.x) - self.lineWidth * 0.5;
    CGFloat minY = fmin(previousLocation.y, currentLocation.y) - self.lineWidth * 0.5;
    CGFloat maxX = fmax(previousLocation.x, currentLocation.x) + self.lineWidth * 0.5;
    CGFloat maxY = fmax(previousLocation.y, currentLocation.y) + self.lineWidth * 0.5;
    [self setNeedsDisplayInRect:CGRectMake(minX, minY, (maxX - minX), (maxY - minY))];
#else
    [self setNeedsDisplay];
#endif
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    // make sure a point is recorded
    [self touchesMoved:touches withEvent:event];
    
    if([self.currentTool isMemberOfClass:[ACEDrawingCropTool class]]){
        ACEDrawingCropTool *mytool = self.currentTool;
        CGRect frame = CGRectMake(mytool.firstPoint.x, mytool.firstPoint.y, mytool.lastPoint.x-mytool.firstPoint.x, mytool.lastPoint.y-mytool.firstPoint.y);
        if(frame.size.height==0 || frame.size.width==0){
            [self.pathArray removeLastObject];
        }else{
            mytool.toolFrame = frame;
            [self crop:(CGRect) frame];
        }
        
        
    }else if([self.currentTool isMemberOfClass:[ACEDrawingMoveTool class]]){
        
        
        ACEDrawingMoveTool *mtool = (ACEDrawingMoveTool *)self.currentTool;
        if(mtool.pathArrayIndex!=-1){
            id<ACEDrawingTool>tool = [self.pathArray objectAtIndex:mtool.pathArrayIndex];
            
            [tool setBegin:false];
            if(mtool.type == ACEDrawingToolTypePen){
                for(int i=0; i<[mtool.moveArray count]; i=i+2){
                    if((i+1) < [mtool.moveArray count]){
                        CGPoint startP = [[mtool.moveArray objectAtIndex:i] CGPointValue];
                        CGPoint endP = [[mtool.moveArray objectAtIndex:(i+1)] CGPointValue];
                        if(i==0){
                            [tool setInitialPoint:startP];
                        }
                        [tool moveFromPoint:startP toPoint:endP];
                    }
                }
            }else{
                CGPoint startP = [[mtool.moveArray objectAtIndex:0] CGPointValue];
                CGPoint endP = [[mtool.moveArray objectAtIndex:([mtool.moveArray count]-1)] CGPointValue];
                [tool setInitialPoint:startP];
                [tool moveFromPoint:startP toPoint:endP];
            }
            
            [self updateCacheImage:YES];
        }
    }else{
        // update the image
        [self updateCacheImage:NO];
    }
    
    
    
    // clear the current tool
    self.currentTool = nil;
    
    // clear the redo queue
    [self.bufferArray removeAllObjects];
    /*
    // call the delegate
    if ([self.delegate respondsToSelector:@selector(drawingView:didEndDrawUsingTool:)]) {
        [self.delegate drawingView:self didEndDrawUsingTool:self.currentTool];
    }
     */
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    // make sure a point is recorded
    [self touchesEnded:touches withEvent:event];
}


#pragma mark - Actions

- (void)clear
{
    [self.bufferArray removeAllObjects];
    if([self.pathArray count]>0){
        for(int i=([self.pathArray count]-1); i>=0; i--){
            id<ACEDrawingTool>tool = [self.pathArray objectAtIndex:i];
            if([tool isMemberOfClass:[ACEDrawingCropTool class]]){
            
                CGRect frame = tool.prevImage.frame;
                self.cropImage.frame = frame;
                self.cropImage.image = tool.prevImage.image;
                self.frame = frame;
                self.cropView.frame = frame;
            
            }
        }
    
    }
    
    [self.pathArray removeAllObjects];
    [self updateCacheImage:YES];
    [self setNeedsDisplay];
}


#pragma mark - Undo / Redo

- (NSUInteger)undoSteps
{
    return self.bufferArray.count;
}

- (BOOL)canUndo
{
    return self.pathArray.count > 0;
}

- (void)undoLatestStep
{
    if ([self canUndo]) {
        id<ACEDrawingTool>tool = [self.pathArray lastObject];
        [self.bufferArray addObject:tool];
        [self.pathArray removeLastObject];
        
        if([tool isMemberOfClass:[ACEDrawingCropTool class]]){

            CGRect frame = tool.prevImage.frame;
            self.cropImage.frame = frame;
            self.cropImage.image = tool.prevImage.image;
            self.frame = frame;
            self.cropView.frame = frame;
        
        }else if([tool isMemberOfClass:[ACEDrawingMoveTool class]]){
            
            ACEDrawingMoveTool *mtool = (ACEDrawingMoveTool *)tool;
            id<ACEDrawingTool>undotool = [self.pathArray objectAtIndex:mtool.pathArrayIndex];
            
            [undotool setBegin:false];
            
            if(mtool.type == ACEDrawingToolTypePen){
                for(int i=0; i<[mtool.prevArray count]; i=i+2){
                    if((i+1) < [mtool.prevArray count]){
                        CGPoint startP = [[mtool.prevArray objectAtIndex:i] CGPointValue];
                        CGPoint endP = [[mtool.prevArray objectAtIndex:(i+1)] CGPointValue];
                        if(i==0){
                            [undotool setInitialPoint:startP];
                        }
                        [undotool moveFromPoint:startP toPoint:endP];
                    }
                }
            }else{
                CGPoint startP = [[mtool.prevArray objectAtIndex:0] CGPointValue];
                CGPoint endP = [[mtool.prevArray objectAtIndex:([mtool.prevArray count]-1)] CGPointValue];
                [undotool setInitialPoint:startP];
                [undotool moveFromPoint:startP toPoint:endP];
                
            }
            
            [self updateCacheImage:YES];
            [undotool setBegin:true];
            
        }else{
            [self updateCacheImage:YES];
        }
        
        [self setNeedsDisplay];
    }
}

- (BOOL)canRedo
{
    return self.bufferArray.count > 0;
}

- (void)redoLatestStep
{
    if ([self canRedo]) {
        id<ACEDrawingTool>tool = [self.bufferArray lastObject];
        [self.pathArray addObject:tool];
        [self.bufferArray removeLastObject];
        if([tool isMemberOfClass:[ACEDrawingCropTool class]]){
            [self crop:tool.toolFrame];
        }else if([tool isMemberOfClass:[ACEDrawingMoveTool class]]){
            ACEDrawingMoveTool *mtool = (ACEDrawingMoveTool *)tool;
            id<ACEDrawingTool>redotool = [self.pathArray objectAtIndex:mtool.pathArrayIndex];
            
            [redotool setBegin:false];
            
            if(mtool.type == ACEDrawingToolTypePen){
                for(int i=0; i<[mtool.moveArray count]; i=i+2){
                    if((i+1) < [mtool.moveArray count]){
                        CGPoint startP = [[mtool.moveArray objectAtIndex:i] CGPointValue];
                        CGPoint endP = [[mtool.moveArray objectAtIndex:(i+1)] CGPointValue];
                        if(i==0){
                            [redotool setInitialPoint:startP];
                        }
                        [redotool moveFromPoint:startP toPoint:endP];
                    }
                }
            }else{
                CGPoint startP = [[mtool.moveArray objectAtIndex:0] CGPointValue];
                CGPoint endP = [[mtool.moveArray objectAtIndex:([mtool.moveArray count]-1)] CGPointValue];
                [redotool setInitialPoint:startP];
                [redotool moveFromPoint:startP toPoint:endP];
                
            }
            [self updateCacheImage:YES];
            [redotool setBegin:true];
        }else{
            [self updateCacheImage:YES];
        }
        [self setNeedsDisplay];
    }
}

-(void)deleteOne: (NSUInteger) deleteId{
    
    [self clear];
    
    
}
-(void) setting:(UIImageView  *) ownImage;{
    self.originImage = ownImage;
    self.currentTool.prevImage = ownImage;
}

#if !ACE_HAS_ARC

- (void)dealloc
{
    self.pathArray = nil;
    self.bufferArray = nil;
    self.currentTool = nil;
    self.image = nil;
    [super dealloc];
}

#endif

@end
