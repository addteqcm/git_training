

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

#if __has_feature(objc_arc)
#define ACE_HAS_ARC 1
#define ACE_RETAIN(exp) (exp)
#define ACE_RELEASE(exp)
#define ACE_AUTORELEASE(exp) (exp)
#else
#define ACE_HAS_ARC 0
#define ACE_RETAIN(exp) [(exp) retain]
#define ACE_RELEASE(exp) [(exp) release]
#define ACE_AUTORELEASE(exp) [(exp) autorelease]
#endif


@protocol ACEDrawingTool <NSObject>

@property (nonatomic, strong) UIColor *lineColor;

@property (nonatomic, assign) CGFloat lineAlpha;
@property (nonatomic, assign) CGFloat lineWidth;
@property (nonatomic,strong) UIImageView *prevImage;

@property (nonatomic, strong) NSMutableArray *moveArray;
@property (nonatomic, assign) int type;
@property (nonatomic, assign) BOOL begin;

@property (nonatomic, assign) CGRect toolFrame;
- (void)setInitialPoint:(CGPoint)firstPoint;
- (void)moveFromPoint:(CGPoint)startPoint toPoint:(CGPoint)endPoint;

- (void)draw;

@end

#pragma mark -

@interface ACEDrawingPenTool : UIBezierPath<ACEDrawingTool>
@property (nonatomic, assign) CGPoint max;
@property (nonatomic, assign) CGPoint min;
@end

#pragma mark -

@interface ACEDrawingLineTool : NSObject<ACEDrawingTool>

@end

#pragma mark -

@interface ACEDrawingArrowTool : NSObject<ACEDrawingTool>

@end

#pragma mark -

@interface ACEDrawingRectangleTool : NSObject<ACEDrawingTool>

@property (nonatomic, assign) BOOL fill;

@end

#pragma mark -

@interface ACEDrawingEllipseTool : NSObject<ACEDrawingTool>

@property (nonatomic, assign) BOOL fill;

@end

#pragma mark -

@interface ACEDrawingTextTool : NSObject<ACEDrawingTool>
@property (nonatomic, assign) BOOL fill;
@end


#pragma mark -

@interface ACEDrawingMoveTool : UIBezierPath<ACEDrawingTool>
@property (nonatomic, strong) NSMutableArray *prevArray;
@property (nonatomic, assign) int pathArrayIndex;

@end


#pragma mark -

@interface ACEDrawingCropTool : NSObject<ACEDrawingTool>
@property (nonatomic, assign) CGPoint firstPoint;
@property (nonatomic, assign) CGPoint lastPoint;
@property (nonatomic, assign) CGRect toolFrame;
@property (nonatomic, assign) CGRect selfFrame;
@property (nonatomic, assign) BOOL fill;

@end

