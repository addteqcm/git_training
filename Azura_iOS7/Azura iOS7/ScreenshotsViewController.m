//
//  ScreenshotsViewController.m
//  Azura iOS7
//
//  Created by HeeJinChoi on 1/15/14.
//  Copyright (c) 2014 addteq. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ScreenshotsViewController.h"

#define kFeedbackImageState @"feedbackImageState"
#define kFeedbackImages @"feedbackImages"

#define kSelectedSS @"selectedSS"
#define kDeletedSS @"deletedSS"

#define FROM_FEEDBACK 3
#define FROM_EDIT 4

@interface ScreenshotsViewController ()

@end

@implementation ScreenshotsViewController
@synthesize arrayImages;
@synthesize arraySelected;
@synthesize arrayDeleted;
@synthesize dataPath;
@synthesize curPath;
@synthesize draggingImage;
@synthesize curFileName;
@synthesize skyblue;
@synthesize source;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.imageList registerClass:[SsCollectionCell class] forCellWithReuseIdentifier:@"My_cell"];
    [self.imageList setAllowsMultipleSelection:YES];
    self.arrayImages = [[NSMutableArray alloc] init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if(self.source == FROM_FEEDBACK){
        self.arraySelected =[NSMutableArray arrayWithArray:[defaults objectForKey:kFeedbackImages]];
    }else if(self.source == FROM_EDIT){
        self.arraySelected = [[NSMutableArray alloc] init];
        self.arrayDeleted = [[NSMutableArray alloc] init];
    }
    
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    dataPath = [docDir stringByAppendingPathComponent:@"/Screenshots"];
    NSArray *fileList = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:nil];
    [self.arrayImages addObjectsFromArray:fileList];
    
    skyblue = [UIColor colorWithRed:0.0f green:122.0f/255.0f blue:1.0f alpha:1.0f];
}

-(void) viewWillAppear:(BOOL)animated{
    [self.navigationController.navigationBar setHidden:YES];
    if(self.source == FROM_FEEDBACK){
        [self.navigationController.toolbar setHidden:YES];
    }
}
-(void) viewWillDisappear:(BOOL)animated{
    [self.navigationController.navigationBar setHidden:NO];
    if(self.source == FROM_FEEDBACK){
        [self.navigationController.toolbar setHidden:NO];
    }
}

-(void) viewDidAppear:(BOOL)animated{
    for(int i =0; i< [arrayImages count]; i++){
        NSString *inFolder = [arrayImages objectAtIndex:i];
        NSString *selected = @"";
        for(int j=0; j<[self.arraySelected count]; j++){
            selected = [self.arraySelected objectAtIndex:j];
            if([selected isEqualToString:inFolder]){
                [self.imageList selectItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionNone];
            }
        }
    }
}

-(void)longGestureAction:(UILongPressGestureRecognizer *)gesture{
    SsCollectionCell *cell= (SsCollectionCell *)[gesture view];
    
    switch ([gesture state]) {
        case UIGestureRecognizerStateBegan:{
            NSIndexPath *ip = [self.imageList indexPathForCell:cell];
            [self.imageList setScrollEnabled:NO];
            NSLog(@"BEGIN %@", cell.filename);
            self.curFileName = cell.filename;
            self.curPath = [dataPath stringByAppendingPathComponent:self.curFileName];
            
            if(ip!=nil){
                [self draggingGestureWillBegin:gesture forCell:cell];
            }else{
                NSLog(@"NILLLL");
            }
            
        }
            break;
        case UIGestureRecognizerStateChanged:{
            NSLog(@"CHANGE");
            [self draggingGestureDidMove:gesture];
        }
            break;
        case UIGestureRecognizerStateEnded:{
            NSLog(@"END");
            [self.imageList setScrollEnabled:YES];
            if(self.draggingImage == nil){
                return;
            }
            [self draggingGestureDidEnd:gesture];
            [self droppedGesture:gesture];
            self.curPath = @"";
            self.curFileName=@"";
           /*
            UIView *draggedView = [self.draggableDelegate dragAndDropTableViewControllerView:self];
            if(draggedView==nil)
                return;
            
            //this does not seem like the best way to do this yet you really don't want to fire one after the other I don't think
            [self.draggableDelegate dragAndDropTableViewController:self draggingGestureDidEnd:gesture];
            [self.dropableDelegate dragAndDropTableViewController:self droppedGesture:gesture];
            
            [self.tableView setScrollEnabled:YES];
            [self.tableView reloadData];
        }
            break;
            
            //        case UIGestureRecognizerStateCancelled:
            //        case UIGestureRecognizerStateFailed:
            //        case UIGestureRecognizerStatePossible:
            //            [self.dragAndDropDelegate dragAndDropTableViewController:self draggingGesture:gesture endedForItem:nil];*/}
            break;
        default:
            break;
    }
}

-(void)droppedGesture:(UIGestureRecognizer *)gesture{
    UIView *viewHit = [self.view hitTest:[gesture locationInView:self.view.superview] withEvent:nil];
    
    NSString *hit = @"UIToolbarButton";
    NSString *dest = [viewHit.class description];
    
    
    NSLog(@"%@ %@", hit, dest);
    if([hit isEqualToString:dest]){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete" message:@"Do you want to delete this image?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
        [alert show];
        
    }else{
        [self.imageList reloadData];
    }
    
    
    
}



-(void)draggingGestureWillBegin:(UIGestureRecognizer *)gesture forCell:(SsCollectionCell *)cell{
    UIGraphicsBeginImageContext(cell.contentView.bounds.size);
    [cell.contentView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *iv = [[UIImageView alloc] initWithImage:img];
    self.draggingImage = [[UIView alloc]initWithFrame:iv.frame];
    [self.draggingImage addSubview:iv];
    [self.draggingImage setBackgroundColor:skyblue];
    [self.draggingImage setCenter:[gesture locationInView:self.view.superview]];
    
    [self.view.superview addSubview:self.draggingImage];
    [self.draggingImage addGestureRecognizer:[[cell gestureRecognizers]objectAtIndex:0]];

}
-(void)draggingGestureDidMove:(UIGestureRecognizer *)gesture{
    [self.draggingImage setCenter:[gesture locationInView:self.view.superview]];
}
-(void)draggingGestureDidEnd:(UIGestureRecognizer *)gesture{
    [self.draggingImage removeFromSuperview];
    self.draggingImage = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionView Datasource
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section{
    return [arrayImages count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SsCollectionCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"CollectionCell" forIndexPath:indexPath];
    
    NSString *name = [arrayImages objectAtIndex:[indexPath row]];
    NSString *filePath = [dataPath stringByAppendingPathComponent:name];
    cell.filename = name;
    
    cell.screenshotFile.image = [UIImage imageWithContentsOfFile:filePath];
    /* drag and drop function
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:cell action:nil];
    longPress.delegate = self;
    [cell addGestureRecognizer:longPress];
    */
    return cell;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]]){
        [gestureRecognizer addTarget:self action:@selector(longGestureAction:)];
    }
    return YES;
}


#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   [self.arraySelected addObject:[arrayImages objectAtIndex:[indexPath row]]];
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // TODO: Deselect item
    [self.arraySelected removeObject:[arrayImages objectAtIndex:[indexPath row]]];
}
/*
 - (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(100, 70);
    
}
*/
- (UIEdgeInsets)collectionView:

(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(10, 5, 10, 5);
    
}

- (IBAction)doneChoosing:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
    if(self.source == FROM_FEEDBACK){
        [_delegate selectImagesFromFeedback:arraySelected];
        
    }else if(self.source==FROM_EDIT){
        [defaults setObject:arrayDeleted forKey:kDeletedSS];
        [defaults setObject:arraySelected forKey:kSelectedSS];
    }
    
    
    [defaults synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelChoosing:(id)sender {
    
    
    if(self.source==FROM_EDIT){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:arrayDeleted forKey:kDeletedSS];
        [defaults synchronize];
    }
    
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)deleteScreenshots:(id)sender {
    NSLog(@"delete");
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete" message:@"Do you want to delete this image?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    [alert show];
    
    
}


#pragma mark - UIAlertViewDelegate Methods

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Yes"]){
        NSLog(@"delete image and reload collectionView");
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *filename=@"";
        for(int i = 0; i< [self.arraySelected count]; i++){
            id tmp = [arraySelected objectAtIndex:i];
            filename = tmp;
            filename = [dataPath stringByAppendingPathComponent:filename];
            if([fileManager fileExistsAtPath:filename]){
                [fileManager removeItemAtPath:filename error:NULL];
                [arrayImages removeObject:tmp];
                [self.arrayDeleted addObject:tmp];
            }
        }
        [self.imageList reloadData];
        [self.arraySelected removeAllObjects];
    }
}

@end
