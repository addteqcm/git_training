//
//  WebViewDetails.h
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 10/2/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebViewDetails : NSObject
@property (nonatomic,retain) NSString* webViewTitle;
@property (nonatomic,retain) UIImage* webViewImage;
@end
