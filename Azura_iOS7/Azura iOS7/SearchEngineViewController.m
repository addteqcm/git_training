//
//  SearchEngineViewController.m
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/10/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "SearchEngineViewController.h"

@interface SearchEngineViewController ()

@end

@implementation SearchEngineViewController
@synthesize checkedIndexPath;
@synthesize _selectedRow;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
        return 3;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int searchEngineIndex = [[[NSUserDefaults standardUserDefaults]valueForKey:@"Search_Engine"] intValue];
    if(indexPath.section==0)
    {
        if(indexPath.row==0)
        {
            SearchEngineCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Google" forIndexPath:indexPath];
            if(searchEngineIndex==0)
            {
                NSLog(@"Was initially selected : Google");
                NSLog(@"Selected Google after : %d",cell.accessoryType);
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                NSLog(@"Selected Google after : %d",cell.accessoryType);
            }
            else
            {
                NSLog(@"Not now selected selected : Google");
                NSLog(@"Not selected Google before : %d",cell.accessoryType);
                cell.accessoryType = UITableViewCellAccessoryNone;
                NSLog(@"Not selected Google after : %d",cell.accessoryType);
            }
            return cell;
            
        }
        if(indexPath.row==1)
        {
            SearchEngineCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Yahoo!" forIndexPath:indexPath];
            if(searchEngineIndex==1)
            {
                NSLog(@"Was initially selected : Yahoo!");
                NSLog(@"Selected Yahoo! before : %d",cell.accessoryType);
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                NSLog(@"Selected Yahoo! after : %d",cell.accessoryType);
            }
            else
            {
                NSLog(@"Not now selected selected : Yahoo!");
                NSLog(@"Not selected Yahoo! before : %d",cell.accessoryType);
                cell.accessoryType = UITableViewCellAccessoryNone;
                NSLog(@"Not selected Yahoo! after : %d",cell.accessoryType);
            }
            return cell;
        }
        if(indexPath.row==2)
        {
            SearchEngineCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Bing" forIndexPath:indexPath];
            if(searchEngineIndex==2)
            {
               NSLog(@"Was initially selected : Bing");
               NSLog(@"Selected Bing before : %d",cell.accessoryType);
               cell.accessoryType = UITableViewCellAccessoryCheckmark;
               NSLog(@"Selected Bing after : %d",cell.accessoryType);
            }
            else
            {
                NSLog(@"Not now selected selected : Bing");
                NSLog(@"Not selected Bing before : %d",cell.accessoryType);
                cell.accessoryType = UITableViewCellAccessoryNone;
                NSLog(@"Not selected Bing after : %d",cell.accessoryType);
            }
            return cell;
        }
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *selectedSearchEngine = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    NSLog(@"%@",selectedSearchEngine);
    SearchEngineCell *googleCell;
    SearchEngineCell *yahooCell;
    SearchEngineCell *bingCell;

    if([selectedSearchEngine isEqual:@"0"])
    {
        NSLog(@"Did select Google");
        googleCell = [tableView dequeueReusableCellWithIdentifier:@"Google" forIndexPath:indexPath];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"Search_Engine"];
        [googleCell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else if([selectedSearchEngine isEqual:@"1"])
    {
        NSLog(@"Did select Yahoo!");
        yahooCell = [tableView dequeueReusableCellWithIdentifier:@"Yahoo!" forIndexPath:indexPath];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"Search_Engine"];
        [yahooCell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else if([selectedSearchEngine isEqual:@"2"])
    {
        NSLog(@"Did select Bing");
        bingCell = [tableView dequeueReusableCellWithIdentifier:@"Bing" forIndexPath:indexPath];
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"Search_Engine"];
        [bingCell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    NSLog(@"Did select Google : %d",googleCell.accessoryType);
    NSLog(@"Did select Yahoo : %d",yahooCell.accessoryType);
    NSLog(@"Did select Bing : %d",bingCell.accessoryType);

    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"Reloading table");
    [tableView reloadData];
    NSLog(@"Table Reloaded");
    [self.navigationController popViewControllerAnimated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
