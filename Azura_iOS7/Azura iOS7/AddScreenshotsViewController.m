//
//  AddScreenshotsViewController.m
//  Azura iOS7
//
//  Created by HeeJinChoi on 1/15/14.
//  Copyright (c) 2014 addteq. All rights reserved.
//

#import "AddScreenshotsViewController.h"
#import "EditScreenshotsViewController.h"
#import "screenshotsCell.h"

#define kFeedbackImages @"feedbackImages"
#define FROM_EDIT 4

@interface AddScreenshotsViewController ()

@end

@implementation AddScreenshotsViewController
@synthesize totalCount;
@synthesize dataPath;
@synthesize slideMenuHeight;
@synthesize skyblue;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.arrayThumbnails = [[NSMutableArray alloc] init];
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    dataPath = [docDir stringByAppendingPathComponent:@"/Screenshots"];
    skyblue = [UIColor colorWithRed:0.0f green:122.0f/255.0f blue:1.0f alpha:1.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    self.totalCount = 3 + [self.arrayThumbnails count];
    //NSLog(@"%d", totalCount);
    return totalCount;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"myListCell";

    screenshotsCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    
    // Here we use the provided setImageWithURL: method to load the web image
    // Ensure you use a placeholder image otherwise cells will be initialized with no image
    //cell.textLabel.text = @"My Text";
    
    
    
    if([indexPath row]==0){
        cell.middleImage.image = [UIImage imageNamed:@"homebig_skyblue.png"];
        [cell.middleImage.layer setBorderColor:[[UIColor clearColor] CGColor]];
        [cell.middleImage.layer setBorderWidth:2.0];

    }else if([indexPath row]==(totalCount-2)){
       
        cell.middleImage.image = [UIImage imageNamed:@"plusbig_skyblue.png"];
        [cell.middleImage.layer setBorderColor:[[UIColor clearColor] CGColor]];
        [cell.middleImage.layer setBorderWidth:2.0];
        
    }else if([indexPath row]==(totalCount-1)){
        cell.middleImage.image = [[UIImage alloc] init];
        [cell.middleImage.layer setBorderColor:[[UIColor clearColor] CGColor]];
        [cell.middleImage.layer setBorderWidth:2.0];

    }else{
        NSString *filePath = [dataPath stringByAppendingPathComponent:[self.arrayThumbnails objectAtIndex:[indexPath row]-1]];
        cell.middleImage.image = [UIImage imageWithContentsOfFile:filePath];
        [cell.middleImage.layer setBorderColor:[skyblue CGColor]];
        [cell.middleImage.layer setBorderWidth:2.0];
    }
    
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([indexPath row]==0){
        NSLog(@"Go back");
        
        if(self.navigationController.navigationBarHidden)
        {
            //        NSLog(@"Navigation bar is hidden");
            [self.navigationController setNavigationBarHidden:NO animated:YES];
            [UIView animateWithDuration:0.3 animations:^{
                
            }];
        }
        else
        {
            //       NSLog(@"Navigation bar is not hidden");
        }

        //[self.parentViewController.navigationController popViewControllerAnimated:YES];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        
    }else if([indexPath row]==(totalCount-2)){
        NSLog(@"choose image");
        
        
        UIStoryboard *iPhoneStoryBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        ScreenshotsViewController *screenshots = [iPhoneStoryBoard instantiateViewControllerWithIdentifier:@"Screenshots"];
        
        screenshots.source = FROM_EDIT;
        EditScreenshotsViewController *edit = (EditScreenshotsViewController *)self.parentViewController;
        
        [edit openList:nil];
        [edit.navigationController pushViewController:screenshots animated:YES];

        
        
        
    }else if([indexPath row]==(totalCount-1)){
        NSLog(@"nothing");
    }else{
        
        //change images
        [_delegate selectImageAtIndex:([indexPath row]-1)];
        
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    
     if([indexPath row]!=0 && [indexPath row]!= (totalCount-1) && [indexPath row] != (totalCount-2)){
         return YES;
     }else{
         return NO;
     }
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        [self.arrayThumbnails removeObjectAtIndex:[indexPath row]-1];
        [self.thumbnailList deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [_delegate deleteImageAtIndex:([indexPath row]-1)];
    }
}

-(void) viewDidLayoutSubviews{
    
    CGRect tFrame = self.thumbnailList.frame;
    tFrame.size.height = slideMenuHeight;
    self.thumbnailList.frame = tFrame;
}

@end
