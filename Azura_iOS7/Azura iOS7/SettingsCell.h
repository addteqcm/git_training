//
//  SettingsCell.h
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/9/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsCell : UITableViewCell<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *homePageText;
@property (strong, nonatomic) IBOutlet UILabel *searchEngineNameText;
@property (strong, nonatomic) IBOutlet UISwitch *wifiTransferSwitch;

@end
