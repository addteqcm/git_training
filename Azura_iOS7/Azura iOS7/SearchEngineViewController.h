//
//  SearchEngineViewController.h
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/10/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchEngineCell.h"
@interface SearchEngineViewController : UITableViewController
{
    NSIndexPath *checkedIndexPath;
}
@property (nonatomic,retain) NSIndexPath* checkedIndexPath;
@property (nonatomic,assign) NSInteger _selectedRow;
@end
