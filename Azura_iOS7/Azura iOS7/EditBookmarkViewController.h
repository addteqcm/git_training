//
//  EditBookmarkViewController.h
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/3/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BookMarkObject.h"

@interface EditBookmarkViewController : UIViewController<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>
{
    UITextField *nameTextField;
    UITextField *urlTextField;
    UINavigationBar *naviBarObj;
    UIBarButtonItem *backButton;
    UINavigationController *naviController;
}

@property (nonatomic, retain) BookMarkObject *bookmark;
@property (nonatomic, retain) UITableView *tableView;

@end
