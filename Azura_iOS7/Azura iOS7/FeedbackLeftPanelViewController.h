//
//  FeedbackLeftPanelViewController.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/29/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "FeedbackLeftTwoViewController.h"


@protocol FeedbackLeftPanelViewControllerDelegate <NSObject>

@optional
- (void)imageSelected:(UIImage *)image withTitle:(NSString *)imageTitle withCreator:(NSString *)imageCreator;
- (void) ProjectSelected: (NSInteger) row;
- (void) ProjectSelectedFromSearch: (NSInteger) row;
- (void) expandViewForSearch;
-(void) shrinkView;
@end



@interface FeedbackLeftPanelViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate>
@property (nonatomic) BOOL firsttime;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBarProject;
@property (nonatomic, weak) id<FeedbackLeftPanelViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UITableView *projTable;
@property (strong, nonatomic) NSMutableArray *searchProjArray;
@end
