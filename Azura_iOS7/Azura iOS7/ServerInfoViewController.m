//
//  ServerInfoViewController.m
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/22/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "ServerInfoViewController.h"

#define STATE_NONE 0

#define kFeedbackIssueKey @"feedbackIssueKey"
#define kFeedbackState @"feedbackState"
#define kFeedbackWords @"feedbackWords"
#define kSearchWord @"searchWord"
#define kNewIssueProjName @"projectName"
#define kNewIssueIssuetype @"issuetype"

@interface ServerInfoViewController ()
@property CGFloat initHeight;
@end

@implementation ServerInfoViewController
@synthesize myServer;
@synthesize myPort;
@synthesize isHTTP;
@synthesize myUserName;
@synthesize myPassword;
@synthesize isRememberMe;
@synthesize tempField;
@synthesize client;
@synthesize keychainItem;
@synthesize testURL;
@synthesize initHeight;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
   
    [super viewDidLoad];
     [self.navigationController.view setBackgroundColor:self.view.backgroundColor];
    keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"JiraLogin" accessGroup:nil];
    [tempField.delegate self];
     self.initHeight = self.tableView.frame.size.height;
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    gestureRecognizer.cancelsTouchesInView = NO;
    [self.tableView addGestureRecognizer:gestureRecognizer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShown:) name:UIKeyboardWillShowNotification object:nil ];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHidden:) name:UIKeyboardWillHideNotification object:nil ];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    NSLog(@"here end editing");
    [textField resignFirstResponder];
   
    return YES;
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (void) viewWillAppear:(BOOL)animated {
    
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"isRememberMe"] == true) {
        //NSLog(@"Remembered username and password");
        NSString *saved_pass = [keychainItem objectForKey:(__bridge id)(kSecValueData)];
        //NSLog(@"%@",saved_pass);
        NSString *saved_user = [keychainItem objectForKey:(__bridge id)(kSecAttrAccount)];
        myUserName = saved_user;
        myPassword = saved_pass;
    }else{
        [keychainItem resetKeychainItem];
        myUserName = @"";
        myPassword = @"";
    }
    
    
    
}

-(void)willMoveToParentViewController:(UIViewController *)parent {
    //NSLog(@"This VC has has been pushed popped OR covered");
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 3;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    //If you want a header put write your logic here
    if(section==0){
        return @"Server Infomation";
    }else if(section==1){
        return @"Login Credentials";
    }
    return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0){
        if(indexPath.row==0){
            ServerInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"serverAddCell" forIndexPath:indexPath];
            cell.txtServer.tag=10;
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"myServer"] != nil){
                myServer=[[NSUserDefaults standardUserDefaults]objectForKey:@"myServer"];
                cell.txtServer.text= myServer;
            }
            cell.txtServer.delegate = self;
            return  cell;
        }
        if (indexPath.row==1) {
            ServerInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"portNoCell" forIndexPath:indexPath];
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"myPort"] != nil){
                myPort=[[NSUserDefaults standardUserDefaults]objectForKey:@"myPort"];
                cell.txtPortNo.text= myPort;
            }
            cell.txtPortNo.tag=20;
            cell.txtPortNo.delegate = self;
            return  cell;
        }
        if (indexPath.row==2) {
            ServerInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"httpsCell" forIndexPath:indexPath];
            if([[NSUserDefaults standardUserDefaults]boolForKey:@"isHttp"] == true){
                isHTTP=true;
                cell.switchHttps.on=YES;
            }else{
                isHTTP=false;
                cell.switchHttps.on=NO;
            }
            return  cell;
        }
    }
    if(indexPath.section==1){
        if(indexPath.row==0){
            
            ServerInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"usernameCell" forIndexPath:indexPath];
            //if([[NSUserDefaults standardUserDefaults]objectForKey:@"myUserName"] != nil){
            if(![myUserName isEqualToString:@""]){
                myUserName=[keychainItem objectForKey:(__bridge id)(kSecAttrAccount)];
                cell.txtUsername.text= myUserName;
            }
            cell.txtUsername.tag=40;
            cell.txtUsername.delegate=self;
            return  cell;
        }
        if (indexPath.row==1) {
            
            ServerInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"passwordCell" forIndexPath:indexPath];
            if(![myPassword isEqualToString:@""]){
                myPassword=[keychainItem objectForKey:(__bridge id)(kSecValueData)];
                cell.txtPassword.text=myPassword;
            }
            cell.txtPassword.tag=50;
            cell.txtPassword.delegate=self;
            return  cell;
        }
        if (indexPath.row==2) {
            ServerInfoCell *cell=[tableView dequeueReusableCellWithIdentifier:@"remembermeCell" forIndexPath:indexPath];
            if(cell==nil){
                return nil;
            }
            if([[NSUserDefaults standardUserDefaults]boolForKey:@"isRememberMe"] == true){
                isRememberMe=true;
                cell.switchRememberme.on=YES;
            }else{
                isRememberMe=false;
                cell.switchRememberme.on=NO;
            }
            return  cell;
        }
    }
    return nil;
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    tempField=textField;
    
    if(textField.tag==10){
        myServer=textField.text;
        
        NSLog(@"server: %@",myServer);
    }
    if(textField.tag==20){
        myPort=textField.text;
    }
    if(textField.tag==40){
        myUserName=textField.text;
        
    }
    if(textField.tag==50){
        myPassword=textField.text;
    }
    [textField resignFirstResponder];
    NSLog(@"resign");
}

-(void) scrollToCell: (NSIndexPath *) path{
    [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    /*
    [textField becomeFirstResponder];
    if(textField.tag==40){
        [self.tableView setContentOffset:CGPointMake(0, textField.frame.size.height*(3.5)) animated:NO];
        NSLog(@"hello %f", textField.frame.size.height);
        //        NSIndexPath *index =[NSIndexPath indexPathForRow:0 inSection:1];
        //        [self.tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    if(textField.tag==50){
        NSLog(@"gg %f", textField.frame.size.height);
        [self.tableView setContentOffset:CGPointMake(0, textField.frame.size.height*(3.5)) animated:NO];
    }
    */
    
    
    
    NSIndexPath *index;
    if(textField.tag == 40 || textField.tag ==50){
        index=[NSIndexPath indexPathForRow:0 inSection:1];
    }else{
        index=[NSIndexPath indexPathForRow:0 inSection:0];
    }
    [self performSelector:@selector(scrollToCell:) withObject:index];
    
}



-(BOOL)checkServer:(NSString *)server{
    /*
     NSString *urlRegEx =
     @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
     NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
     
     return [urlTest evaluateWithObject:server];
     */
    return TRUE;
}
-(BOOL)check{
    BOOL flag = true;
    
    // crash when user click done without any information.
    if(myServer != nil){
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL: [NSURL URLWithString:myServer]];
        NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL: [NSURL URLWithString:myServer]];
        for (NSHTTPCookie *cookie in cookies)
        {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
        }
    }
    
    if(myServer == nil || [myServer isEqualToString:@""] || ![self checkServer:myServer] ){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Server Address"
                                                        message:@"Please enter a valid server address"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        flag=false;
    }
    else if(myUserName == nil || [myUserName isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Username"
                                                        message:@"Please enter a valid Username"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        flag=false;
    }else if( myPassword == nil ||[myPassword isEqualToString:@""]){
       
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Password"
                                                        message:@"Please enter a valid Password"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        flag=false;
    }
    
     NSLog(@" username %@ mypassword %@", myUserName, myPassword );
    
    
    return flag;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a story board-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 
 */

- (IBAction)switchHttp:(id)sender {
    
    UISwitch *senderswitch = sender;
    
    if(senderswitch.on){
        isHTTP=true;
    }else{
        isHTTP=false;
    }
    
    
}

- (IBAction)switchRememberme:(id)sender {
    
    UISwitch *senderswitch = sender;
    
    if(senderswitch.on){
        isRememberMe=true;
    }else{
        isRememberMe=false;
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    
}

- (IBAction)saveServerInfo:(id)sender {
    
    NSIndexPath *index =[NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [self.tableView endEditing:YES];
    NSLog(@"Server: %@",myServer);
    NSLog(@"Port: %@",myPort);
    NSLog(@"HTTP");
    NSLog(isHTTP ? @"true" : @"false");
    NSLog(@"Username: %@",myUserName);
    NSLog(@"Password: %@",myPassword);
    NSLog(@"Remember Me");
    NSLog(isRememberMe ? @"true" : @"false");
    
    if([self check]){
        
        
        
        
        //Delete session if it exists:
        NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL: [NSURL URLWithString:myServer]];
        for (NSHTTPCookie *cookie in cookies)
        {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
        }
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        
        
        
        NSString *serverUrl;
        if(![myPort isEqual: @""]){
            
            NSString *temp = myServer;
            NSLog(@"********************************");
            
            if([myServer hasPrefix:@"http://" ]){
                NSRange httprange = [myServer rangeOfString:@"http://"];
                temp = [myServer substringFromIndex:httprange.location+httprange.length];
                isHTTP = false;
            }else if([myServer hasPrefix:@"https://" ]){
                NSRange httprange = [myServer rangeOfString:@"https://"];
                temp = [myServer substringFromIndex:httprange.location+httprange.length];
                NSLog(@"YEAS %@", temp);
                isHTTP = true;
            }
            
            NSRange prange = [temp rangeOfString:@"/"];
            if(prange.location != NSNotFound){
                // if there is a path
                NSString *front = [temp substringToIndex:prange.location];
                NSString *behind = [temp substringFromIndex:prange.location+prange.length-1];
                
                NSRange frange = [front rangeOfString:@":"];
                if(frange.location==NSNotFound){
                    // add port number here
                    temp = [NSString stringWithFormat:@"%@:%@%@",front, myPort, behind];
                }else{
                    myPort=@"";
                }
            }
            
            myServer = temp;
        }else{
            // no port number
            NSString *temp = myServer;
            if([myServer hasPrefix:@"http://" ]){
                NSRange httprange = [myServer rangeOfString:@"http://"];
                temp = [myServer substringFromIndex:httprange.location+httprange.length];
                isHTTP = false;
            }else if([myServer hasPrefix:@"https://" ]){
                NSRange httprange = [myServer rangeOfString:@"https://"];
                temp = [myServer substringFromIndex:httprange.location+httprange.length];
                NSLog(@"YEAS %@", temp);
                isHTTP = true;
            }
            myServer = temp;
        }
        if(isHTTP){
            myServer = [NSString stringWithFormat:@"%@%@", @"https://", myServer];
        }else{
            myServer = [NSString stringWithFormat:@"%@%@", @"http://", myServer];
        }
        
        serverUrl = myServer;
        
        NSLog(@"my URL: %@",serverUrl);
        
        client = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:myServer]];
        [client registerHTTPOperationClass:[AFJSONRequestOperation class]];
        [client setDefaultHeader:@"Accept" value:@"application/json"];
        [client setParameterEncoding:AFJSONParameterEncoding];
        NSString * myUrl = [[NSString alloc]initWithFormat:@"%@/rest/auth/latest/session",serverUrl];
        
        
        [client setParameterEncoding:AFFormURLParameterEncoding];
        [client setAuthorizationHeaderWithUsername:myUserName password:myPassword];
        [client getPath:myUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary* json = responseObject;
            NSLog(@"res login %@",responseObject);
            NSString *check = [json objectForKey:@"name"];
            NSRange checkRange = [check rangeOfString:myUserName];
            if (checkRange.location != NSNotFound || checkRange.location != 0) {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfully Connected"
                                                                message:@"Connection Success!"
                                                               delegate:self
                                                      cancelButtonTitle:@"Dismiss"
                                                      otherButtonTitles:nil];
                [alert show];
                
                
                NSString *prevUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"myServer"];
                if(![prevUrl isEqualToString:myServer]){
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setInteger:STATE_NONE forKey:kFeedbackState];
                    [defaults setValue:@"" forKey:kFeedbackWords];
                    [defaults setValue:@"" forKey:kNewIssueProjName];
                    [defaults setValue:@"" forKey:kNewIssueIssuetype];
                    [defaults setValue:@"" forKey:kFeedbackIssueKey];
                    [defaults setValue:@"" forKey:kSearchWord];

                }
                
                [[NSUserDefaults standardUserDefaults]setValue:myServer forKey:@"myServer"];
                [[NSUserDefaults standardUserDefaults]setBool:isHTTP forKey:@"isHttp"];
                [[NSUserDefaults standardUserDefaults]setBool:isRememberMe forKey:@"isRememberMe"];
                if(myPort!=NULL){
                    [[NSUserDefaults standardUserDefaults]setValue:myPort forKey:@"myPort"];
                }
                
                if(isRememberMe == true){
                    
                    //[keychainItem setObject:myPassword forKey:(__bridge id)(kSecValueData)];
                    [keychainItem setObject:myPassword forKey:(__bridge id)(kSecValueData)];
                    [keychainItem setObject:myUserName forKey:(__bridge id)(kSecAttrAccount)];
                    NSLog(@"Username:%@",[keychainItem objectForKey:(__bridge id)(kSecAttrAccount)]);
                    NSLog(@"uname %@",myUserName);
                    NSLog(@"pswd %@",myPassword);
                    //                    NSString *saved_pass = [keychainItem objectForKey:(__bridge id)(kSecValueData)];
                    //                    NSString *saved_user = [keychainItem objectForKey:(__bridge id)(kSecAttrAccount)];
                    //                    [[NSUserDefaults standardUserDefaults]setValue:myUserName forKey:@"myUserName"];
                    //                    [[NSUserDefaults standardUserDefaults]setValue:myPassword forKey:@"myPassword"];
                    // myUserName=saved_user;
                    //myPassword=saved_pass;
                }else{
                    //                    [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"myUserName"];
                    //                    [[NSUserDefaults standardUserDefaults]setValue:nil forKey:@"myPassword"];
                    [keychainItem resetKeychainItem];
                    myUserName=@"";
                    myPassword=@"";
                    
                }
                [self.tableView reloadData];
            }
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"%@",error);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cannot Connect To Server"
                                                            message:@"Invalid Credentials and/or No internet"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL: [NSURL URLWithString:myServer]];
            for (NSHTTPCookie *cookie in cookies)
            {
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
            }

        }];
        
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }else{
        NSLog(@"WHAT?");
    }
    
     NSString *serverUrl;
     if(![myPort isEqual: @""]){
     serverUrl = [NSString stringWithFormat:@"%@:%@",myServer,myPort];
     }else{
     serverUrl=myServer;
     }
     
}



/*
-(void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(scrollView.contentOffset.y >= 0) {
        NSLog(@"Up");
        NSIndexPath *index =[NSIndexPath indexPathForRow:0 inSection:1];
        [self.tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }else{
        NSLog(@"Down");
        
    }
    
}
*/


-(void) keyboardShown: (NSNotification *)notification{
    NSLog(@"Show Keyboard");
    NSDictionary *userInfo = notification.userInfo;
    NSTimeInterval duration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];

    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.initHeight =self.tableView.frame.size.height;
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        
        CGRect f =self.tableView.frame;
        f.size.height -= kbSize.height;
        self.tableView.frame = f;
    } completion:nil];

}
-(void) keyboardHidden: (NSNotification *)notification{
    NSLog(@"Hide keyboard");
    
    NSDictionary *userInfo = notification.userInfo;
    NSTimeInterval duration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        
        CGRect f =self.tableView.frame;
        f.size.height = self.initHeight;
        self.tableView.frame = f;
     } completion:nil];
}
-(void) hideKeyboard{
    [self.tableView endEditing:YES];
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Dismiss"]){
        NSLog(@"dismiss");
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}

@end
