//
//  ICarouselViewController.m
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/20/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "ICarouselViewController.h"
#import "WebViewDetails.h"

@interface ICarouselViewController ()
//@property (nonatomic,strong) NSMutableArray *items;
@property (nonatomic,strong) NSMutableArray *itemDetails;
@end

@implementation ICarouselViewController
@synthesize carousel;
@synthesize toolbar;
@synthesize webView;
@synthesize currentImage;
@synthesize parentView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    NSLog(@"In Carousel View Controller");
//    self.items = [[NSMutableArray alloc] initWithCapacity:16];
    self.itemDetails = [[NSMutableArray alloc] initWithCapacity:16];
    carousel.type = iCarouselTypeCoverFlow;
//    NSLog(@"Size of item images : %d",[self.itemDetails count]);
    // Do any additional setup after loading the view from its nib.
    self.toolbar.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [self.itemDetails count];
 //   return [self.items count];
}

-(UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    
//    NSLog(@"Rotated and Reloading ......... Index : %d",index);

    UIButton *button = nil;
    UILabel *label = nil;
    UIImageView *imageView = nil;
//    UILabel *labelCount = nil;
    
    if (UIInterfaceOrientationIsPortrait(interfaceOrientation))
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0,0,200.0f,260.0f)];
        label = [[UILabel alloc] initWithFrame:CGRectMake(0,0,175.0f,25.0f)];
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,25,200.0f,235.0f)];
        button = [[UIButton alloc] initWithFrame:CGRectMake(175,0,25.0f,25.0f)];
  //      labelCount = [[UILabel alloc] initWithFrame:CGRectMake(0,280,200.0f,25.0f)];
        view.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        view.layer.shadowColor = [UIColor grayColor].CGColor;
        view.layer.shadowOffset = CGSizeMake(0,2);
        view.layer.shadowOpacity = 1;
        view.layer.shadowRadius = 8.0;
    //    NSLog(@"x of the view : %f",view.frame.origin.x);
    //    NSLog(@"y of the view : %f",view.frame.origin.y);
    //    NSLog(@"Width of the view : %f",view.frame.size.width);
    //    NSLog(@"Height of the view : %f",view.frame.size.height);
       //     imageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
       //     imageView.layer.borderWidth = 3.0f;
       //     imageView.contentMode = UIViewContentModeRedraw;
        imageView.autoresizesSubviews = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        imageView.image = ((WebViewDetails *)[self.itemDetails objectAtIndex:index]).webViewImage;
        [view addSubview:imageView];
        label.backgroundColor = [UIColor colorWithRed:225/256.0 green:227/256.0 blue:238/256.0 alpha:1.0];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:15];
        label.textColor = [UIColor blueColor];
        label.text = ((WebViewDetails *)[self.itemDetails objectAtIndex:index]).webViewTitle;
        [view addSubview:label];
            
        [button setImage:[UIImage imageNamed:@"icon-remove.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(deleteTab:)
             forControlEvents:(UIControlEvents)UIControlEventTouchDown];
        button.backgroundColor = [UIColor colorWithRed:225/256.0 green:227/256.0 blue:238/256.0 alpha:1.0];
        [view addSubview:button];
        
   //     labelCount.text = [NSString stringWithFormat:@"%d", index];
    //    [view addSubview:labelCount];
    }
    else
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0,0,240.0f,200.0f)];
        label = [[UILabel alloc] initWithFrame:CGRectMake(0,0,215.0f,25.0f)];
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 25, 240.0f, 175.0f)];
        button = [[UIButton alloc] initWithFrame:CGRectMake(215, 0, 25.0f, 25.0f)];
  //       labelCount = [[UILabel alloc] initWithFrame:CGRectMake(0,230,240.0f,25.0f)];
  //      NSLog(@"%d",self.carousel.numberOfItems);
        view.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        view.layer.shadowColor = [UIColor grayColor].CGColor;
        view.layer.shadowOffset = CGSizeMake(0,2);
        view.layer.shadowOpacity = 1;
        view.layer.shadowRadius = 8.0;
   //     NSLog(@"x of the view : %f",view.frame.origin.x);
   //     NSLog(@"y of the view : %f",view.frame.origin.y);
   //     NSLog(@"Width of the view : %f",view.frame.size.width);
   //     NSLog(@"Height of the view : %f",view.frame.size.height);
      //      view.layer.borderColor = [UIColor lightGrayColor].CGColor;
      //      view.layer.borderWidth = 3.0f;
        imageView.autoresizesSubviews = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        imageView.image = ((WebViewDetails *)[self.itemDetails objectAtIndex:index]).webViewImage;
        [view addSubview:imageView];
        label.backgroundColor = [UIColor colorWithRed:225/256.0 green:227/256.0 blue:238/256.0 alpha:1.0];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:15];
        label.textColor = [UIColor blueColor];
        label.text = ((WebViewDetails *)[self.itemDetails objectAtIndex:index]).webViewTitle;
        [view addSubview:label];
        
        [button setImage:[UIImage imageNamed:@"icon-remove.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(deleteTab:)
             forControlEvents:(UIControlEvents)UIControlEventTouchDown];
        button.backgroundColor = [UIColor colorWithRed:225/256.0 green:227/256.0 blue:238/256.0 alpha:1.0];
        button.layer.borderColor = [UIColor blackColor].CGColor;
        [view addSubview:button];
        
     //   labelCount.text = [NSString stringWithFormat:@"%d", index];
     //   [view addSubview:labelCount];
    }
    return view;

}

-(void)checkNumberOfTabs
{
    if([carousel numberOfItems] >= 16)
    {
    //    NSLog(@"Hereeee...");
        self.newtab.enabled = NO;
    }
    else
    {
        if(!self.newtab.enabled)
        {
            self.newtab.enabled = YES;
        }
    }
}

//-(void)insertTab:(NSMutableArray *)array webViewDetails:(NSMutableArray *)detailsArray//(UIWebView *)webV
-(void)insertTab:(NSMutableArray *)detailsArray
{
    for (int index=0; index < [detailsArray count]; index++) {
    //    self.webView = (UIWebView *)[detailsArray objectAtIndex:index];
    //    self.itemDetails = detailsArray;
        WebViewDetails *webD = (WebViewDetails *)[detailsArray objectAtIndex:index];
    //    NSLog(@"From webview details : %@",webD.webViewTitle);
    //    NSLog(@"%@",[self.webView stringByEvaluatingJavaScriptFromString:@"document.title"]);
        NSInteger index = MAX(0, carousel.numberOfItems);
    //    NSLog(@"At index : %d",[carousel numberOfItems]);
        if(index < 16)
        {
    //        [self.items insertObject:self.webView atIndex:index];
            [self.itemDetails insertObject:webD atIndex:index];
            [carousel insertItemAtIndex:index animated:YES];
            [self checkNumberOfTabs];
        }
        else
        {
            UIAlertView *alertDialog;
            alertDialog = [[ UIAlertView alloc]
                           initWithTitle: @"Number of tabs exceeded"
                           message:@"Select one of the existing tabs to continue"
                           delegate: nil
                           cancelButtonTitle: @" Ok"
                           otherButtonTitles: nil];
            [alertDialog show];
        }
    }
   // self.webView = webV;
}

- (IBAction)insertTabs:(id)sender {

    [self.delegate newTabCarouselViewController:self didCreateANewTab:[carousel numberOfItems]];
//    NSLog(@"%d",[carousel numberOfItems]);
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)resetTabs
{
    int count = [carousel numberOfItems];
 //   NSLog(@"COUNT : %d",count);
    for(int index=count-1;index>=0;index--)
    {
 //       NSLog(@"Removing item at index : %d",index);
    //    [self.items removeObjectAtIndex:index];
        [self.itemDetails removeObjectAtIndex:index];
        [carousel removeItemAtIndex:index animated:YES];
    }
    [self checkNumberOfTabs];
}

- (IBAction)doneTab:(id)sender
{
    [self.delegate doneTabCarouselViewController:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)deleteTab:(id)sender
{
//    NSLog(@"Delete tab pressed");
    if (carousel.numberOfItems > 1)
    {
        NSInteger index = carousel.currentItemIndex;
    //    [self.items removeObjectAtIndex:index];
        [self.itemDetails removeObjectAtIndex:index];
        [carousel removeItemAtIndex:index animated:YES];
        [self.delegate deleteTabCarouselViewController:self didDeleteTabInCarousel:index];
    }
    [self checkNumberOfTabs];
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
 //   NSLog(@"Tapped view number: %d", index);
    [self.delegate selectTabCarouselViewController:self didSelectTabInCarousel:index];
//    NSLog(@"%d",[self.carousel numberOfItems]);
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (CATransform3D)carousel:(iCarousel *)_carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * carousel.itemWidth);
}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return YES;
        }
        case iCarouselOptionSpacing:
        {
            if(UIInterfaceOrientationIsPortrait(interfaceOrientation))
            {
                //add a bit of spacing between the item views
                return value * 1.05f;
            }
            else
            {
                return value * 1.25f;
            }
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        default:
        {
            return value;
        }
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if(toInterfaceOrientation == UIInterfaceOrientationPortrait)
    {
        NSLog(@"to portrait");
        [carousel reloadData];
    }
    else
    {
        NSLog(@"to landscape");
 //       [carousel reloadData];

    }
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if(fromInterfaceOrientation == UIInterfaceOrientationPortrait)
    {
        NSLog(@"from portrait");
        [carousel reloadData];
    }
    else
    {
        NSLog(@"from landscape");
        [carousel reloadData];
    }
    
}

@end
