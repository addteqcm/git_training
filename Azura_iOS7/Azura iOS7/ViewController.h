//
//  ViewController.h
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 8/26/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddBookmarkViewController.h"
#import "ShowBookmarkViewController.h"
#import "SettingsViewController.h"
#import "sqlite3.h"
#import "ICarouselViewController.h"
#import "NavigationController.h"
#import "ShowFeedbackViewController.h"
#import "EditScreenshotsViewController.h"
@class WebViewDetails;

@interface ViewController : UIViewController<UITextFieldDelegate,UIWebViewDelegate,UIScrollViewDelegate,UIActionSheetDelegate,ShowBookmarkViewControllerDelegate,UITableViewDataSource, UITableViewDelegate,iCarouselViewControllerDelegate>
{
    NSUserDefaults *userDefaults;
    
    UIButton *backButton;
    UIButton *forwardButton;
    UIButton *refreshButton;
    UIButton *homeButton;
    UIBarButtonItem *back;
    UIBarButtonItem *forward;
    
    AddBookmarkViewController *addBookmarkViewController;
    ShowBookmarkViewController *showBookmarkViewController;
    ICarouselViewController *carouselViewController;
    
    sqlite3 *DB;
    NSString *databasePath;
    NSMutableArray *webViewDetailsArray;
}

-(void)createDatabase;

@property (strong, nonatomic) WebViewDetails *webViewDetails;
@property (strong, nonatomic) IBOutlet UITextField *locationField;
@property (strong, nonatomic) IBOutlet UINavigationItem *navItem;
//@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, retain) NSString *URLString;
@property (nonatomic, assign) NSUInteger currentIndexOfWebView;
@property (nonatomic, assign) NSUInteger previousIndexOfWebView;
@property (nonatomic, assign) CGRect dimensionOfWebview;
@property (nonatomic) CGFloat lastContentOffset;



- (IBAction)goToAnnotateImage:(id)sender;
- (IBAction)saveImageInFolder:(id)sender;
- (IBAction)goToShowFeedbacks:(id)sender;
- (IBAction)goToSettings:(id)sender;
- (IBAction)goToBookmarks:(id)sender;
- (IBAction)showAllTabs:(id)sender;
-(void)refresh;
-(void)goToHomePage;
-(UIImage *)getImageOfWebView:(UIWebView *)webV;
@property (nonatomic, retain) NSMutableArray *autocompleteArray;
@property (nonatomic, retain) NSMutableArray *urlsInDB;
@property (nonatomic, retain) UITableView *autocompleteTableView;
@property (nonatomic, retain) NSMutableArray *arrayOfWebViews;

@end
