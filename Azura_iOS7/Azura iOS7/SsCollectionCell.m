//
//  SsCollectionCell.m
//  Azura iOS7
//
//  Created by HeeJinChoi on 1/16/14.
//  Copyright (c) 2014 addteq. All rights reserved.
//

#import "SsCollectionCell.h"

@implementation SsCollectionCell
@synthesize selectedFlag;
@synthesize filename;
/*
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
 */
-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        UIView *bgView = [[UIView alloc] initWithFrame:self.backgroundView.frame];
        //bgView.backgroundColor = [UIColor whiteColor];
        bgView.layer.borderColor = [[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0] CGColor];
        bgView.layer.borderWidth = 4;
        self.selectedBackgroundView = bgView;
        
        
        
        
    }
    return self;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


@end
