//
//  FullSSViewController.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 3/13/14.
//  Copyright (c) 2014 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullSSViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *fullImage;
@property (strong, nonatomic) NSString *openImage;
@property (strong, nonatomic) NSMutableArray *imageLists;
- (IBAction)swipeLeft:(id)sender;
- (IBAction)swipeRight:(id)sender;
@end
