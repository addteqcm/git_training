//
//  ScreenshotsViewController.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 1/15/14.
//  Copyright (c) 2014 addteq. All rights reserved.
//

#import "SsCollectionCell.h"

@protocol ScreenshotsViewControllerDelegate <NSObject>

@optional
-(void) selectImagesFromFeedback:(NSMutableArray *)arraySelected;

@end

@interface ScreenshotsViewController : UIViewController  <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) id<ScreenshotsViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UICollectionView *imageList;
@property (strong, nonatomic) NSMutableArray *arrayImages;
@property (strong, nonatomic) NSMutableArray *arraySelected;
@property (strong, nonatomic) NSMutableArray *arrayDeleted;

@property (strong, nonatomic) NSString *dataPath;
@property (strong, nonatomic) UIView *draggingImage;
@property (strong, nonatomic) NSString *curPath;
@property (strong, nonatomic) NSString *curFileName;
@property (strong, nonatomic) UIColor *skyblue;

@property (nonatomic) int source;

- (IBAction)doneChoosing:(id)sender;
- (IBAction)cancelChoosing:(id)sender;
- (IBAction)deleteScreenshots:(id)sender;

-(void)droppedGesture:(UIGestureRecognizer *)gesture;

-(void)draggingGestureWillBegin:(UIGestureRecognizer *)gesture forCell:(SsCollectionCell *)cell;
-(void)draggingGestureDidMove:(UIGestureRecognizer *)gesture;
-(void)draggingGestureDidEnd:(UIGestureRecognizer *)gesture;

//-(UIView *)dragAndDropTableViewControllerView:(DragAndDropTableViewController *)ddtvc;
//-(id)dragAndDropTableViewControllerSelectedItem:(DragAndDropTableViewController *)ddtvc;
@end
