//
//  AddBookmarkViewController.h
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 8/30/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BookMarkObject.h"

@interface AddBookmarkViewController : UIViewController<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>
{
    BookMarkObject *bookmark;
    NSMutableArray *bookmarkArray;
    UITextField *nameTextField;
    UINavigationBar *naviBarObj;
}

@property (retain,nonatomic) BookMarkObject *bookmark;
@property (retain, nonatomic)  UITableView *tableview;

- (void)setBookmark:(NSString *)aName url:(NSURL *)aURL;

@end