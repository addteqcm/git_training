//
//  NavigationController.h
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/26/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationController : UINavigationController
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;

@property (nonatomic)NSInteger selectedProj;
@property (nonatomic)NSInteger selectedType;

@property (strong, nonatomic) NSString *stringSelectedProj;
@property (strong, nonatomic) NSString *stringSelectedType;

@property (strong, nonatomic) NSString *existingissuekey;


@property (strong, nonatomic) NSString *feedbackWords;
@property (strong, nonatomic) NSMutableArray *feedbackImages;
@property (strong, nonatomic) NSData *feedbackAudio;
@property (nonatomic) int feedbackState;







@end
