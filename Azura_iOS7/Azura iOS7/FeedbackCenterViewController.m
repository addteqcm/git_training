//
//  FeedbackCenterViewController.m
//  Azura iOS7
//
//  Created by HeeJinChoi on 2/12/14.
//  Copyright (c) 2014 addteq. All rights reserved.
//

#import "FeedbackCenterViewController.h"
#import "ScreenshotsViewController.h"
#import "FullSSViewController.h"
#import "UIImage+JMCResize.h"
#import "JMCAttachmentItem.h"

#import "JMCMacros.h"
#import "Projects.h"
#import "Plans.h"

#define STATE_NONE 0
#define STATE_NEWISSUE 1
#define STATE_EXISTING 2

#define FROM_FEEDBACK 3

#define MAXLENGTH 50

#define kFeedbackIssueKey @"feedbackIssueKey"
#define kFeedbackState @"feedbackState"
#define kFeedbackWords @"feedbackWords"
#define kFeedbackAudio @"feedbackAudio"

#define kFeedbackImages @"feedbackImages"
#define kFeedbackImageState @"feedbackImageState"

#define kNewIssueProjName @"projectName"
#define kNewIssueIssuetype @"issuetype"

@interface FeedbackCenterViewController ()
@property (nonatomic) CGFloat keyboardSizeY;
@end

@implementation FeedbackCenterViewController
@synthesize actionSh;

@synthesize textViewOrigin;
@synthesize previousFlag;
@synthesize hud;

@synthesize origin;
@synthesize bubbleTable;
@synthesize bottomInset;
@synthesize bubbleData;

@synthesize issuekey;
@synthesize issuetitle;

@synthesize user;
@synthesize textField;
@synthesize color;
@synthesize files;
@synthesize fileCreated;
@synthesize fileAttachedFlag;
@synthesize recorder;
@synthesize audiofile;

@synthesize arrayOfComments;
@synthesize changeFlag;
@synthesize feedbackWordsString;
@synthesize feedbackStateInt;

@synthesize projectsArray;
@synthesize client;
@synthesize serverUrl;
@synthesize keychainItem;

@synthesize defaults;
@synthesize imageFileNames;

@synthesize selectedProject;
@synthesize selectedIssueType;

@synthesize keyboardSizeY;
@synthesize screenshotList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.textField.delegate=self;
    
    changeFlag = false;
    self.leftButton.tag=1;
    self.rightButton.tag=1;
    
    textField.layer.borderWidth=1.0f;
    textField.layer.borderColor=[[UIColor colorWithRed:0.0f green:122.0f/255.0f blue:1.0f alpha:1.0f] CGColor];
    textField.layer.cornerRadius = 10.0f;
    
    // keyboard function
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShown:) name:UIKeyboardWillShowNotification object:nil ];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHidden:) name:UIKeyboardWillHideNotification object:nil ];
    
    
    
    // when user clicks view, hide keyboard
    UITapGestureRecognizer *tapGR;
    tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGR.numberOfTapsRequired = 1;
    [self.maintableview addGestureRecognizer:tapGR];
    [self.maintableview setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_watermark"]]];
    defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"myServer"]!=nil){
        serverUrl = [defaults objectForKey:@"myServer"];
        
        // color for bubbleTable
        color = [UIColor colorWithRed:42.0/255.0 green:42.0/255.0 blue:42.0/255.0 alpha:1.0];
        
        // username to compare with comment's author
        keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"JiraLogin" accessGroup:nil];
        user = [keychainItem objectForKey:(__bridge id)(kSecAttrAccount)];
        NSLog(@"user : %@", user);
        
        arrayOfComments = [[NSMutableArray alloc] init];
        
        // to store existing attachment image files
        // and show the image in bubble when encountering comments
        // using "!filename.png|thumbnail!" or something else mentions filename.png
        files = [[NSMutableArray alloc] init];
        fileCreated = [[NSMutableArray alloc] init];
        fileAttachedFlag = [[NSMutableArray alloc]init];
        
        client = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:serverUrl]];
        [client registerHTTPOperationClass:[AFJSONRequestOperation class]];
        [client setDefaultHeader:@"Accept" value:@"application/json"];
        [client setParameterEncoding:AFJSONParameterEncoding];
        
        // to store the projects for choosing project and issuetype(left side sliding menu)
        projectsArray = [NSMutableArray new];
        screenshotList = [NSMutableArray new];
        
        // bubble inset
        bottomInset=30;
        
        
        
        // when user was seeing some issue before, it uploads comments of issue.
        defaults = [NSUserDefaults standardUserDefaults];
        feedbackStateInt = [defaults integerForKey:kFeedbackState];
        feedbackWordsString = [defaults stringForKey:kFeedbackWords];
        
        // store projects information to "projectsArray"
        [self getProjects];
        
        if(feedbackWordsString == NULL){
            feedbackWordsString = @"";
        }else{
            self.textField.text = feedbackWordsString;
        }
        
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *path = [docDir stringByAppendingPathComponent:@"audio.aac"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if([fileManager fileExistsAtPath:path]){
            NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:path];
            audiofile = [fileHandle availableData];
            [fileHandle closeFile];
            [fileManager removeItemAtPath:path error:NULL];
        }
        
        imageFileNames = [NSMutableArray arrayWithArray:[defaults objectForKey:kFeedbackImages]];
        if(feedbackStateInt == STATE_EXISTING){
            issuekey = [defaults stringForKey:kFeedbackIssueKey];
            [self getComments:issuekey];
        }
    }else{
        UIAlertView *alertDialog;
        alertDialog = [[UIAlertView alloc]
                       initWithTitle:@"Error"
                       message:@"Server address is empty"
                       delegate: nil
                       cancelButtonTitle: @"Dismiss"
                       otherButtonTitles: nil];
        alertDialog.alertViewStyle=UIAlertViewStyleDefault;
        [alertDialog show];
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AVAudioRecorderDelegate Methods

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)avRecorder successfully:(BOOL)success
{
    audiofile = [recorder audioData];
    [recorder cleanUp];
    
    if(feedbackStateInt==STATE_EXISTING){
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"yyyy-MM-dd-HH.mm.ss.a"];
        NSLog(@"%@", [format stringFromDate:[[NSDate alloc]init]]);
        NSString *attachURL = [[NSString alloc] initWithFormat:@"%@/rest/api/2/issue/%@/attachments",serverUrl, issuekey];
        
        if(audiofile){
            // Attach the images
            NSString *mimetype = @"audio/aac";
            NSString *receiptName = [[NSString alloc] initWithFormat:@"recording-%@.aac", [format stringFromDate:[[NSDate alloc]init]]];
            
            AFHTTPClient *myclient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:attachURL]];
            [myclient setDefaultHeader:@"X-Atlassian-Token" value:@"nocheck"];
            NSURLRequest *request =
            [myclient multipartFormRequestWithMethod:@"POST"
                                                path:attachURL
                                          parameters:nil
                           constructingBodyWithBlock: ^(id<AFMultipartFormData> formData) {
                               [formData appendPartWithFileData:audiofile name:@"file" fileName:receiptName mimeType:mimetype];
                               //[formData appendPartWithFormData:[[NSNumber numberWithInt:imageData.length].stringValue dataUsingEncoding:NSUTF8StringEncoding] name:@"filelength"];
                           }];
            
            AFHTTPRequestOperation *operation1 =
            [myclient HTTPRequestOperationWithRequest:request
                                              success:^(AFHTTPRequestOperation *operation2, id json) {
                                                  NSLog(@"All images Attached OK");
                                                  UIImage *thumbnail = [UIImage imageNamed:@"audio_attachment"];
                                                  NSDate *now = [[NSDate alloc]init];
                                                  NSBubbleData *heyBubble = [NSBubbleData dataWithImage:thumbnail date:now type:BubbleTypeMine];
                                                  
                                                  [bubbleData addObject:heyBubble];
                                                  [bubbleTable reloadData];
                                                  textField.text = @"";
                                                  textField.placeholder=@"Type your comment here";
                                                  [textField resignFirstResponder];
                                                  if((bubbleTable.contentSize.height-bubbleTable.bounds.size.height) > 0 ){
                                                      CGPoint bottomOffset = CGPointMake(0, bubbleTable.contentSize.height-bubbleTable.bounds.size.height+bottomInset);
                                                      [bubbleTable setContentOffset:bottomOffset animated:NO];
                                                  }
                                                  audiofile=nil;
                                              }
                                              failure:^(AFHTTPRequestOperation *operation2, NSError *error) {
                                                  NSLog(@"All failed %@", error);
                                              }];
            
            [myclient enqueueHTTPRequestOperation:operation1];
        }
        changeFlag=false;
    }else{
        changeFlag=true;
    }
    
}




- (IBAction)showHideRightPanel:(id)sender {
    [textField endEditing:YES];
    UIButton *button = sender;
	switch (button.tag) {
		case 0: {
			[_delegate movePanelToOriginalPosition];
            
			break;
		}
			
		case 1: {
			[_delegate movePanelLeft];
			break;
		}
            
		default:
			break;
	}
    
}

- (IBAction)showHideLeftPanel:(id)sender {
    [textField endEditing:YES];
    UIButton *button = sender;
    switch (button.tag) {
		case 0: {
            [_delegate movePanelToOriginalPosition];
			break;
		}
			
		case 1: {
			[_delegate movePanelRight];
			break;
		}
			
		default:
			break;
	}
}


- (IBAction)moveLeftPanel:(id)sender {
    [textField endEditing:YES];
    UIButton *button = self.leftButton;
    switch (button.tag) {
		case 0: {
            [_delegate movePanelToOriginalPosition];
			break;
		}
			
		case 1: {
            if(self.rightButton.tag!=0){
                [_delegate movePanelRight];
            }else{
                [_delegate movePanelToOriginalPosition];
            }
			break;
		}
			
		default:
			break;
	}
    
}

- (IBAction)moveRightPanel:(id)sender {
    [textField endEditing:YES];
    UIButton *button = self.rightButton;
	switch (button.tag) {
		case 0: {
			[_delegate movePanelToOriginalPosition];
            
			break;
		}
			
		case 1: {
            if(self.leftButton.tag!=0){
               [_delegate movePanelLeft];
            }else{
                [_delegate movePanelToOriginalPosition];
            }
			
			break;
		}
            
		default:
			break;
	}
}

-(void) expandViewForSearch{
    [_delegate expandPanel];
}

-(void) shrinkView{
    [_delegate shrinkPanel];
}

-(void)handleTap:(UITapGestureRecognizer *)sender{
    
    // hide keyboard
    if(sender.state == UIGestureRecognizerStateEnded){
        
        [self.view endEditing:YES];
        
        if(self.leftButton.tag==0 || self.rightButton.tag==0){
            [_delegate movePanelToOriginalPosition];
        }
                
    }
}

-(void) selectExistingIssue: (Plans *) p{
    self.changeFlag = true;
    self.issuekey = p.label;
    self.issuetitle = p.value;
    
    [self getComments:self.issuekey];
    
    [_delegate movePanelToOriginalPosition];
    
    feedbackStateInt = STATE_EXISTING;
}

-(void) keyboardShown: (NSNotification *)notification{
    NSLog(@"show keyboard");
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    origin = self.maintableview.frame;
    textViewOrigin = self.mainsubview.frame;
    keyboardSizeY = kbSize.height;
    CGFloat viewHeight = bubbleTable.bounds.size.height-kbSize.height-self.mainsubview.frame.size.height;
    
    [UIView animateWithDuration:0.25f animations:^{
        
        
        // if it is not in searching(not right sliding menu)
       // if(right.searchStart!=true){
            
            if((bubbleTable.contentSize.height-bubbleTable.bounds.size.height)>=0){
                
                CGFloat yoffset = self.maintableview.contentOffset.y+kbSize.height;
                self.maintableview.frame = CGRectMake(origin.origin.x, origin.origin.y, origin.size.width, origin.size.height-kbSize.height);
                self.maintableview.contentOffset = CGPointMake(0, yoffset);
                self.mainsubview.frame = CGRectMake(textViewOrigin.origin.x, textViewOrigin.origin.y-kbSize.height, textViewOrigin.size.width, textViewOrigin.size.height);
                previousFlag=1;
                
                // NSLog(@"case 1 content size is big");
            }else{
                if((bubbleTable.contentSize.height-viewHeight) > 0 ){

                    CGPoint bottomOffset;
                    if((bubbleTable.contentSize.height-bubbleTable.bounds.size.height) > 0 ){
                        bottomOffset = CGPointMake(0, bubbleTable.contentSize.height-bubbleTable.bounds.size.height);
                    }else{
                        bottomOffset = CGPointMake(0, 0);
                    }
                    [bubbleTable setContentOffset:bottomOffset animated:NO];
                    self.maintableview.frame = CGRectMake(origin.origin.x, origin.origin.y, origin.size.width, origin.size.height-kbSize.height);
                    self.mainsubview.frame = CGRectMake(textViewOrigin.origin.x, textViewOrigin.origin.y-kbSize.height, textViewOrigin.size.width, textViewOrigin.size.height);
                    previousFlag=2;
                    
                    //NSLog(@"case 2 content size is not bigger than screen view but needs to move for keyboard");
                }else{
                    NSLog(@"333");
                    CGRect newfr = self.mainsubview.frame;
                    newfr.origin.y -= kbSize.height;
                    self.mainsubview.frame = newfr;
                    previousFlag=3;
                    //NSLog(@"case 3 content size is small so just move input view");
                }
            }
        //}
    }];
    
    
}
-(void) keyboardHidden: (NSNotification *)notification{
    // if it is not in searching(not right sliding menu)
    [UIView animateWithDuration:0.25f animations:^{
            
        if(previousFlag==1){
            
            
            self.maintableview.contentOffset= CGPointMake(self.maintableview.contentOffset.x, self.maintableview.contentOffset.y - keyboardSizeY);
            self.maintableview.frame = origin;
            self.mainsubview.frame = textViewOrigin;
        }else if(previousFlag==2){
            self.maintableview.frame = origin;
            self.mainsubview.frame = textViewOrigin;
            CGPoint bottomOffset;
            if((bubbleTable.contentSize.height-bubbleTable.bounds.size.height) > 0 ){
                bottomOffset = CGPointMake(0, bubbleTable.contentSize.height-bubbleTable.bounds.size.height);
            }else{
                bottomOffset = CGPointMake(0, 0);
            }
            [bubbleTable setContentOffset:bottomOffset animated:NO];
                
        }else if(previousFlag==3){
            self.mainsubview.frame = textViewOrigin;
        }
            
    }];
    
    
}

- (void) ProjectSelected: (NSInteger) row{
    self.selectedProject = row;
    

    [_delegate rotatePanel];
    
}

- (void) ProjectSelectedFromSearch: (NSInteger) row{
    self.selectedProject = row;
    
    [_delegate rotatePanelFromSearch];
    
}

-(void) backButtonIsclicked{
    [_delegate goBackToProject];
}

-(void) IssueTypeSelected:(NSInteger)row{
    self.selectedIssueType = row;
    self.changeFlag = true;
    
    
    
    [_delegate movePanelToOriginalPosition];
    
    feedbackStateInt = STATE_NEWISSUE;
    
    [arrayOfComments removeAllObjects];
    
    bubbleData = [[NSMutableArray alloc] initWithArray:arrayOfComments];
    bubbleTable.bubbleDataSource = self;
    bubbleTable.contentInset=UIEdgeInsetsMake(0, 0, bottomInset, 0);
    
    // The line below sets the snap interval in seconds. This defines how the bubbles will be grouped in time.
    // Interval of 120 means that if the next messages comes in 2 minutes since the last message, it will be added into the same group.
    // Groups are delimited with header which contains date and time for the first message in the group.
    
    bubbleTable.snapInterval = 120;
    
    // The line below enables avatar support. Avatar can be specified for each bubble with .avatar property of NSBubbleData.
    // Avatars are enabled for the whole table at once. If particular NSBubbleData misses the avatar, a default placeholder will be set (missingAvatar.png)
    
    bubbleTable.showAvatars = NO;
    
    // Uncomment the line below to add "Now typing" bubble
    // Possible values are
    //    - NSBubbleTypingTypeSomebody - shows "now typing" bubble on the left
    //    - NSBubbleTypingTypeMe - shows "now typing" bubble on the right
    //    - NSBubbleTypingTypeNone - no "now typing" bubble
    
    bubbleTable.typingBubble = NSBubbleTypingTypeNobody;
    
    /*
     if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
     CGRect bframe = self.bubbleTable.frame;
     bframe.size.height = [UIScreen mainScreen].bounds.size.height-99;
     self.bubbleTable.frame = bframe;
     
     CGRect inputframe = self.textInputView.frame;
     inputframe.origin.y = self.bubbleTable.frame.size.height;
     inputframe.size.height = 35;
     self.textInputView.frame = inputframe;
     NSLog(@"%f %f %f %f ", inputframe.origin.x,inputframe.origin.y, inputframe.size.width,     inputframe.size.height );
     
     [self.overView addSubview:self.bubbleTable];
     [self.overView addSubview:self.textInputView];
     
     [self.textInputView addSubview:self.textField];
     [self.textInputView addSubview:self.sendButton];
     }
     */
    
    [bubbleTable reloadData];
    //NSLog(@"1111111 Bubble table %d", [bubbleData count]);
        
    self.feedbackTitle.text = @"Summary";
    
    
    Projects *p = projectsArray[self.selectedProject];
    IssueType *itype = p.issuetypes[self.selectedIssueType];
    
    
    [self.leftButton setTitle:p.projectkey forState:UIControlStateNormal];
    [self.rightButton setTitle:itype.issuetypename forState:UIControlStateNormal];
}

-(void) getComments:(NSString *)key{
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.hud];
    [self.hud setLabelText:@"Loading..."];
    self.hud.dimBackground = YES;
    [self.hud show:YES];
    [arrayOfComments removeAllObjects];
    [files removeAllObjects];
    [fileCreated removeAllObjects];
    [fileAttachedFlag removeAllObjects];
    [screenshotList removeAllObjects];
    
    
    
    NSString *commentURL = [[NSString alloc] initWithFormat:@"%@/rest/api/2/issue/%@",serverUrl, key];
    
    // need to consider path (it needs to do "resolvePath" and store path in UserDefaults in ServerInfoViewController)
    /*
     NSString *commeokntPath = nil;
     if([path isKindOfClass:[NSNull class]] || path == NULL) {
     commentPath = [[NSString alloc] initWithFormat:@"%@", commentURL];
     }
     else {
     commentPath = [[NSString alloc] initWithFormat:@"%@%@", path, commentURL];
     }
     */
    [client getPath:commentURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"hong %@",responseObject);
        
        //get result array from results
        NSDictionary *fields = [responseObject objectForKey:@"fields"];
        
        NSArray *attach = [fields objectForKey:@"attachment"];
        NSString *summary = [fields objectForKey:@"summary"];
        
        NSDictionary* comments = [fields objectForKey:@"comment"];
        
        //NSLog(@"how many attachments?? %d", [attach count]);
        if([attach count]!=0){
            for(int i=0; i<[attach count]; i++){
                [files addObject:[attach[i] objectForKey:@"filename"]];
                
                NSString *fileType = [attach[i] objectForKey:@"mimeType"];
                
                if([fileType hasPrefix:@"image"]){
                    [files addObject:[attach[i] objectForKey:@"thumbnail"]];
                    NSLog(@"thumbnail");
                
                    [files addObject:[attach[i] objectForKey:@"content"]];
                    NSLog(@"CONTENT");
                }
                
                
                
            }
            
        }
        
        NSArray *comp = [key componentsSeparatedByString:@"-"];
        
        [self.leftButton setTitle:[comp objectAtIndex:0] forState:UIControlStateNormal];
        [self.rightButton setTitle:key forState:UIControlStateNormal];
        NSArray* comment = [comments objectForKey:@"comments"];
        
        for (int i=0; i < [comment count]; i++) {
            //      NSLog(@"Comment %d %@", i, comment[i]);
            NSDictionary *authorinfo = [comment[i] objectForKey:@"author"];
            NSString *author = [authorinfo objectForKey:@"name"];
            //            NSLog(@"Author: %@", author);
            NSString *body = [comment[i] objectForKey:@"body"];
            //            NSLog(@"Content: %@", content);
            NSString *creationdate = [comment[i] objectForKey:@"created"];
            //            NSLog(@"Date: %@", creationdate);
            
            NSString *goodDate = nil;
            NSString *formattedDate = nil;
            NSDate *useDate;
            NSRange clean = [creationdate rangeOfString:@"."];
            if (clean.location != NSNotFound) {
                goodDate = [creationdate substringToIndex:clean.location];
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                NSDate *date = [formatter dateFromString:goodDate];
                
                [formatter setDateFormat:@"MMM dd, yyyy, h:mm aaa"];
                formattedDate = [NSString stringWithFormat:@"%@",
                                 [formatter stringFromDate:date]];
                useDate = [formatter dateFromString:formattedDate];
            }
            self.color = [UIColor colorWithRed:42.0/255.0 green:42.0/255.0 blue:42.0/255.0 alpha:1.0];
            
            NSString *image = nil;
            BOOL imageflag = false;
            NSString *bigImage = nil;
            
            for(int j=0; j< [files count]; j=j+3){
                NSLog(@"files %@ %@", files[j], body);
                if([body rangeOfString:files[j]].location != NSNotFound){
                    // show image
                    
                    image = files[j+1];
                    bigImage = files[j+2];
                    imageflag = true;
                    
                    // after finding out image from attachments, finish the loop.
                    j = [files count];
                }
            }
    
            if ([user rangeOfString:author].location != NSNotFound) {
                // Right aligned label
                // UIColor *color = [UIColor blackColor];
                NSBubbleData *heyBubble=nil;
                if(imageflag){
                    
                    UIImage *originImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:image]]];
                    
                    UIImage *thumbnail = [originImage jmc_resizedImage:CGSizeMake(80,110) interpolationQuality:kCGInterpolationMedium];
                    
                    
                    
                    heyBubble = [NSBubbleData dataWithImage:thumbnail date:useDate type:BubbleTypeMine];
                    
                    heyBubble.imageUrl = bigImage;
                    [screenshotList addObject:bigImage];
                }else{
                    heyBubble = [NSBubbleData dataWithText:body date:useDate type:BubbleTypeMine fontColor:self.color];
                    
                }
                [arrayOfComments addObject:heyBubble];
            }else{
                //Left aligned comment
                
                NSBubbleData *photoBubble = nil;
                if(imageflag){
                    UIImage *thumbnail = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:image]]];
                    photoBubble = [NSBubbleData dataWithImage:thumbnail date:useDate type:BubbleTypeSomeoneElse];
                    
                    photoBubble.imageUrl = bigImage;
                    [screenshotList addObject:bigImage];
                }else{
                    photoBubble = [NSBubbleData dataWithText:body date:useDate type:BubbleTypeSomeoneElse fontColor:self.color];
                    
                }

                [arrayOfComments addObject:photoBubble];
            }
        }
        
        
        bubbleData = [[NSMutableArray alloc] initWithArray:arrayOfComments];
        bubbleTable.bubbleDataSource = self;
        bubbleTable.contentInset=UIEdgeInsetsMake(0, 0, bottomInset, 0);
        
        // The line below sets the snap interval in seconds. This defines how the bubbles will be grouped in time.
        // Interval of 120 means that if the next messages comes in 2 minutes since the last message, it will be added into the same group.
        // Groups are delimited with header which contains date and time for the first message in the group.
        
        bubbleTable.snapInterval = 120;
        
        // The line below enables avatar support. Avatar can be specified for each bubble with .avatar property of NSBubbleData.
        // Avatars are enabled for the whole table at once. If particular NSBubbleData misses the avatar, a default placeholder will be set (missingAvatar.png)
        
        bubbleTable.showAvatars = NO;
        
        // Uncomment the line below to add "Now typing" bubble
        // Possible values are
        //    - NSBubbleTypingTypeSomebody - shows "now typing" bubble on the left
        //    - NSBubbleTypingTypeMe - shows "now typing" bubble on the right
        //    - NSBubbleTypingTypeNone - no "now typing" bubble
        
        bubbleTable.typingBubble = NSBubbleTypingTypeNobody;
        
        /*
         if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
         CGRect bframe = self.bubbleTable.frame;
         bframe.size.height = [UIScreen mainScreen].bounds.size.height-99;
         self.bubbleTable.frame = bframe;
         
         CGRect inputframe = self.textInputView.frame;
         inputframe.origin.y = self.bubbleTable.frame.size.height;
         inputframe.size.height = 35;
         self.textInputView.frame = inputframe;
         NSLog(@"%f %f %f %f ", inputframe.origin.x,inputframe.origin.y, inputframe.size.width, inputframe.size.height );
         
         [self.overView addSubview:self.bubbleTable];
         [self.overView addSubview:self.textInputView];
         
         [self.textInputView addSubview:self.textField];
         [self.textInputView addSubview:self.sendButton];
         }
         */
        [bubbleTable reloadData];
        //NSLog(@"1111111 Bubble table %d", [bubbleData count]);
        issuetitle = summary;
        if (![issuetitle isKindOfClass:[NSNull class]] && issuetitle != NULL) {
            self.feedbackTitle.text = summary;
        }else{
            self.feedbackTitle.text = @"Summary";
        }
        
        if((bubbleTable.contentSize.height-bubbleTable.bounds.size.height) > 0 ){
            CGPoint bottomOffset = CGPointMake(0, bubbleTable.contentSize.height-bubbleTable.bounds.size.height+bottomInset);
            [bubbleTable setContentOffset:bottomOffset animated:NO];
        }
        
        [hud hide:YES];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    } failure:^(AFHTTPRequestOperation *failoperation, NSError *error) {
        [hud hide:YES];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        
        
        UIAlertView *alertDialog;
        alertDialog = [[UIAlertView alloc]
                       initWithTitle:@"Error"
                       message:@"Server Error or Unavailable, Please logout!"
                       delegate: self
                       cancelButtonTitle: @"Dismiss"
                       otherButtonTitles: nil];
        alertDialog.alertViewStyle=UIAlertViewStyleDefault;
        [alertDialog show];
        
        NSLog(@"Network Request Error:%@", error);
        
    } ];
    
}

- (void) getProjects{
    
    
    
    NSString *testURL = [[NSString alloc]initWithFormat:@"%@/rest/api/latest/issue/createmeta",serverUrl];
    [client getPath:testURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //NSLog(@"testURL %@",testURL);
         
         NSDictionary* json = responseObject;
            NSLog(@"JSON %@", json);
         
         //parse json for results
         NSArray* projects = [json objectForKey:@"projects"];
         for(int i=0; i<[projects count]; i++){
             
             Projects *proj = [[Projects alloc] init];
             proj.projectkey = [projects[i] objectForKey:@"key"];
             proj.projectname =[projects[i] objectForKey:@"name"];
             NSDictionary *icons = [projects[i] objectForKey:@"avatarUrls"];
             proj.icon = [icons objectForKey:@"24x24"];
             NSArray *issuetypes = [projects[i] objectForKey:@"issuetypes"];
             proj.issuetypes = [NSMutableArray new];
             for(int j=0; j<[issuetypes count]; j++)
             {
                 if(![[issuetypes[j] objectForKey:@"name"] isEqualToString:@"Sub-task"]){
                     IssueType *issuetype = [[IssueType alloc] init];
                     issuetype.issuetypename =[issuetypes[j] objectForKey:@"name"];
                     issuetype.iconUrl = [issuetypes[j] objectForKey:@"iconUrl"];
                     [proj.issuetypes addObject:issuetype];
                 }
                 
             }
             [self.projectsArray addObject:proj];
         }
         
         NSString *projName = [defaults stringForKey:kNewIssueProjName];
         NSString *issueTypeIs = [defaults stringForKey:kNewIssueIssuetype];
         
         if(feedbackStateInt == STATE_NEWISSUE){
             BOOL findProj = false;
             if([projectsArray count]!=0){
                 for(int i=0; i<[projectsArray count]; i++){
                     Projects *p = projectsArray[i];
                     if([p.projectname isEqualToString:projName]){
                         //self.left.selectedrow = i;
                         NSArray *issuetypes = p.issuetypes;
                         if([issuetypes count]!=0){
                             for(int j=0; j < [issuetypes count]; j++){
                                 IssueType *itype = issuetypes[j];
                                 if([itype.issuetypename isEqualToString:issueTypeIs]){
                                     NSLog(@"%@ %@", projName, issueTypeIs);
                                     
                                     [self.leftButton setTitle:p.projectkey forState:UIControlStateNormal];
                                     [self.rightButton setTitle:itype.issuetypename forState:UIControlStateNormal];
                                     findProj = true;
                                 }
                             }
                         }
                     }
                 }
             }
             if(!findProj){
                 feedbackStateInt = STATE_NONE;
             }
         }
         
         
     }
            failure:^(AFHTTPRequestOperation *operation, NSError *error){
                NSLog(@"%@",error);
            }];
    
}

- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView
{
    return [bubbleData count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row
{
    return [bubbleData objectAtIndex:row];
}

-(void) textFieldDidBeginEditing:(UITextField *)textField{
    
    
    if(self.leftButton.tag==0 || self.rightButton.tag==0){
        [self.textField endEditing:YES];
        [_delegate movePanelToOriginalPosition];
        
    }
}

- (IBAction)showActionsheet:(id)sender {
    NSLog(@"Click menu");
    [textField endEditing:YES];
    if(self.leftButton.tag==0 || self.rightButton.tag==0){
        [_delegate movePanelToOriginalPosition];
    }else{
    UIActionSheet *actionSheet;
    
    //Action Sheet Title
    NSString *actionSheetTitle = @"Menu";
    //Action Sheet Button Titles
    NSString *browser = @"Go to browser";
    NSString *cancelTitle = @"Cancel";
    NSString *simage = @"Select Images";
    NSString *raudio = @"Record Audio";
    actionSheet = [[UIActionSheet alloc] initWithTitle:actionSheetTitle
                                              delegate:self
                                     cancelButtonTitle:cancelTitle
                                destructiveButtonTitle:nil
                                     otherButtonTitles:browser, simage, raudio, nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleAutomatic];
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        actionSheet.cancelButtonIndex = -1;
    }
    //    } else {
    //        actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    //    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if (actionSheet.visible) {
            [actionSheet dismissWithClickedButtonIndex:actionSheet.cancelButtonIndex
                                              animated:YES];
        } else {
            [actionSheet showFromBarButtonItem:sender animated:YES];
        }
    } else {
        [actionSheet showInView:self.view];
    }
    self.actionSh = actionSheet;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    /*
     if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
     [menuButton setEnabled:YES];
     
     }
     */
    if(buttonIndex==-1)
    {
        NSLog(@"Touch outside");
        //[actionSheet showInView:[self.view window]];
    }
    else
    {
        //Get the name of the curnt pressed button
        NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
        if ([buttonTitle isEqualToString:@"Go to browser"]) {
            [self.view endEditing:YES];
            
            if(![textField.text isEqualToString:@""]){
                changeFlag = true;
            }
            
            if(changeFlag == true){
            
                UIAlertView *alert =
                [[UIAlertView alloc] initWithTitle: @"Draft"
                                       message: @"Do you want to save it as draft?"
                                      delegate: nil
                             cancelButtonTitle:@"No"
                             otherButtonTitles:@"Save as draft", nil];
                alert.delegate=self;
                [alert show];
            }else{
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            
        }
        else if ([buttonTitle isEqualToString:@"Select Images"]) {
            UIStoryboard *iPhoneStoryBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            ScreenshotsViewController *screenshots = [iPhoneStoryBoard instantiateViewControllerWithIdentifier:@"Screenshots"];
            screenshots.source = FROM_FEEDBACK;
            screenshots.delegate = self;
            [self.navigationController pushViewController:screenshots animated:YES];
        }
        else if ([buttonTitle isEqualToString:@"Record Audio"]) {
            recorder = [JMCRecorder instance];
            if (!recorder) {
                UIAlertView *alert =
                [[UIAlertView alloc] initWithTitle: JMCLocalizedString(@"Voice Recording", @"Alert title when no audio")
                                           message: JMCLocalizedString(@"JMCVoiceRecordingNotSupported", @"Alert when no audio")
                                          delegate: nil
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil];
                [alert show];
                return;
            }
            recorder.recorder.delegate = self;
            
            [recorder start];
            /*
             _timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateProgress:) userInfo:nil repeats:YES];
             self.progressView.progress = 0;
             
             self.countdownView.hidden = NO;
             
             // start animating the voice button...
             NSMutableArray *sprites = [NSMutableArray arrayWithCapacity:8];
             for (int i = 1; i < 9; i++) {
             NSString *sprintName = [@"icon_record_" stringByAppendingFormat:@"%d", i];
             UIImage *img = [UIImage imageNamed:sprintName];
             [sprites addObject:img];
             }
             self.voiceButton.imageView.animationImages = sprites;
             self.voiceButton.imageView.animationDuration = 0.85f;
             [self.voiceButton.imageView startAnimating];
             */
            UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle: @"Recording..."
                                       message: @"Click OK after finishing recording"
                                      delegate: nil
                             cancelButtonTitle:@"Cancel"
                             otherButtonTitles:@"OK", nil];
            alert.delegate=self;
            [alert show];
        }
    }
    
}
#pragma mark - UIAlertViewDelegate Methods

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"OK"]){
        [recorder stop];
        
    }else if([title isEqualToString:@"Dismiss"]){
        
    }else{
        [defaults setObject:imageFileNames forKey:kFeedbackImages];
        if([title isEqualToString:@"No"]){
            // do not save as draft
            [defaults setInteger:STATE_NONE forKey:kFeedbackState];
            [defaults setValue:@"" forKey:kFeedbackWords];
            [defaults setValue:@"" forKey:kNewIssueProjName];
            [defaults setValue:@"" forKey:kNewIssueIssuetype];
            [defaults setValue:@"" forKey:kFeedbackIssueKey];
            
        }else if([title isEqualToString:@"Save as draft"]){
            [defaults setInteger:feedbackStateInt forKey:kFeedbackState];
            [defaults setValue:textField.text forKey:kFeedbackWords];
                
            if(feedbackStateInt==STATE_NEWISSUE){
                Projects *p = projectsArray[self.selectedProject];
                IssueType *itype = p.issuetypes[self.selectedIssueType];
                [defaults setValue:p.projectname forKey:kNewIssueProjName];
                [defaults setValue:itype.issuetypename forKey:kNewIssueIssuetype];
                [defaults setValue:@"" forKey:kFeedbackIssueKey];
            }else if(feedbackStateInt==STATE_EXISTING){
                [defaults setValue:@"" forKey:kNewIssueProjName];
                [defaults setValue:@"" forKey:kNewIssueIssuetype];
                [defaults setValue:issuekey forKey:kFeedbackIssueKey];
            }
        }
        [defaults synchronize];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(void) selectImagesFromFeedback:(NSMutableArray *)arraySelected{
    imageFileNames = [NSMutableArray arrayWithArray:arraySelected];
    [defaults setObject:imageFileNames forKey:kFeedbackImages];
    [defaults synchronize];
    if(feedbackStateInt == STATE_EXISTING){
        // send images right away
        if([imageFileNames count]!=0){
            NSString *attachURL = [[NSString alloc] initWithFormat:@"%@/rest/api/2/issue/%@/attachments",serverUrl, issuekey];
            for(int i=0; i<[imageFileNames count]; i++){
                NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *dataPath = [docDir stringByAppendingPathComponent:@"/Screenshots"];
                NSString *receiptName = [imageFileNames objectAtIndex:i];
                NSString *filePath = [dataPath stringByAppendingPathComponent:receiptName];
                    
                UIImage *origImg = [UIImage imageWithContentsOfFile:filePath];
                    
                CGSize screenSize = [UIScreen mainScreen].bounds.size;
                if (origImg.size.height > screenSize.height) {
                        
                    // resize image... its too huge! (only meant to be screenshots, not photos..)
                    CGSize size = origImg.size;
                    float ratio = screenSize.height / size.height;
                    CGSize smallerSize = CGSizeMake(ratio * size.width, ratio * size.height);
                    origImg = [origImg jmc_resizedImage:smallerSize interpolationQuality:kCGInterpolationMedium];
                }
                //NSLog(@"%@", receiptName);
                NSArray *type = [receiptName componentsSeparatedByString:@"."];
                NSString *mimetype = [[NSString alloc] initWithFormat:@"image/%@", type[1]];
                AFHTTPClient *myclient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:attachURL]];
                [myclient setDefaultHeader:@"X-Atlassian-Token" value:@"nocheck"];
                NSURLRequest *request =
                [myclient multipartFormRequestWithMethod:@"POST"
                                                        path:attachURL
                                                  parameters:nil
                                   constructingBodyWithBlock: ^(id<AFMultipartFormData> formData) {
                                       [formData appendPartWithFileData:UIImagePNGRepresentation(origImg) name:@"file" fileName:receiptName mimeType:mimetype];
                                       //[formData appendPartWithFormData:[[NSNumber numberWithInt:imageData.length].stringValue dataUsingEncoding:NSUTF8StringEncoding] name:@"filelength"];
                                   }];
                    
                AFHTTPRequestOperation *operation =
                [myclient HTTPRequestOperationWithRequest:request
                                                      success:^(AFHTTPRequestOperation *operation, id json) {
                                                          
                                                          NSArray *jsonResp = [[operation responseString] componentsSeparatedByString:@","];
                                                          
                                                          NSString *bigImage  = nil;
                                                          for(NSString *tmp in jsonResp){
                                                              NSLog(@"* %@", tmp);
                                                              if([tmp hasPrefix:@"\"content\":\""]){
                                                                  NSArray *tmp2 = [tmp componentsSeparatedByString:@"\""];
                                                                  if([tmp2 count]>3){
                                                                      bigImage = [tmp2 objectAtIndex:3];
                                                                      
                                                                  }
                                                              }
                                                          }
                                                          
                                                          
                                                          UIImage *thumbnail = [origImg jmc_resizedImage:CGSizeMake(80,110) interpolationQuality:kCGInterpolationMedium];
                                                          NSDate *now = [[NSDate alloc]init];
                                                          NSBubbleData *heyBubble = [NSBubbleData dataWithImage:thumbnail date:now type:BubbleTypeMine];
                                                          
                                                          if(bigImage){
                                                              heyBubble.imageUrl = bigImage;
                                                              [screenshotList addObject:bigImage];
                                                          }
                                                          // send comments with image name as a thumbnail.
                                                          NSString *sendURL = [[NSString alloc] initWithFormat:@"%@/rest/api/2/issue/%@/comment",serverUrl, issuekey];
                                                          /*
                                                           NSString *commentPath = nil;
                                                           if([path isKindOfClass:[NSNull class]] || path == NULL) {
                                                           commentPath = [[NSString alloc] initWithFormat:@"%@", commentURL];
                                                           }
                                                           else {
                                                           commentPath = [[NSString alloc] initWithFormat:@"%@%@", path, commentURL];
                                                           }
                                                           */
                                                          NSString *fileComment = [NSString stringWithFormat:@"!%@|thumbnail!", receiptName];
                                                          // body : contents of comment
                                                          [client registerHTTPOperationClass:[AFJSONRequestOperation class]];
                                                          [client setDefaultHeader:@"Accept" value:@"application/json"];
                                                          [client setParameterEncoding:AFJSONParameterEncoding];
                                                          NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:fileComment,@"body",nil];
                                                          [client postPath:sendURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                              NSLog(@"Success!!!!!!!! ");
                                                          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                              NSLog(@"Failed!!!!!!!!");
                                                              NSLog(@"Network Request Error:%@", error);
                                                              UIAlertView *alertDialog;
                                                              alertDialog = [[UIAlertView alloc]
                                                                             initWithTitle:@"Error"
                                                                             message:@"Fail to send comments"
                                                                             delegate: nil
                                                                             cancelButtonTitle: @"Close"
                                                                             otherButtonTitles: nil];
                                                              alertDialog.alertViewStyle=UIAlertViewStyleDefault;
                                                              [alertDialog show];
                                                              
                                                              [hud hide:YES];
                                                              [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                                              
                                                          }];
                                                          
                                                          
                                                          [bubbleData addObject:heyBubble];
                                                          [bubbleTable reloadData];
                                                          textField.text = @"";
                                                          textField.placeholder=@"Type your comment here";
                                                          [textField resignFirstResponder];
                                                          if((bubbleTable.contentSize.height-bubbleTable.bounds.size.height) > 0 ){
                                                              CGPoint bottomOffset = CGPointMake(0, bubbleTable.contentSize.height-bubbleTable.bounds.size.height+bottomInset);
                                                              [bubbleTable setContentOffset:bottomOffset animated:NO];
                                                          }
                                                          if((i+1) == [imageFileNames count]){
                                                              [imageFileNames removeAllObjects];
                                                              [defaults setObject:imageFileNames forKey:kFeedbackImages];
                                                              [defaults synchronize];
                                                              NSLog(@"delete everything");
                                                          }
                                                          
                                                      }
                                                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                          NSLog(@"All failed %@", error);
                                                      }];
                    
                [myclient enqueueHTTPRequestOperation:operation];
            }
        }
        changeFlag = false;
    }else{
        changeFlag = true;
    }
}

- (IBAction)sendFeedback:(id)sender {
    
    NSString *summaryMsg;
    NSString *msg;
    
    if(self.leftButton.tag==0 || self.rightButton.tag==0){
        [_delegate movePanelToOriginalPosition];
    }else{
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"yyyy-MM-dd-HH.mm.ss.a"];
        //NSLog(@"%@", [format stringFromDate:[[NSDate alloc]init]]);
        
        if(feedbackStateInt==STATE_NEWISSUE){
            if([textField.text isEqualToString:@""]){
                
                if([imageFileNames count]!=0 || audiofile){
                    // if there are attachments, create issue ticket with no description title.
                    msg = @"No description";
                    summaryMsg = msg;
                    [self sendWithSummary:summaryMsg withDescription:msg withDate:format];
                }else{
                    // if there is no attachment, do not send it.
                    [textField resignFirstResponder];
                }
                    
            }else{
                msg = textField.text;
                if([msg length] > MAXLENGTH){
                    summaryMsg = [msg substringToIndex:MAXLENGTH-1];
                }else{
                    summaryMsg = msg;
                }
                [self sendWithSummary:summaryMsg withDescription:msg withDate:format];
            }
        }else{
            if([textField.text isEqualToString:@""]){
                [textField resignFirstResponder];
            }else{
                [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                self.hud = [[MBProgressHUD alloc] initWithView:self.view];
                [self.view addSubview:self.hud];
                [self.hud setLabelText:@"Sending..."];
                self.hud.dimBackground = YES;
                [self.hud show:YES];
                    
                // add comments
                NSString *sendURL = [[NSString alloc] initWithFormat:@"%@/rest/api/2/issue/%@/comment",serverUrl, issuekey];
                /*
                 NSString *commentPath = nil;
                 if([path isKindOfClass:[NSNull class]] || path == NULL) {
                 commentPath = [[NSString alloc] initWithFormat:@"%@", commentURL];
                 }
                 else {
                 commentPath = [[NSString alloc] initWithFormat:@"%@%@", path, commentURL];
                 }
                 */
                
                // body : contents of comment
                [client registerHTTPOperationClass:[AFJSONRequestOperation class]];
                [client setDefaultHeader:@"Accept" value:@"application/json"];
                [client setParameterEncoding:AFJSONParameterEncoding];
                NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:textField.text,@"body",nil];
                [client postPath:sendURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    NSLog(@"Success!!!!!!!! ");
                    //NSLog(@"%@", responseObject);
                    NSBubbleData *sayBubble = [NSBubbleData dataWithText:textField.text date:[NSDate dateWithTimeIntervalSinceNow:0] type:BubbleTypeMine fontColor:color];
                    
                    [bubbleData addObject:sayBubble];
                    [bubbleTable reloadData];
                    textField.text = @"";
                    textField.placeholder=@"Type your comment here";
                    [textField resignFirstResponder];
                    if((bubbleTable.contentSize.height-bubbleTable.bounds.size.height) > 0 ){
                        CGPoint bottomOffset = CGPointMake(0, bubbleTable.contentSize.height-bubbleTable.bounds.size.height+bottomInset);
                        [bubbleTable setContentOffset:bottomOffset animated:NO];
                    }
                    
                    
                    [hud hide:YES];
                    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog(@"Failed!!!!!!!!");
                    NSLog(@"Network Request Error:%@", error);
                    UIAlertView *alertDialog;
                    alertDialog = [[UIAlertView alloc]
                                   initWithTitle:@"Error"
                                   message:@"Fail to send comments"
                                   delegate: nil
                                   cancelButtonTitle: @"Close"
                                   otherButtonTitles: nil];
                    alertDialog.alertViewStyle=UIAlertViewStyleDefault;
                    [alertDialog show];
                    
                    [hud hide:YES];
                    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                    
                }];
            }
        }
    }
    
}


- (void) sendWithSummary:(NSString *) summaryMsg withDescription:(NSString *) msg withDate:(NSDateFormatter *) format{
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.hud];
    [self.hud setLabelText:@"Sending..."];
    self.hud.dimBackground = YES;
    [self.hud show:YES];

    
    NSString *createURL = [[NSString alloc] initWithFormat:@"%@/rest/api/2/issue/", serverUrl];
    
    Projects *p = projectsArray[selectedProject];
    IssueType *itype = p.issuetypes[selectedIssueType];
    NSLog(@"%@%@",p.projectname, itype.issuetypename);
    NSDictionary *pproj = [NSDictionary dictionaryWithObjectsAndKeys:p.projectkey,@"key", nil];
    NSDictionary *pissuetype = [NSDictionary dictionaryWithObjectsAndKeys:itype.issuetypename, @"name", nil];
    NSDictionary *pfield = [NSDictionary dictionaryWithObjectsAndKeys:pproj, @"project",summaryMsg, @"summary", msg, @"description" ,pissuetype, @"issuetype", nil];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:pfield, @"fields", nil];
    NSLog(@"%@", params);
    
    [client postPath:createURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success!!!!!!!! ");
        NSLog(@"%@", responseObject);
        
        changeFlag = false;
        NSDictionary *json = responseObject;
        NSString *issued = [json objectForKey:@"key"];
        issuekey = issued;
        [self.rightButton setTitle:issuekey forState:UIControlStateNormal];
        feedbackStateInt = STATE_EXISTING;
        
        
        self.feedbackTitle.text = summaryMsg;
        
        
        NSBubbleData *sayBubble = [NSBubbleData dataWithText:msg date:[NSDate dateWithTimeIntervalSinceNow:0] type:BubbleTypeMine fontColor:color];
        
        [bubbleData addObject:sayBubble];
        [bubbleTable reloadData];
        textField.text = @"";
        textField.placeholder=@"Type your comment here";
        [textField resignFirstResponder];
        if((bubbleTable.contentSize.height-bubbleTable.bounds.size.height) > 0 ){
            CGPoint bottomOffset = CGPointMake(0, bubbleTable.contentSize.height-bubbleTable.bounds.size.height+bottomInset);
            [bubbleTable setContentOffset:bottomOffset animated:NO];
        }
        NSString *attachURL = [[NSString alloc] initWithFormat:@"%@/rest/api/2/issue/%@/attachments",serverUrl, issuekey];
        
        if(audiofile){
            // Attach the images
            NSString *mimetype = @"audio/aac";
            NSString *receiptName = [[NSString alloc] initWithFormat:@"recording-%@.aac", [format stringFromDate:[[NSDate alloc]init]]];
            
            AFHTTPClient *myclient= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:attachURL]];
            [myclient setDefaultHeader:@"X-Atlassian-Token" value:@"nocheck"];
            NSURLRequest *request =
            [myclient multipartFormRequestWithMethod:@"POST"
                                                path:attachURL
                                          parameters:nil
                           constructingBodyWithBlock: ^(id<AFMultipartFormData> formData) {
                               [formData appendPartWithFileData:audiofile name:@"file" fileName:receiptName mimeType:mimetype];
                               //[formData appendPartWithFormData:[[NSNumber numberWithInt:imageData.length].stringValue dataUsingEncoding:NSUTF8StringEncoding] name:@"filelength"];
                           }];
            
            AFHTTPRequestOperation *operation1 =
            [myclient HTTPRequestOperationWithRequest:request
                                              success:^(AFHTTPRequestOperation *operation2, id json) {
                                                  NSLog(@"All images Attached OK");
                                                  UIImage *thumbnail = [UIImage imageNamed:@"audio_attachment"];
                                                  NSDate *now = [[NSDate alloc]init];
                                                  NSBubbleData *heyBubble = [NSBubbleData dataWithImage:thumbnail date:now type:BubbleTypeMine];
                                                  
                                                  [bubbleData addObject:heyBubble];
                                                  [bubbleTable reloadData];
                                                  textField.text = @"";
                                                  textField.placeholder=@"Type your comment here";
                                                  [textField resignFirstResponder];
                                                  if((bubbleTable.contentSize.height-bubbleTable.bounds.size.height) > 0 ){
                                                      CGPoint bottomOffset = CGPointMake(0, bubbleTable.contentSize.height-bubbleTable.bounds.size.height+bottomInset);
                                                      [bubbleTable setContentOffset:bottomOffset animated:NO];
                                                  }
                                                  audiofile=nil;
                                              }
                                              failure:^(AFHTTPRequestOperation *operation2, NSError *error) {
                                                  NSLog(@"All failed %@", error);
                                              }];
            
            [myclient enqueueHTTPRequestOperation:operation1];
        }
        
        if([imageFileNames count]!=0){
            
            
            AFHTTPClient *myclient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:attachURL]];
            [myclient setDefaultHeader:@"X-Atlassian-Token" value:@"nocheck"];
            
            for(int i=0; i<[imageFileNames count]; i++){
                NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *dataPath = [docDir stringByAppendingPathComponent:@"/Screenshots"];
                NSString *receiptName = [imageFileNames objectAtIndex:i];
                NSString *filePath = [dataPath stringByAppendingPathComponent:receiptName];
                
                UIImage *origImg = [UIImage imageWithContentsOfFile:filePath];
                
                CGSize screenSize = [UIScreen mainScreen].bounds.size;
                if (origImg.size.height > screenSize.height) {
                    
                    // resize image... its too huge! (only meant to be screenshots, not photos..)
                    CGSize size = origImg.size;
                    float ratio = screenSize.height / size.height;
                    CGSize smallerSize = CGSizeMake(ratio * size.width, ratio * size.height);
                    origImg = [origImg jmc_resizedImage:smallerSize interpolationQuality:kCGInterpolationMedium];
                }
                //NSLog(@">>%@", receiptName);
                NSArray *type = [receiptName componentsSeparatedByString:@"."];
                NSString *mimetype = [[NSString alloc] initWithFormat:@"image/%@", type[1]];
                
                
                
                NSURLRequest *request =
                [myclient multipartFormRequestWithMethod:@"POST"
                                                    path:attachURL
                                              parameters:nil
                               constructingBodyWithBlock: ^(id<AFMultipartFormData> formData) {
                                   [formData appendPartWithFileData:UIImagePNGRepresentation(origImg) name:@"file" fileName:receiptName mimeType:mimetype];
                                   //[formData appendPartWithFormData:[[NSNumber numberWithInt:imageData.length].stringValue dataUsingEncoding:NSUTF8StringEncoding] name:@"filelength"];
                               }];
                
                AFHTTPRequestOperation *operation =
                [myclient HTTPRequestOperationWithRequest:request
                                                  success:^(AFHTTPRequestOperation *operation, id json) {
                                                      NSLog(@"All images Attached OK");
                                                      
                                                      NSArray *jsonResp = [[operation responseString] componentsSeparatedByString:@","];
                                                      
                                                      NSString *bigImage  = nil;
                                                      for(NSString *tmp in jsonResp){
                                                          NSLog(@"* %@", tmp);
                                                          if([tmp hasPrefix:@"\"content\":\""]){
                                                              NSArray *tmp2 = [tmp componentsSeparatedByString:@"\""];
                                                              if([tmp2 count]>3){
                                                                  bigImage = [tmp2 objectAtIndex:3];
                                                                  
                                                              }
                                                          }
                                                      }
                                                      
                                                      
                                                      UIImage *thumbnail = [origImg jmc_resizedImage:CGSizeMake(80,110) interpolationQuality:kCGInterpolationMedium];
                                                      NSDate *now = [[NSDate alloc]init];
                                                      NSBubbleData *heyBubble = [NSBubbleData dataWithImage:thumbnail date:now type:BubbleTypeMine];
                                                      
                                                    
                                                      if(bigImage){
                                                          heyBubble.imageUrl = bigImage;
                                                          [screenshotList addObject:bigImage];
                                                      }
                                                      
                                                      [bubbleData addObject:heyBubble];
                                                      
                                                      
                                                      NSString *sendURL = [[NSString alloc] initWithFormat:@"%@/rest/api/2/issue/%@/comment",serverUrl, issuekey];
                                                      /*
                                                       NSString *commentPath = nil;
                                                       if([path isKindOfClass:[NSNull class]] || path == NULL) {
                                                       commentPath = [[NSString alloc] initWithFormat:@"%@", commentURL];
                                                       }
                                                       else {
                                                       commentPath = [[NSString alloc] initWithFormat:@"%@%@", path, commentURL];
                                                       }
                                                       */
                                                      NSString *fileComment = [NSString stringWithFormat:@"!%@|thumbnail!", receiptName];
                                                      // body : contents of comment
                                                      [client registerHTTPOperationClass:[AFJSONRequestOperation class]];
                                                      [client setDefaultHeader:@"Accept" value:@"application/json"];
                                                      [client setParameterEncoding:AFJSONParameterEncoding];
                                                      NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:fileComment,@"body",nil];
                                                      [client postPath:sendURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                          NSLog(@"Success!!!!!!!! ");
                                                      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                          NSLog(@"Failed!!!!!!!!");
                                                          NSLog(@"Network Request Error:%@", error);
                                                          UIAlertView *alertDialog;
                                                          alertDialog = [[UIAlertView alloc]
                                                                         initWithTitle:@"Error"
                                                                         message:@"Fail to send comments"
                                                                         delegate: nil
                                                                         cancelButtonTitle: @"Close"
                                                                         otherButtonTitles: nil];
                                                          alertDialog.alertViewStyle=UIAlertViewStyleDefault;
                                                          [alertDialog show];
                                                          
                                                          [hud hide:YES];
                                                          [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                                          
                                                      }];
                                                      
                                                      
                                                      [bubbleTable reloadData];
                                                      textField.text = @"";
                                                      textField.placeholder=@"Type your comment here";
                                                      [textField resignFirstResponder];
                                                      if((bubbleTable.contentSize.height-bubbleTable.bounds.size.height) > 0 ){
                                                          CGPoint bottomOffset = CGPointMake(0, bubbleTable.contentSize.height-bubbleTable.bounds.size.height+bottomInset);
                                                          [bubbleTable setContentOffset:bottomOffset animated:NO];
                                                      }
                                                      if((i+1) == [imageFileNames count]){
                                                          [imageFileNames removeAllObjects];
                                                          [defaults setObject:imageFileNames forKey:kFeedbackImages];
                                                          [defaults synchronize];
                                                          NSLog(@"delete everything");
                                                      }
                                                      
                                                  }
                                                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                      NSLog(@"All failed %@", error);
                                                  }];
                
                [myclient enqueueHTTPRequestOperation:operation];
                
                
            }
            
        }
        
        [hud hide:YES];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failed!!!!!!!!");
        NSLog(@"Network Request Error:%@", error);
        UIAlertView *alertDialog;
        alertDialog = [[UIAlertView alloc]
                       initWithTitle:@"Error"
                       message:@"Fail to send feedbacks"
                       delegate: nil
                       cancelButtonTitle: @"Close"
                       otherButtonTitles: nil];
        alertDialog.alertViewStyle=UIAlertViewStyleDefault;
        [alertDialog show];
        [hud hide:YES];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        
    }];
}

- (void) didSelectNSBubbleDataCell:(NSBubbleData *)dataCell{
    //NSLog(@"choose image %@", dataCell.view);
    
    UIStoryboard *iPhoneStoryBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    FullSSViewController *fullcon = [iPhoneStoryBoard instantiateViewControllerWithIdentifier:@"fullScreenImages"];
    fullcon.openImage = dataCell.imageUrl;
    fullcon.imageLists = self.screenshotList;
    
    [self.navigationController pushViewController:fullcon animated:YES];
    

    
    
}



@end
