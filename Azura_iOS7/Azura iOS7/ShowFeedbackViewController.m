//
//  ShowFeedbackViewController.m
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/22/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "ShowFeedbackViewController.h"
#import "Projects.h"
#import "UIBubbleTableView.h"
#import "UIBubbleTableViewDataSource.h"
#import "NSBubbleData.h"
#import "NavigationController.h"


#define PANEL_WIDTH 60


#define CENTER_TAG 1
#define LEFT_TAG 2
#define RIGHT_TAG 3
#define LEFT_SECOND_TAG 4

#define IMAGE_CHOSEN 10
#define IMAGE_NONE 20

#define kFeedbackIssueKey @"feedbackIssueKey"
#define kFeedbackState @"feedbackState"
#define kFeedbackWords @"feedbackWords"
#define kFeedbackAudio @"feedbackAudio"

#define kFeedbackImages @"feedbackImages"
#define kFeedbackImageState @"feedbackImageState"

@interface ShowFeedbackViewController ()



@end

@implementation ShowFeedbackViewController



@synthesize leftone;
@synthesize lefttwo;
@synthesize rightone;




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // flag for swipe gesture
    leftone = false;
    lefttwo = false;
    rightone = false;
    
    // left = project / right = issue
    UIStoryboard *iphonestoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    
    self.center = [iphonestoryboard instantiateViewControllerWithIdentifier:@"FeedbackCenter"];
    self.center.view.tag = CENTER_TAG;
    self.center.delegate = self;
    [self.view addSubview:self.center.view];
    [self addChildViewController:_center];
    [_center didMoveToParentViewController:self];
    
    
    
    
    
        
}


-(void) viewWillAppear:(BOOL)animated{
    
    [self.navigationController.toolbar setHidden:YES];
    [self.navigationController.navigationBar setHidden:YES];
    
}

-(void) viewWillDisappear:(BOOL)animated{
    
    [self.navigationController.toolbar setHidden:NO];
    [self.navigationController.navigationBar setHidden:NO];
    /*
    if(!gotoBrowser){
        // store the issuekey inside of navigation controller
        // it needs to be stored in userdefaults
        // also when user leaves the feedback page, it needs to store draft
        // (selected image and recorded audio too)
        [defaults setValue:issuekey forKey:kFeedbackIssueKey];
        [defaults setValue:textField.text forKey:kFeedbackWords];
        [defaults synchronize];
    }else{
        if(newissue==true){
            if([imageFileNames count]!=0){
                [defaults setValue:imageFileNames forKey:kFeedbackImages];
            }
            if(audiofile){
                // save audio file in directory
                NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *path = [docDir stringByAppendingPathComponent:@"audio.aac"];
                NSLog(@"Path is %@", path);
                
                [audiofile writeToFile:path atomically:YES];
                
            }
            
            
            
        }
    }*/
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)getRightView {
	// init view if it doesn't already exist
	if (_right == nil)
	{
        UIStoryboard *iphonestoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        self.right = [iphonestoryboard instantiateViewControllerWithIdentifier:@"FeedbackRight"];
		self.right.view.tag = RIGHT_TAG;
		self.right.delegate = _center;
		
		[self.view addSubview:self.right.view];
		
		[self addChildViewController:self.right];
		[_right didMoveToParentViewController:self];
		
		_right.view.frame = CGRectMake(PANEL_WIDTH, 0, self.view.frame.size.width-PANEL_WIDTH, self.view.frame.size.height);
	}
	self.rightone = YES;
    
	// setup view shadows
	[self showCenterViewWithShadow:YES withOffset:2];
    
	UIView *view = self.right.view;
	return view;
}


-(UIView *)getLeftView {
	// init view if it doesn't already exist
	if (_left == nil)
	{
        UIStoryboard *iphonestoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        self.left = [iphonestoryboard instantiateViewControllerWithIdentifier:@"FeedbackLeft"];
        self.left.view.tag = LEFT_TAG;
        self.left.delegate = _center;
        
        [self.view addSubview:self.left.view];
        
		[self addChildViewController:_left];
		[_left didMoveToParentViewController:self];
        
		_left.view.frame = CGRectMake(0, 0, self.view.frame.size.width-PANEL_WIDTH, self.view.frame.size.height);
	}
	self.leftone = YES;
    
	// setup view shadows
	[self showCenterViewWithShadow:YES withOffset:-2];
    
	UIView *view = self.left.view;
	return view;
}

-(UIView *)getLeftSecondView {
	// init view if it doesn't already exist
	if (_leftSecond == nil)
	{
        UIStoryboard *iphonestoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        self.leftSecond = [iphonestoryboard instantiateViewControllerWithIdentifier:@"FeedbackLeftTwo"];
        
        
        self.leftSecond.view.tag = LEFT_SECOND_TAG;
        self.leftSecond.delegate = _center;
        
        [self.view addSubview:self.leftSecond.view];
        
		[self addChildViewController:_leftSecond];
		[_leftSecond didMoveToParentViewController:self];
        
		self.leftSecond.view.frame = CGRectMake(self.view.frame.size.width-PANEL_WIDTH, 0, self.view.frame.size.width-PANEL_WIDTH, self.view.frame.size.height);
	}
	self.lefttwo = YES;
    
	// setup view shadows
	//[self showCenterViewWithShadow:YES withOffset:-2];
    
	UIView *view = self.leftSecond.view;
	return view;
}

-(void)showCenterViewWithShadow:(BOOL)value withOffset:(double)offset {
	if (value) {
		[_center.view.layer setCornerRadius:4];
		[_center.view.layer setShadowColor:[UIColor blackColor].CGColor];
		[_center.view.layer setShadowOpacity:0.8];
		[_center.view.layer setShadowOffset:CGSizeMake(offset, offset)];
        
        UIBezierPath *path = [UIBezierPath bezierPath];
        // Start at the Top Left Corner
        [path moveToPoint:CGPointMake(0.0, 20.0)];
        
        // This is the extra point in the middle :) Its the secret sauce.
        [path addLineToPoint:CGPointMake(CGRectGetWidth(_center.view.frame) / 2.0, CGRectGetHeight(_center.view.frame) / 2.0)];
        
        // Move to the Top Right Corner
        [path addLineToPoint:CGPointMake(CGRectGetWidth(_center.view.frame), 20.0)];
        
        // Move to the Bottom Right Corner
        [path addLineToPoint:CGPointMake(CGRectGetWidth(_center.view.frame), CGRectGetHeight(_center.view.frame))];
        
        
        
        // Move to the Bottom Left Corner
        [path addLineToPoint:CGPointMake(0.0, CGRectGetHeight(_center.view.frame))];
        
        // Move to the Close the Path
        [path closePath];

        
        _center.view.layer.shadowPath = path.CGPath;
        
	} else {
		[_center.view.layer setCornerRadius:0.0f];
		[_center.view.layer setShadowOffset:CGSizeMake(offset, offset)];
	}
}

#pragma mark -
#pragma mark Delegate Actions

-(void)movePanelLeft {
	UIView *childView = [self getRightView];
	[self.view sendSubviewToBack:childView];
    [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        _center.view.frame = CGRectMake(-self.view.frame.size.width + PANEL_WIDTH, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
                     completion:^(BOOL finished) {
                         if (finished) {
                             
                             _center.rightButton.tag = 0;
                         }
                     }];
}

-(void) expandPanel{
    
    if(_center.leftButton.tag == 0){
        
        [UIView animateWithDuration:0.2 animations:^{
            // animation left
            [UIView setAnimationDelay:10];
            
        } completion:^(BOOL finished){
            [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                _center.view.frame = CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
                self.left.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            }
                             completion:^(BOOL finished) {
                                 if (finished) {
                                     
                                 }
                             }];
            
        }];
        
        
        
        
        
    }else if(_center.rightButton.tag == 0){
        
        [UIView animateWithDuration:0.2 animations:^{
            // animation left
            [UIView setAnimationDelay:10];
            
        } completion:^(BOOL finished){
            [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            _center.view.frame = CGRectMake(-self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
            self.right.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
             }
             completion:^(BOOL finished) {
             if (finished) {
                 NSLog(@"success");
             }else{
                 NSLog(@"fail");
             }
             }];
            
            
        }];
        
        
    }
}

-(void) shrinkPanel{
    if(_center.leftButton.tag == 0){
        [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
           _center.view.frame = CGRectMake(self.view.frame.size.width - PANEL_WIDTH, 0, self.view.frame.size.width, self.view.frame.size.height);
            self.left.view.frame = CGRectMake(0, 0, self.view.frame.size.width-PANEL_WIDTH, self.view.frame.size.height);        }
                         completion:^(BOOL finished) {
                             if (finished) {
                                 _center.leftButton.tag = 0;
                             }
                         }];
        
    }else if(_center.rightButton.tag == 0){
        [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            _center.view.frame = CGRectMake(-self.view.frame.size.width + PANEL_WIDTH, 0, self.view.frame.size.width, self.view.frame.size.height);
            self.right.view.frame = CGRectMake(PANEL_WIDTH, 0, self.view.frame.size.width-PANEL_WIDTH, self.view.frame.size.height);
        }
                         completion:^(BOOL finished) {
                             if (finished) {
                                 _center.rightButton.tag=0;
                             }
                         }];
    }
}


-(void)movePanelRight {
    UIView *childView = [self getLeftView];
    [self.view sendSubviewToBack:childView];
    
    [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        _center.view.frame = CGRectMake(self.view.frame.size.width - PANEL_WIDTH, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
                     completion:^(BOOL finished) {
                         if (finished) {
                             _center.leftButton.tag = 0;
                         }
                     }];
}
-(void)movePanelToOriginalPosition {

	[UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        _center.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
                     completion:^(BOOL finished) {
                         if (finished) {
                             [self resetMainView];
                            
                         }
                     }];
}

-(void) rotatePanel{
    
    UIView *childView = [self getLeftSecondView];
    [self.view sendSubviewToBack:childView];
    
    Projects *proj = self.center.projectsArray[self.center.selectedProject];
    
    self.leftSecond.types = proj.issuetypes;
    [_leftSecond.tableveiw reloadData];
    
    [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [_left.view setFrame:CGRectOffset([_left.view frame], -[_left.view frame].size.width, 0)];
        [_leftSecond.view setFrame:CGRectOffset([_leftSecond.view frame], -[_left.view frame].size.width, 0)];
    }
                     completion:^(BOOL finished) {
                         if (finished) {
                            // [self resetMainView];
                         }
                     }];
    
}

-(void) rotatePanelFromSearch{
    
    UIView *childView = [self getLeftSecondView];
    [self.view sendSubviewToBack:childView];
    
    Projects *proj = self.center.projectsArray[self.center.selectedProject];
    
    _left.view.frame = CGRectMake(0, 0, self.view.frame.size.width-PANEL_WIDTH, self.view.frame.size.height);
    
    //CGRect frame = _left.view.frame;
    //frame.size.width =self.view.frame.size.width-PANEL_WIDTH;
    
    
    self.leftSecond.types = proj.issuetypes;
    [_leftSecond.tableveiw reloadData];
    
    [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [_left.view setFrame:CGRectOffset([_left.view frame], -[_left.view frame].size.width, 0)];
        [_leftSecond.view setFrame:CGRectOffset([_leftSecond.view frame], -[_left.view frame].size.width, 0)];
        _center.view.frame = CGRectMake(self.view.frame.size.width - PANEL_WIDTH, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
                     completion:^(BOOL finished) {
                         if (finished) {
                             // [self resetMainView];
                         }
                     }];
    
}

-(void) goBackToProject{
    [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [_left.view setFrame:CGRectOffset([_left.view frame], [_left.view frame].size.width, 0)];
        [_leftSecond.view setFrame:CGRectOffset([_leftSecond.view frame], [_left.view frame].size.width, 0)];
    }
                     completion:^(BOOL finished) {
                         if (finished) {
                             // [self resetMainView];
                         }
                     }];
}

-(void)resetMainView {
	// remove left and right views, and reset variables, if needed
	if (_left != nil) {
        if(_leftSecond != nil){
            [self.leftSecond.view removeFromSuperview];
            self.leftSecond = nil;
            self.lefttwo = NO;
        }
		[self.left.view removeFromSuperview];
        
		self.left = nil;
		
        _center.leftButton.tag = 1;
		self.leftone = NO;
	}
	if (_right != nil) {
		[self.right.view removeFromSuperview];
		self.right = nil;
		_center.rightButton.tag = 1;
		self.rightone = NO;
	}
	// remove view shadows
	[self showCenterViewWithShadow:NO withOffset:0];
}



@end
