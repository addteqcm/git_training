//
//  FeedbackLeftTwoViewController.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/29/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FeedbackLeftTwoViewControllerDelegate <NSObject>

@optional
- (void) IssueTypeSelected: (NSInteger) row;
- (void) backButtonIsclicked;

@end

@interface FeedbackLeftTwoViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) id<FeedbackLeftTwoViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UITableView *tableveiw;
@property (nonatomic) NSMutableArray *types;
@property (nonatomic) NSInteger secondSelectedRow;
@property (nonatomic) BOOL finish;
- (IBAction)backButtonClicked:(id)sender;
@end
