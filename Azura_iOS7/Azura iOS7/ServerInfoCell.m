//
//  ServerInfoCell.m
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/22/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "ServerInfoCell.h"

@implementation ServerInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
@end
