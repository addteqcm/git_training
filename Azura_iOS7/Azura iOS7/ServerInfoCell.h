//
//  ServerInfoCell.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/22/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServerInfoCell : UITableViewCell <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *txtServer;
@property (strong, nonatomic) IBOutlet UITextField *txtPortNo;
@property (strong, nonatomic) IBOutlet UISwitch *switchHttps;
@property (strong, nonatomic) IBOutlet UITextField *txtUsername;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UISwitch *switchRememberme;

@end
