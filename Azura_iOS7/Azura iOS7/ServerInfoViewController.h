//
//  ServerInfoViewController.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/22/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerInfoCell.h"

#import "AFNetworking.h"
#import "KeychainItemWrapper.h"

@interface ServerInfoViewController : UITableViewController <UITextFieldDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) KeychainItemWrapper *keychainItem;
@property(strong,nonatomic)NSString *myServer;
@property(strong,nonatomic)NSString *myPort;
@property BOOL isHTTP;
@property(strong,nonatomic)NSString *myUserName;
@property(strong,nonatomic)NSString *myPassword;
@property(strong,nonatomic)NSString *testURL;
@property BOOL isRememberMe;
@property (strong, nonatomic) UITextField *tempField;
@property (strong, nonatomic) AFHTTPClient *client;

- (IBAction)switchHttp:(id)sender;
- (IBAction)switchRememberme:(id)sender;
- (IBAction)saveServerInfo:(id)sender;





@end
