//
//  FeedbackRightViewController.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/29/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "Plans.h"

@protocol FeedbackRightViewControllerDelegate <NSObject>

@optional
- (void)imageSelected:(UIImage *)image withTitle:(NSString *)imageTitle withCreator:(NSString *)imageCreator;
- (void) expandViewForSearch;
- (void) shrinkView;
- (void) selectExistingIssue: (Plans *) p;
@end

@interface FeedbackRightViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate>


@property (weak, nonatomic) id<FeedbackRightViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UISearchBar *searchbar;
@property (strong, nonatomic) NSMutableArray *filtered;
@property (strong, nonatomic) AFHTTPClient *searchclient;
@property (strong, nonatomic) Plans *selectedPlan;
@property (strong, nonatomic) IBOutlet UITableView *mainTableView;
@property (strong, nonatomic) NSString *searchHistory;
@property (nonatomic) CGRect origin;
@end
