//
//  AddBookmarkViewController.m
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 8/30/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "AddBookmarkViewController.h"

@interface AddBookmarkViewController ()
- (void)saveAction;
- (void)cancelAction;
- (void)saveBookmarks;
@end

@implementation AddBookmarkViewController

@synthesize bookmark;
@synthesize tableview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // Generating the tableview
    [self.tableview setDelegate:self];
    tableview = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStyleGrouped];
    self.tableview.dataSource = self;
    self.tableview.scrollEnabled = YES;
    
    // Generating the navigation bar object for table view
    naviBarObj = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 44)];
    
    // Set Orientation adjustments for the bookmark tableview
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(UIInterfaceOrientationIsLandscape(orientation))
    {
        naviBarObj.frame = CGRectMake(0,20,self.view.frame.size.height,44);
        self.tableview.frame = CGRectMake(0, 60, self.view.frame.size.height,self.view.frame.size.width);
    }
    else
    {
        naviBarObj.frame = CGRectMake(0,20, self.view.frame.size.width, 44);
        self.tableview.frame = CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height);
    }
    
    // Add the table view to the view
    [self.view addSubview:tableview];
    [self.view insertSubview:naviBarObj aboveSubview:tableview];
    
    // Add bar button items to the navigation bar
    UIBarButtonItem *cancelItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelAction)];
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveAction)];
    UINavigationItem *navigItem = [[UINavigationItem alloc] initWithTitle:@"Add Bookmark"];
    navigItem.rightBarButtonItem = doneItem;
    navigItem.leftBarButtonItem = cancelItem;
    
    naviBarObj.items = [NSArray arrayWithObjects:navigItem, nil];
    
    // Use userdefaults to get bookmarks
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *bookmarks = [defaults objectForKey:@"bookmarks"];
    if(bookmarks)
    {
        bookmarkArray = [[NSMutableArray alloc] initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:bookmarks]];
    }
    else
    {
        bookmarkArray = [[NSMutableArray alloc] initWithCapacity:1];
    }
    
    // Display edit button in the navigation bar for the view controller
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    CGRect frame = naviBarObj.frame;
    if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
    {
        NSLog(@"Portrait %f",frame.size.height);
        frame.size.height = 44;
    }
    else
    {
        NSLog(@"Landscape %f",frame.size.height);
        frame.size.height = 44;
    }
    naviBarObj.frame = frame;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    naviBarObj.frame = CGRectMake(0,60,self.view.frame.size.width,44);
    tableview.frame = CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height);
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(UIInterfaceOrientationIsLandscape(orientation))
    {
        naviBarObj.frame = CGRectMake(0, 20, self.view.frame.size.height, 44);
        self.tableview.frame = CGRectMake(0, 60, self.view.frame.size.height, self.view.frame.size.width);
    }
    else
    {
        naviBarObj.frame = CGRectMake(0, 20, self.view.frame.size.width, 44);
        self.tableview.frame = CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height);
    }
    
    naviBarObj.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    tableview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self.tableview reloadData];
    
    [nameTextField becomeFirstResponder];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(void)saveAction
{
    NSLog(@"Save Action for Bookmarks");
    // Force resignFirstResponder for edited textfield
    [nameTextField resignFirstResponder];
    BOOL saveURL = YES;
    // Check that the URL is not already in the bookmark list
    for(BookMarkObject *bm in bookmarkArray)
    {
        if([bm.url.absoluteString isEqual:bookmark.url.absoluteString])
        {
            saveURL = NO;
            break;
        }
    }
    // Add the new URL in the list
    if(saveURL)
    {
        bookmark.name = nameTextField.text;
        [bookmarkArray addObject:bookmark];
        [self.tableview reloadData];
    }
    
    [self saveBookmarks];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
}

// Cancel the add bookmarks view
-(void)cancelAction
{
    NSLog(@"Cancel Action for Bookmarks");
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

// Save bookmarks array
-(void)saveBookmarks
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:bookmarkArray] forKey:@"bookmarks"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)setBookmark:(NSString *)aName url:(NSURL *)aURL
{
    bookmark = [[BookMarkObject alloc] initWithName:aName andURL:aURL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self setBookmark:nil];
}

#pragma - text delegate methods
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self saveAction];
}

#pragma mark - Table view delegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSInteger rows = 0;
    switch (section) {
        case 0:
        {
            rows = 2;
            break;
        }
        default:
        {
            break;
        }
    }
    return rows;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *UrlCellIdentifier = @"urlCell";
    static NSString *TextFieldCellIdentifier = @"textFieldCell";
    UITableViewCell *cell = nil;
    
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:TextFieldCellIdentifier];
                    if(cell == nil)
                    {
                        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TextFieldCellIdentifier];
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                        nameTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 10, 280, 30)];
                        nameTextField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
                        nameTextField.adjustsFontSizeToFitWidth = YES;
                        nameTextField.textColor = [UIColor colorWithRed:50.0/255 green:79.0/255 blue:133.0/255 alpha:1.0];
                        nameTextField.keyboardType = UIKeyboardTypeDefault;
                        nameTextField.returnKeyType = UIReturnKeyDone;
                        nameTextField.backgroundColor = [UIColor clearColor];
                        nameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
                        nameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
                        nameTextField.textAlignment = NSTextAlignmentLeft;
                        nameTextField.delegate = self;
                        nameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
                        [nameTextField setEnabled:YES];
                        nameTextField.text = bookmark.name;
                        
                        [cell addSubview:nameTextField];
                    }
                    break;
                }
                case 1:
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:UrlCellIdentifier];
                    if(cell == nil)
                    {
                        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:UrlCellIdentifier];
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    }
                    cell.textLabel.adjustsFontSizeToFitWidth = YES;
                    cell.textLabel.font = [UIFont systemFontOfSize:17.0];
                    cell.textLabel.minimumScaleFactor = 14.0;
                    cell.textLabel.textColor = [UIColor colorWithRed:165.0/255 green:165.0/255 blue:165.0/255 alpha:1.0];
                    cell.textLabel.text = [bookmark.url absoluteString];
                    break;
                }
                default:
                    break;
            }
            break;
            
        default:
            break;
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end