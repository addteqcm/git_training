//
//  Projects.m
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/30/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "Projects.h"

@implementation Projects
@synthesize projectkey;
@synthesize projectname;
@synthesize issuetypes;
@synthesize icon;
@end

@implementation IssueType

@synthesize iconUrl;
@synthesize issuetypename;

@end
