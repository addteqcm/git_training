//
//  Plans.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 11/14/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Plans : NSObject
@property (strong, nonatomic) NSString *label;
@property (strong, nonatomic) NSString *value;
@property (strong, nonatomic) NSString *iconUrl;
@property (strong, nonatomic) NSString *projectName;
@end
