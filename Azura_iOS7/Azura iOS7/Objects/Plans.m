//
//  Plans.m
//  Azura iOS7
//
//  Created by HeeJinChoi on 11/14/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "Plans.h"

@implementation Plans
@synthesize label;
@synthesize iconUrl;
@synthesize value;
@synthesize projectName;
@end
