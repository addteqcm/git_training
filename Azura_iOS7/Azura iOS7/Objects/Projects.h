//
//  Projects.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/30/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Projects : NSObject
@property (strong, nonatomic) NSString *projectname;
@property (strong, nonatomic) NSString *projectkey;
@property (strong, nonatomic) NSString *icon;
@property (strong, nonatomic) NSMutableArray *issuetypes;

@end


@interface IssueType : NSObject
@property (strong, nonatomic) NSString *iconUrl;
@property (strong, nonatomic) NSString *issuetypename;
@end