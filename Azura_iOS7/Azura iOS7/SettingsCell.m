//
//  SettingsCell.m
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/9/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "SettingsCell.h"

@implementation SettingsCell
@synthesize homePageText;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    [homePageText setDelegate:self];
    [homePageText setBorderStyle:UITextBorderStyleNone];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
