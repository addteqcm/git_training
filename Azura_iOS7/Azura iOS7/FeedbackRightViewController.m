//
//  FeedbackRightViewController.m
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/29/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "FeedbackRightViewController.h"
#import "NavigationController.h"
#import "ShowFeedbackViewController.h"
#import "Projects.h"
#define ISSUE @"Issue"
#define STATE_EXISTING 2
#define kFeedbackIssueKey @"feedbackIssueKey"
#define kFeedbackState @"feedbackState"
#define kSearchWord @"searchWord"

@interface FeedbackRightViewController ()

@end

@implementation FeedbackRightViewController
@synthesize filtered;
@synthesize searchclient;
@synthesize selectedPlan;
@synthesize origin;
@synthesize searchHistory;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    filtered = [NSMutableArray new];
    [_searchbar setDelegate:self];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.searchHistory = [defaults stringForKey:kSearchWord];
    
    if(self.searchHistory != NULL && ![self.searchHistory isEqualToString:@""]){
        
        self.searchbar.text = self.searchHistory;
        [self getPlansForMaintable:[self.searchHistory componentsSeparatedByString:@" "]];
    }else{
        [self getRecentPlans];
    }
}


-(void) viewWillDisappear:(BOOL)animated{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:self.searchHistory forKey:kSearchWord];
    [defaults synchronize];
}

-(void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView{
    // resize frame of tableview
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"number of rows");
    return [filtered count];
}

-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // nothing. useless
    static NSString *cellidentifier = @"SimpleTableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
    if(cell==nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    
    //if(tableView == self.searchDisplayController.searchResultsTableView){
        Plans *pl = self.filtered[[indexPath row]];
        cell.textLabel.text =[[NSString alloc] initWithFormat:@"%@ - %@", pl.label,pl.value];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:pl.iconUrl]]];
        UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 14.0 ];
        cell.textLabel.font  = myFont;
    
        return cell;
    
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Plans *p = self.filtered[[indexPath row]];
    [_delegate selectExistingIssue:p];
   
    
    /* NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    // choose issue. and it will upload comments of that issue.
    self.selectedPlan = self.filtered[[indexPath row]];
    ShowFeedbackViewController *con = (ShowFeedbackViewController *)self.parentViewController;
    con.issuekey = selectedPlan.label;
    con.issuetitle = selectedPlan.value;
    
    [con getComments:con.issuekey];
    
    NavigationController *navi = (NavigationController *)self.navigationController;
    navi.existingissuekey = selectedPlan.label;
    navi.feedbackState = STATE_EXISTING;
    
    [defaults setValue:selectedPlan.label forKey:kFeedbackIssueKey];
    [defaults setInteger:STATE_EXISTING forKey:kFeedbackState];
    [defaults synchronize];
    
    con.rightone=false;
    con.buttonIssue.title=ISSUE;
    [con.right.view removeFromSuperview];*/
    
}

- (BOOL) searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    // expand menu view as full screen
    [_delegate expandViewForSearch];
    return YES;
}


-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [_delegate shrinkView];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString{
    
    [self.filtered removeAllObjects];
    self.searchHistory = searchString;
    // search only search string is not empty
    if([searchString length]!=0){
        // search every words.
        // if user type "azura bamboo"
        // search both
        NSArray *searchWords = [searchString componentsSeparatedByString:@" "];
        [self getPlans: searchWords];
    }else{
        //NSLog(@"Do not search");
        [self getRecentPlans];
    }
    return YES;
}
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption{
    
    
    return YES;
}
- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller{
    
}

-(void) searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller{
    [_delegate shrinkView];
    [self.filtered removeAllObjects];
    [self getRecentPlans];
    self.searchHistory = @"";
}


-(void) getPlans: (NSArray *) words{
    
    // final jql for jira
    NSMutableString *finalwords = [[NSMutableString alloc]initWithString:@""];
    
    ShowFeedbackViewController *con = (ShowFeedbackViewController *)self.parentViewController;
    
    // pname : existing project name
    NSMutableArray *pname = [NSMutableArray new];
    
    // if user type "azu", it also search project start with AZU
    NSMutableArray *additional = [NSMutableArray new];
    
    for(int i=0; i < [con.center.projectsArray count]; i++){
        Projects *p = con.center.projectsArray[i];
        [pname addObject:p.projectname];
        [pname addObject:p.projectkey];
    }
    
    
    for(int i=0; i< [words count]; i++){
        
        
        // search words
        NSString *wlower = [[words[i] lowercaseString] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(![wlower isEqualToString:@""]){
            if(i!=0){
                [finalwords appendString:@"|"];
            }
            BOOL flag = false;
            for(NSString *ss in pname){
                NSString *slower = [ss lowercaseString];
                if([ss caseInsensitiveCompare:wlower]== NSOrderedSame){
                    // if search word is existing
                    flag=true;
                }else if([slower hasPrefix:wlower] && ![slower isEqualToString:wlower]){
                    // if there is project that starts with search words
                    //NSLog(@"PREFIX %@ %@", slower, wlower);
                    [additional addObject:slower];
                }
            }
        
        
            // added search words to jql for summary and project.
            NSString *summary = [[NSString alloc] initWithFormat:@"summary~\"%@*\"", wlower];
            if(flag==true){
                //NSLog(@"it has %@",wlower]);
                NSString *project = [[NSString alloc] initWithFormat:@"project=%@", wlower];
                [finalwords appendFormat:@"%@|%@", summary, project];
            }else{
                //NSLog(@"it does not have %@", wlower]);
                [finalwords appendFormat:@"%@", summary];
            }
        
        }
        
    }
    
    // search projects start with search words
    if([additional count]>0){
        for(int i=0; i<[additional count]; i++){
            [finalwords appendString:@"|"];
            NSString *project = [[NSString alloc] initWithFormat:@"project=%@", additional[i]];
            [finalwords appendFormat:@"%@",project];
        
        }
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *serverUrl;
    if([defaults objectForKey:@"myServer"]!=nil){
        serverUrl = [defaults objectForKey:@"myServer"];
    }
    searchclient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:serverUrl]];
    [searchclient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [searchclient setDefaultHeader:@"Accept" value:@"application/json"];
    [searchclient setParameterEncoding:AFJSONParameterEncoding];
    NSString *testURL = [[NSString alloc]initWithFormat:@"%@/rest/api/2/search?jql=%@",serverUrl, finalwords];
    NSString *strURL = [testURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@" , strURL);
    [searchclient getPath:strURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //NSLog(@"here");
         //NSLog(@"testURL %@",testURL);
         // NSDictionary* json = responseObject;
         //NSLog(@"res %@",responseObject);
         
         NSDictionary* json = responseObject;
           // NSLog(@"JSON %@", json);
         //NSString *num = [json objectForKey:@"total"];
         //NSLog(@"%@", num);
         
         //parse json for results
         NSArray* projects = [json objectForKey:@"issues"];
         for(int i=0; i<[projects count]; i++){
             Plans *pl = [[Plans alloc] init];
             pl.label = [projects[i] objectForKey:@"key"];
             NSDictionary *fields = [projects[i] objectForKey:@"fields"];
             pl.value = [fields objectForKey:@"summary"];
             NSDictionary *proj = [fields objectForKey:@"project"];
             pl.projectName=[proj objectForKey:@"name"];
             NSDictionary *issuetype = [fields objectForKey:@"issuetype"];
             pl.iconUrl = [issuetype objectForKey:@"iconUrl"];
             //NSLog(@"%@ %@ %@",pl.label, pl.value, pl.iconUrl);
             [self.filtered addObject:pl];
         }
         
         [self.searchDisplayController.searchResultsTableView reloadData];
     }
     
            failure:^(AFHTTPRequestOperation *operation, NSError *error){
                NSLog(@"%@",error);
                NSLog(@"%@", [operation description]);
            }];
    

}

-(void) getPlansForMaintable: (NSArray *) words{
    // final jql for jira
    NSMutableString *finalwords = [[NSMutableString alloc]initWithString:@""];
    
    ShowFeedbackViewController *con = (ShowFeedbackViewController *)self.parentViewController;
    
    // pname : existing project name
    NSMutableArray *pname = [NSMutableArray new];
    
    // if user type "azu", it also search project start with AZU
    NSMutableArray *additional = [NSMutableArray new];
    
    for(int i=0; i < [con.center.projectsArray count]; i++){
        Projects *p = con.center.projectsArray[i];
        [pname addObject:p.projectname];
        [pname addObject:p.projectkey];
    }
    
    
    for(int i=0; i< [words count]; i++){
        
        
        // search words
        NSString *wlower = [[words[i] lowercaseString] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if(![wlower isEqualToString:@""]){
            if(i!=0){
                [finalwords appendString:@"|"];
            }
            BOOL flag = false;
            for(NSString *ss in pname){
                NSString *slower = [ss lowercaseString];
                if([ss caseInsensitiveCompare:wlower]== NSOrderedSame){
                    // if search word is existing
                    flag=true;
                }else if([slower hasPrefix:wlower] && ![slower isEqualToString:wlower]){
                    // if there is project that starts with search words
                    //NSLog(@"PREFIX %@ %@", slower, wlower);
                    [additional addObject:slower];
                }
            }
            
            
            // added search words to jql for summary and project.
            NSString *summary = [[NSString alloc] initWithFormat:@"summary~\"%@*\"", wlower];
            if(flag==true){
                //NSLog(@"it has %@",wlower]);
                NSString *project = [[NSString alloc] initWithFormat:@"project=%@", wlower];
                [finalwords appendFormat:@"%@|%@", summary, project];
            }else{
                //NSLog(@"it does not have %@", wlower]);
                [finalwords appendFormat:@"%@", summary];
            }
            
        }
        
    }
    
    // search projects start with search words
    if([additional count]>0){
        for(int i=0; i<[additional count]; i++){
            [finalwords appendString:@"|"];
            NSString *project = [[NSString alloc] initWithFormat:@"project=%@", additional[i]];
            [finalwords appendFormat:@"%@",project];
            
        }
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *serverUrl;
    if([defaults objectForKey:@"myServer"]!=nil){
        serverUrl = [defaults objectForKey:@"myServer"];
    }
    searchclient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:serverUrl]];
    [searchclient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [searchclient setDefaultHeader:@"Accept" value:@"application/json"];
    [searchclient setParameterEncoding:AFJSONParameterEncoding];
    NSString *testURL = [[NSString alloc]initWithFormat:@"%@/rest/api/2/search?jql=%@",serverUrl, finalwords];
    NSString *strURL = [testURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@" , strURL);
    [searchclient getPath:strURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //NSLog(@"here");
         //NSLog(@"testURL %@",testURL);
         // NSDictionary* json = responseObject;
         //NSLog(@"res %@",responseObject);
         [self.filtered removeAllObjects];
         NSDictionary* json = responseObject;
         // NSLog(@"JSON %@", json);
         //NSString *num = [json objectForKey:@"total"];
         //NSLog(@"%@", num);
         
         //parse json for results
         NSArray* projects = [json objectForKey:@"issues"];
         for(int i=0; i<[projects count]; i++){
             Plans *pl = [[Plans alloc] init];
             pl.label = [projects[i] objectForKey:@"key"];
             NSDictionary *fields = [projects[i] objectForKey:@"fields"];
             pl.value = [fields objectForKey:@"summary"];
             NSDictionary *proj = [fields objectForKey:@"project"];
             pl.projectName=[proj objectForKey:@"name"];
             NSDictionary *issuetype = [fields objectForKey:@"issuetype"];
             pl.iconUrl = [issuetype objectForKey:@"iconUrl"];
             //NSLog(@"%@ %@ %@",pl.label, pl.value, pl.iconUrl);
             [self.filtered addObject:pl];
         }
         
         [self.mainTableView reloadData];
     }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error){
                      NSLog(@"%@",error);
                      NSLog(@"%@", [operation description]);
                  }];
    
    
}

-(void) getRecentPlans{
    NSString *searchWord = @"issuekey in issueHistory() ORDER BY lastViewed DESC";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *serverUrl;
    if([defaults objectForKey:@"myServer"]!=nil){
        serverUrl = [defaults objectForKey:@"myServer"];
    }
    searchclient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:serverUrl]];
    [searchclient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [searchclient setDefaultHeader:@"Accept" value:@"application/json"];
    [searchclient setParameterEncoding:AFJSONParameterEncoding];
    NSString *testURL = [[NSString alloc]initWithFormat:@"%@/rest/api/2/search?jql=%@",serverUrl, searchWord];
    NSString *strURL = [testURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [searchclient getPath:strURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //NSLog(@"testURL %@",testURL);
         // NSDictionary* json = responseObject;
         //NSLog(@"res %@",responseObject);
         
         NSDictionary* json = responseObject;
         // NSLog(@"JSON %@", json);
         //NSString *num = [json objectForKey:@"total"];
         //NSLog(@"%@", num);
         
         //parse json for results
         NSArray* projects = [json objectForKey:@"issues"];
         for(int i=0; i<[projects count]; i++){
             Plans *pl = [[Plans alloc] init];
             pl.label = [projects[i] objectForKey:@"key"];
             NSDictionary *fields = [projects[i] objectForKey:@"fields"];
             pl.value = [fields objectForKey:@"summary"];
             NSDictionary *proj = [fields objectForKey:@"project"];
             pl.projectName=[proj objectForKey:@"name"];
             NSDictionary *issuetype = [fields objectForKey:@"issuetype"];
             pl.iconUrl = [issuetype objectForKey:@"iconUrl"];
             //NSLog(@"%@ %@ %@",pl.label, pl.value, pl.iconUrl);
             [self.filtered addObject:pl];
             
         }
         
         [self.mainTableView reloadData];
     }
     
                  failure:^(AFHTTPRequestOperation *operation, NSError *error){
                      NSLog(@"%@",error);
                      NSLog(@"%@", [operation description]);
                  }];
    
    
}


@end
