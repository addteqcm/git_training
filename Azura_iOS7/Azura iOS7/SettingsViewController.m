//
//  SettingsViewController.m
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/9/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "SettingsViewController.h"
#import "sqlite3.h"
#import "KeychainItemWrapper.h"

#import "MyHTTPConnection.h"
#import "HTTPServer.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
#import <ifaddrs.h>
#import <arpa/inet.h>

@interface SettingsViewController ()
@property (nonatomic) BOOL isWifiTransfer;
@property (nonatomic, strong) NSString *myHttpPort;
@end

@implementation SettingsViewController
@synthesize homePageTempText;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView.delegate self];
    [self.homePageTempText.delegate self];
    [self.homePageTempText setBorderStyle:UITextBorderStyleNone];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 1;
    }
    else if(section == 1)
    {
        return 1;
    }
    else if(section == 2)
    {
        return 3;
    }
    else if(section == 3)
    {
        return 3;
    }
    else
        return 0;
}

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return @"Wi-Fi";
    }
    else if(section == 1)
    {
        return @"Jira";
    }
    else if(section == 2)
    {
        return @"Browser Properties";
    }
    else if(section == 3)
    {
        return @"Privacy";
    }
    else
        return 0;
}*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        if(indexPath.row==0)
        {
            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"wifiTransferCell" forIndexPath:indexPath];
            if(self.isWifiTransfer){
                cell.wifiTransferSwitch.on = true;
            }else{
                cell.wifiTransferSwitch.on=false;
            }
            return cell;
        }
    }
    if(indexPath.section==1)
    {
        if(indexPath.row==0)
        {
            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"jiraCredentialsCell" forIndexPath:indexPath];
            return cell;
        }
    }
    if(indexPath.section==2)
    {
        if(indexPath.row==0)
        {
            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"homePageCell" forIndexPath:indexPath];
            [cell.homePageText setTag:1];
            [cell.homePageText setBorderStyle:UITextBorderStyleNone];
            [cell.homePageText setFont:[UIFont fontWithName:@"Helvetica" size:17]];
            [cell.homePageText setTextAlignment:NSTextAlignmentRight];
            self.homePageTempText = cell.homePageText;
            if([[NSUserDefaults standardUserDefaults] valueForKey:@"homepage"]!=nil)
            {
                NSString *homepage = [[NSUserDefaults standardUserDefaults] valueForKey:@"homepage"];
                [self.homePageTempText setText:homepage];
            }
            else
            {
                NSString *homepage = @"http://www.addteq.com";
                [self.homePageTempText setText:homepage];
            }
            return cell;
        }
        if(indexPath.row==1)
        {
            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"searchEngineCell" forIndexPath:indexPath];
            int selectedSearchEngineIndex = [[[NSUserDefaults standardUserDefaults]valueForKey:@"Search_Engine"] intValue];
            if(selectedSearchEngineIndex == 0)
            {
                cell.searchEngineNameText.text = @"Google";
            }
            else if(selectedSearchEngineIndex == 1)
            {
                cell.searchEngineNameText.text = @"Yahoo!";
            }
            else if(selectedSearchEngineIndex == 2)
            {
                cell.searchEngineNameText.text = @"Bing";
            }
            return cell;
        }
        if(indexPath.row==2)
        {
            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"developerCell" forIndexPath:indexPath];
            return cell;
        }
    }
    if(indexPath.section==3)
    {
        if(indexPath.row==0)
        {
            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"privateBrowsingCell" forIndexPath:indexPath];
            return cell;
        }
        if(indexPath.row==1)
        {
            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"clearCookiesCell" forIndexPath:indexPath];
            return cell;
        }
        if(indexPath.row==2)
        {
            SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"deleteBrowsingHistoryCell" forIndexPath:indexPath];
            return cell;
        }
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section==0)
    {
        if(indexPath.row==0)
        {
            [homePageTempText resignFirstResponder];
        }
        if(indexPath.row==1)
        {
            [homePageTempText resignFirstResponder];
        }
    }
    if(indexPath.section==1)
    {
        if(indexPath.row==0)
        {
            [homePageTempText resignFirstResponder];
        }
    }
    if(indexPath.section==2)
    {
        if(indexPath.row==0)
        {
            [homePageTempText resignFirstResponder];
        }
        if(indexPath.row==1)
        {
            [homePageTempText resignFirstResponder];
        }
    }
    if(indexPath.section==3)
    {
        if(indexPath.row==0){
            [homePageTempText resignFirstResponder];
            // privarte browsing
        }
        else if(indexPath.row==1)
        {
            [homePageTempText resignFirstResponder];
            [self DeleteCookies];
        }
        else if(indexPath.row==2)
        {
            [homePageTempText resignFirstResponder];
            [self DeleteBrowsingHistory];
        }
    }
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    //Checks For Approval
    if(actionSheet.tag==1){
        if (buttonIndex == 1) {
            NSHTTPCookie *cookie;
            NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
            for (cookie in [storage cookies]) {
                [storage deleteCookie:cookie];
            }
            
            KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"JiraLogin" accessGroup:nil];
            [keychainItem resetKeychainItem];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete"
                                                            message:@"All cookies are deleted"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Dismiss"
                                                  otherButtonTitles:nil];
            [alert show];
        } else {
            NSLog(@"Stopped from deleting cookies");
        }
    }else if(actionSheet.tag==2){
        if (buttonIndex == 1) {
            NSLog(@"Deleting history");
            NSString *documentDirectory;
            NSArray *directoryPaths;
            
            // Get the documents directory
            directoryPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            documentDirectory = directoryPaths[0];
            NSString *databasePath = [[NSString alloc] initWithString:[documentDirectory stringByAppendingPathComponent:@"URL_History.sql"]];
            
            sqlite3 *DB;
            const char *dbPath = [databasePath UTF8String];
            NSString *msg;
            if(sqlite3_open(dbPath, &DB) == SQLITE_OK)
            {
                char *errMsg;
                const char *sql_stmt = "DELETE FROM HistoryOfURLs";
                
                if (sqlite3_exec(DB, sql_stmt, NULL, NULL, &errMsg)== SQLITE_OK) {
                    msg = @"All browsing history is deleted";
                }else{
                    msg = @"Fail to delete browing history";
                }
            }
            sqlite3_close(DB);
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete"
                                                            message:msg
                                                           delegate:nil
                                                  cancelButtonTitle:@"Dismiss"
                                                  otherButtonTitles:nil];
            [alert show];
            
        } else {
            NSLog(@"Stopped from deleting history");
        }
    }
}
-(void)DeleteCookies{
    //implement deleting cookies here!!
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete All Cookies?"
                                                    message:@"Confirm if you want to delete"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    alert.tag=1;
    [alert show];
}
-(void)DeleteBrowsingHistory{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete All Browsing History?"
                                                    message:@"Confirm if you want to delete"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    alert.tag=2;
    [alert show];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [homePageTempText resignFirstResponder];
}

-(void) viewWillAppear:(BOOL)animated{
    [self.navigationController.toolbar setHidden:YES];
}



-(void)viewWillDisappear:(BOOL)animated
{
    NSIndexPath *myIndexPath = [NSIndexPath indexPathForRow:0 inSection:2];
    UITableViewCell *myCell = [self.tableView cellForRowAtIndexPath:myIndexPath];
    UITextView *myTextView = (UITextView *) [myCell viewWithTag:1];
    NSString *myTextViewText = myTextView.text;
    [[NSUserDefaults standardUserDefaults]setValue:myTextViewText forKey:@"homepage"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    if([self isMovingFromParentViewController]){
        // when user click back button
        [self.navigationController.toolbar setHidden:NO];
    }
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

-(void) startServer{
    NSError *err;
    if([_httpServer start:&err]){
        [self setMyHttpPort:[NSString stringWithFormat:@"%hu", [_httpServer listeningPort]]];
        NSLog(@"my port %@", self.myHttpPort);
    }
    
}

-(NSString *) tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
    if(section==0){
        if(self.isWifiTransfer==true){
            return [NSString stringWithFormat: @"Connect to this device using: \nhttp://%@:%@",[self getIPAddress],self.myHttpPort];
        }
    }
    return nil;
}


- (IBAction)wifiTransferOnOff:(id)sender {
    [self.homePageTempText resignFirstResponder];
    UISwitch *sw = (UISwitch *) sender;
    if(sw.on){
        // wifi transfer is on
        self.isWifiTransfer = true;
        [DDLog addLogger:[DDTTYLogger sharedInstance]];
        _httpServer = [[HTTPServer alloc] init];
        [_httpServer setType:@"_http._tcp."];
        
        NSString *webPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Web"];
        NSLog(@"Setting document root: %@", webPath);
        [_httpServer setDocumentRoot:webPath];
        
        [_httpServer setConnectionClass:[MyHTTPConnection class]];
        [self startServer];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }else{
        // wifi transder is off
        self.isWifiTransfer = false;
        [_httpServer stop:false];
        [self setMyHttpPort:@""];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    
}

- (NSString *)getIPAddress
{
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *wifiAddressSimulator = nil;
    NSString *wifiAddress = nil;
    if(!getifaddrs(&interfaces)) {
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
            if(sa_type == AF_INET || sa_type == AF_INET6) {
                NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
                NSString *addr = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                // pdp_ip0 for 3g/4g
                if([name isEqualToString:@"en0"]) {
                    // Interface is the wifi connection on the simulator
                    wifiAddressSimulator = addr;
                } else if([name isEqualToString:@"en1"]) {
                    // Interface is the cell connection on the iPhone
                    wifiAddressSimulator = addr;
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    NSString *addr;
    if(wifiAddressSimulator==NULL){
        addr = wifiAddress;
    }else{
        addr=wifiAddressSimulator;
    }
    return addr;
}


@end
