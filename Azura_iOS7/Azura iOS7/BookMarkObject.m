//
//  BookMarkObject.m
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 8/30/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "BookMarkObject.h"

@implementation BookMarkObject
@synthesize name;
@synthesize url;

- (id) initWithName:(NSString *)aName andURL:(NSURL *)aUrl
{
    
    if (self = [super init])
    {
        self.name = aName;
        self.url = aUrl;
    }
    return self;
}

- (id) initWithCoder: (NSCoder *)coder
{
    self = [self init];
    if (self != nil)
    {
        self.name = [coder decodeObjectForKey: @"name"];
        self.url = [coder decodeObjectForKey: @"url"];
    }
    return self;
}

- (void) encodeWithCoder: (NSCoder *)coder
{
    [coder encodeObject:name forKey:@"name"];
    [coder encodeObject:url forKey:@"url"];
}

- (void) dealloc
{
    self.name = nil;
    self.url = nil;
}

@end
