//
//  SsCollectionCell.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 1/16/14.
//  Copyright (c) 2014 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SsCollectionCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *screenshotFile;
@property (nonatomic) BOOL selectedFlag;
@property (strong, nonatomic) NSString *filename;
@end
