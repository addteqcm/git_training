//
//  EditScreenshotsViewController.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 1/15/14.
//  Copyright (c) 2014 addteq. All rights reserved.
//

#import "AddScreenshotsViewController.h"
#import "ALRadialMenu.h"
#import "ACEDrawingView.h"

#import "JMCSketchView.h"
#import "JMCSketch.h"
#import "JMCSketchScrollView.h"
#import <QuartzCore/QuartzCore.h>
#import "JMCSketchContainerView.h"
#import "JMCSketchViewControllerDelegate.h"
#import "JMCVector.h"
#import "JMCShapeView.h"

#import "WEPopoverController.h"
#import "ColorViewController.h"


@interface EditScreenshotsViewController : UIViewController <AddScreenshotsDelegate, UIAlertViewDelegate,UIActionSheetDelegate, UIGestureRecognizerDelegate, ALRadialMenuDelegate, ACEDrawingViewDelegate, ColorViewControllerDelegate, WEPopoverControllerDelegate, UIPopoverControllerDelegate>

@property (strong, nonatomic) IBOutlet UIView *wholeView;
@property (strong, nonatomic) IBOutlet UIView *centerView;
@property (weak, nonatomic) IBOutlet UIView *centerTopView;
@property (weak, nonatomic) IBOutlet UIView *centerBottomView;
@property (strong, nonatomic) UIImageView *mainImage;
@property (strong, nonatomic) JMCSketchContainerView *mainView;

@property (strong, nonatomic) IBOutlet JMCSketchScrollView *imageScrollView;
@property (strong, nonatomic) ALRadialMenu *radialMenu;

@property (strong, nonatomic) NSMutableArray *arraySelected;
@property (strong, nonatomic) NSMutableArray *arrayDeleted;
//@property (strong, nonatomic) UIImageView *figure;
//@property (strong, nonatomic) UIView *draggingImage;
@property (nonatomic) BOOL openListFlag;
@property (nonatomic) BOOL openToolFlag;

@property (strong, nonatomic) AddScreenshotsViewController *addSSVC;
@property (nonatomic) CGFloat slideMenuWidth;

@property (strong, nonatomic) IBOutlet UIButton *menubutton;
@property (strong, nonatomic) IBOutlet UIButton *toolButton;
@property (strong, nonatomic) IBOutlet UIButton *colorPickerButton;
@property (strong, nonatomic) UIColor *skyblue;
@property (strong, nonatomic) UIColor *greenColor;
@property (nonatomic, strong) WEPopoverController *wePopoverController;

@property (strong, nonatomic) ACEDrawingView *drawingView;

@property (strong, nonatomic) NSString *filename;

@property (strong, nonatomic) UIColor *lineInDrawingView;

@property (nonatomic) int curIndex;

- (IBAction)openTool:(id)sender;

- (IBAction)openList:(id)sender;
- (IBAction)pickColors:(id)sender;

- (IBAction)deleteCurImage:(id)sender;
- (IBAction)undo:(id)sender;
- (IBAction)save:(id)sender;
- (IBAction)redo:(id)sender;

- (IBAction)done:(id)sender;


@end
