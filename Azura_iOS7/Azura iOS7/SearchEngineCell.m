//
//  SearchEngineCell.m
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/10/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "SearchEngineCell.h"

@implementation SearchEngineCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
