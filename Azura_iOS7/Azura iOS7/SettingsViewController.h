//
//  SettingsViewController.h
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/9/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsCell.h"

@class HTTPServer;

@interface SettingsViewController : UITableViewController<UITextFieldDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) HTTPServer *httpServer;
@property (strong, nonatomic) IBOutlet UITextField *homePageTempText;


- (IBAction)wifiTransferOnOff:(id)sender;

@end
