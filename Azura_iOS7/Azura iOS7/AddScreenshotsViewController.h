//
//  AddScreenshotsViewController.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 1/15/14.
//  Copyright (c) 2014 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ScreenshotsViewController.h"

@protocol AddScreenshotsDelegate <NSObject>

@optional
-(void) deleteImageAtIndex:(int)index;
-(void) selectImageAtIndex:(int)index;
@end

@interface AddScreenshotsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) id<AddScreenshotsDelegate> delegate;
@property (strong, nonatomic) NSMutableArray *arrayThumbnails;
@property (strong, nonatomic) IBOutlet UITableView *thumbnailList;
@property (nonatomic) int totalCount;
@property (strong, nonatomic) NSString *dataPath;
@property (strong, nonatomic) UIColor *skyblue;
@property (nonatomic) CGFloat slideMenuHeight;
@end
