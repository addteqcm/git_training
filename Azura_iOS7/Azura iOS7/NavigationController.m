//
//  NavigationController.m
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/26/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "NavigationController.h"
#import "EditScreenshotsViewController.h"
@interface NavigationController ()

@end

@implementation NavigationController
@synthesize selectedProj;
@synthesize selectedType;
@synthesize existingissuekey;
@synthesize feedbackAudio;
@synthesize feedbackImages;
@synthesize feedbackState;
@synthesize feedbackWords;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) shouldAutorotate {
/*
    id currentViewController = self.topViewController;
    if([currentViewController isKindOfClass:[EditScreenshotsViewController class]])
        return NO;*/
    // Return YES for supported orientations
    return YES;
    
    
    
}
 
-(NSUInteger)supportedInterfaceOrientations {
    id currentViewController = self.topViewController;
    if([currentViewController isKindOfClass:[EditScreenshotsViewController class]])
        return UIInterfaceOrientationMaskPortrait;
    
    return UIInterfaceOrientationMaskAll;
    
}


@end
