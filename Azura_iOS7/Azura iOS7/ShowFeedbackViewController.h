//
//  ShowFeedbackViewController.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/22/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <QuartzCore/QuartzCore.h>
#import "FeedbackRightViewController.h"
#import "FeedbackLeftPanelViewController.h"
#import "FeedbackCenterViewController.h"





@interface ShowFeedbackViewController : UIViewController <FeedbackCenterViewControllerDelegate>

@property (strong, nonatomic) FeedbackLeftPanelViewController *left;
@property (strong, nonatomic) FeedbackRightViewController *right;
@property (strong, nonatomic) FeedbackCenterViewController *center;
@property (strong, nonatomic) FeedbackLeftTwoViewController *leftSecond;


@property (nonatomic) BOOL leftone;
@property (nonatomic) BOOL lefttwo;
@property (nonatomic) BOOL rightone;




@end
