//
//  SearchEngineCell.h
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/10/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchEngineCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *preferredSearchEngine;

@end
