//
//  ICarouselViewController.h
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/20/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@class WebViewDetails;

@protocol iCarouselViewControllerDelegate <NSObject>
-(void)newTabCarouselViewController:(id)controller didCreateANewTab:(NSInteger)index;
-(void)selectTabCarouselViewController:(id)controller didSelectTabInCarousel:(NSInteger)index;
-(void)deleteTabCarouselViewController:(id)controller didDeleteTabInCarousel:(NSInteger)index;
-(void)doneTabCarouselViewController:(id)controller;
@end

@interface ICarouselViewController : UIViewController
@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *newtab;
@property (strong, nonatomic) UIImage *currentImage;
@property (nonatomic, weak) id <iCarouselViewControllerDelegate> delegate;
@property (nonatomic, strong) UIView *parentView;
- (IBAction)insertTabs:(id)sender;
-(void)insertTab:(NSMutableArray *)detailsArray;
- (void)deleteTab:(id)sender;
- (IBAction)doneTab:(id)sender;
//-(UIImage *)getImageOfWebView:(UIWebView *)webV;
-(void)resetTabs;
@end
