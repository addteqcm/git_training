//
//  BookMarkObject.h
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 8/30/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookMarkObject : NSObject<NSCoding>
{
    NSString *name;
    NSURL *url;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSURL *url;

- (id) initWithName:(NSString *)name andURL:(NSURL *)url;

@end
