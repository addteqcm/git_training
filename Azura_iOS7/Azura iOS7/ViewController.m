//
//  ViewController.m
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 8/26/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "ViewController.h"
#import "WebViewDetails.h"

//#define kOpenFeedback @"openFeedback"

@interface ViewController ()
-(void)getBack;
-(void)getForward;

@end

@implementation ViewController
BOOL webViewScrolledToTop;
BOOL webViewScrolledToBottom;
UISwipeGestureRecognizer *swipeRecognizerUp;
UISwipeGestureRecognizer *swipeRecognizerDown;
@synthesize locationField;
@synthesize navItem;
@synthesize webView;
@synthesize URLString;
@synthesize autocompleteArray;
@synthesize autocompleteTableView;
@synthesize urlsInDB;
@synthesize arrayOfWebViews;
@synthesize currentIndexOfWebView;
@synthesize previousIndexOfWebView;
@synthesize dimensionOfWebview;
@synthesize webViewDetails;
@synthesize lastContentOffset;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self shouldAutomaticallyForwardRotationMethods];
    
    [self createDatabase];
    
    [self setBackButton];
    
    [self setForwardButton];
    
    [self setRefreshButton];
    
    [self setHomeButton];
    
    [self setNavigationControllerItems];
    
    [self setAutocompleteProperties];
    
    [self setSearchTextField];
    
    [self setToolbar];
    
//    [self.webView setScalesPageToFit:YES];
    
    arrayOfWebViews = [[NSMutableArray alloc] initWithCapacity:16];
    webViewDetailsArray = [[NSMutableArray alloc] initWithCapacity:16];
    [self setCurrentIndexOfWebView:0];
    
    [arrayOfWebViews addObject:self.webView];
    [self.webView setDelegate:self];
    [self.webView.scrollView setDelegate:self];
    [self.webView setScalesPageToFit:YES];
    
    [self.view addSubview:[arrayOfWebViews objectAtIndex:0]];
    [self goToHomePage];
    
     
    
//    [self setCarouselViewController];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    if(self.URLString != nil)
    {
        NSLog(@"URL String is not nil.. so loading..");
        [self setWebviewUsingString:self.URLString];
        [self setURLString:nil];
    }
    [self.navigationController.navigationBar setHidden:NO];
}

-(void)viewDidAppear:(BOOL)animated
{
    /*
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSInteger openFeedback = [defaults integerForKey:kOpenFeedback];
    if(openFeedback==YES){
        [self goToShowFeedbacks:nil];
        [defaults setInteger:NO forKey:kOpenFeedback];
        [defaults synchronize];
    }
     */
    [autocompleteTableView setDelegate:self];
    [autocompleteTableView setDataSource:self];
    
    [autocompleteArray removeAllObjects];
    [autocompleteTableView reloadData];
}

/*-(void)setCarouselViewController
{
    carouselViewController = [[ICarouselViewController alloc] init];
    [carouselViewController.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [carouselViewController setParentView:self.view];
}*/

-(void)goToHomePage
{
    userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *homePage = [userDefaults objectForKey:@"homepage"];
    if(homePage != nil)
    {
        [self setWebviewUsingString:homePage];
    }
    else
    {
        [self setWebviewUsingString:@"http://www.addteq.com"];
    }
}

-(void)createDatabase
{
    NSString *documentDirectory;
    NSArray *directoryPaths;
    
    // Get the documents directory
    directoryPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentDirectory = directoryPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:[documentDirectory stringByAppendingPathComponent:@"URL_History.sql"]];
    
//    NSLog(@"Database Path is : %@",databasePath);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:databasePath] == NO)
    {
        const char *dbPath = [databasePath UTF8String];
        if(sqlite3_open(dbPath, &DB) == SQLITE_OK)
        {
      //      NSLog(@"Database opened succesfully");
            char *errorMsg;
            NSString *createStmt =[NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS HistoryOfURLs (PastURLs TEXT PRIMARYKEY NOT NULL UNIQUE);"];
            if(sqlite3_exec(DB,[createStmt UTF8String],NULL,NULL,&errorMsg) != SQLITE_OK)
            {
         //       NSLog(@"Failed to create table");
            }
            else
            {
          //      NSLog(@"Table created successfully");
            }
            sqlite3_close(DB);
       //     NSLog(@"Database closed");
        }
        else
        {
      //      NSLog(@"Failed to open/create database");
        }
    }
    else
    {
  //      NSLog(@"Database already exists");
        sqlite3_close(DB);
  //      NSLog(@"Database closed");
    }
}

-(void)setAutocompleteProperties
{
    if(!urlsInDB)
    {
        self.urlsInDB = [[NSMutableArray alloc] init];
    }
    if(!autocompleteArray)
    {
        self.autocompleteArray = [[NSMutableArray alloc] init];
    }

    autocompleteTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    [self.autocompleteTableView setDelegate:self];
    [self.autocompleteTableView setDataSource:self];
    [self.autocompleteTableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [self.autocompleteTableView setScrollEnabled:YES];
    [self.autocompleteTableView setHidden:YES];
    [self.autocompleteTableView setAllowsSelectionDuringEditing:YES];
    [self.autocompleteTableView setAllowsSelection:YES];
    [self.autocompleteTableView setUserInteractionEnabled:YES];
    [self.autocompleteTableView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.autocompleteTableView.layer setShadowOffset:CGSizeMake(0, 1)];
    [self.autocompleteTableView.layer setShadowOpacity:1];
    [self.autocompleteTableView.layer setShadowRadius:1.25];
}

// Initializing the back button
-(void)setBackButton
{
//    NSLog(@"Setting Back Button");
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0,0,20,20)];
    [backButton setImage:[UIImage imageNamed:@"icon-chevron-left.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(getBack) forControlEvents:UIControlEventTouchUpInside];
    [backButton setHidden:YES];
    back = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [back setTintColor:[UIColor blueColor]];
}

// Initializing the forward button
-(void)setForwardButton
{
//    NSLog(@"Setting Front Button");
    forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [forwardButton setFrame:CGRectMake(0, 0, 20, 20)];
    [forwardButton setImage:[UIImage imageNamed:@"icon-chevron-right.png"] forState:UIControlStateNormal];
    [forwardButton addTarget:self action:@selector(getForward) forControlEvents:UIControlEventTouchUpInside];
    [forwardButton setHidden:YES];
    forward = [[UIBarButtonItem alloc] initWithCustomView:forwardButton];
    [forward setTintColor:[UIColor blueColor]];
}

- (void)getBack
{
 //   NSLog(@"back button pressed");
    [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] goBack];
}

- (void)getForward
{
 //   NSLog(@"fwd button pressed");
    [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] goForward];
}

// Initializing the back and front button in navigation controller items
-(void)setNavigationControllerItems
{
//    NSLog(@"Setting Navigation Controller Items");
    [self.navigationController setNavigationBarHidden:NO];
    UIBarButtonItem *showAllTabsBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-stacks"] style:UIBarButtonItemStylePlain target:self action:@selector(showAllTabs:)];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6
    [self.navigationItem setRightBarButtonItems: [NSArray arrayWithObjects:negativeSpacer,showAllTabsBarButton,negativeSpacer,nil]];
}

// Initializing the refresh button
-(void)setRefreshButton
{
 //   NSLog(@"Setting Refresh Button");
    refreshButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [refreshButton setImage:[UIImage imageNamed:@"icon-repeat.png"] forState:UIControlStateNormal];
    [refreshButton addTarget:self action:@selector(refresh) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setHomeButton
{
//    NSLog(@"Setting Home button");
    homeButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0,25,25)];
    [homeButton setImage:[UIImage imageNamed:@"icon-home.png"] forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(goToHomePage) forControlEvents:UIControlEventTouchUpInside];
}

// Initializing the search text field
-(void)setSearchTextField
{
//    NSLog(@"Setting Search TextField");
    self.locationField.delegate=self;
    [locationField setKeyboardType:UIKeyboardTypeDefault];
    [locationField setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [locationField setRightViewMode:UITextFieldViewModeUnlessEditing];
    [locationField setRightView:refreshButton];
    [locationField setLeftViewMode:UITextFieldViewModeUnlessEditing];
    [locationField setLeftView:homeButton];
    [locationField setClearButtonMode:UITextFieldViewModeWhileEditing];
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    lastContentOffset = scrollView.contentOffset.y;
}

-(void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat endOfScrollview = scrollView.frame.size.height;
    
    if (lastContentOffset > (int)scrollView.contentOffset.y) {
        // moved up, show toolbar
        [self handleSwipeDown];
    }else if(scrollView.contentOffset.y<=0){
        // show bar
        [self handleSwipeDown];
        
    }else if((scrollView.contentSize.height-scrollView.contentOffset.y) <= endOfScrollview){
        // show bar
        [self handleSwipeDown];
    }else{
        // hide bar
        [self handleSwipeUp];
    }
}




// Initializing the toolbar
-(void)setToolbar
{
    /*
    UIToolbar *toolbar = ((NavigationController *)self.parentViewController).toolbar;
    [toolbar setBackgroundImage:[[UIImage alloc]init] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
     */
     //    NSLog(@"Setting Toolbar");
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTintColor:[UIColor blueColor]];
    [button setFrame:CGRectMake(0,0,25,25)];
    [button setImage:[UIImage imageNamed:@"camera.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(saveImageInFolder:) forControlEvents:UIControlEventTouchUpInside];
    
    //UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] init];
    //[longPress addTarget:self action:@selector(goToAnnotateImage:)];
    
    //[button addGestureRecognizer:longPress];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [barButtonItem setTintColor:[UIColor blueColor]];
    
    UIBarButtonItem *jiraButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"jira.png"] style:UIBarButtonItemStylePlain target:self action:@selector(goToImageAnnotation)];
 //   barButtonItem.style = UIBarButtonItemStylePlain;
    
 //   UIBarButtonItem *sendFeedbackBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(goToAnnotateImage:)];
    
//    btn.showsTouchWhenHighlighted = YES; // keep the "glow" effect
//    [btn addTarget:self action:@selector(someSelector) forControlEvents:someEvent];
    // repeat as needed for different events/selectors
    // or add gesture recognizers
    
//    UIBarButtonItem *btnItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    //icon-chat.png
    UIBarButtonItem *showFeedbackBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"bubble.png"] style:UIBarButtonItemStylePlain target:self action:@selector(goToShowFeedbacks:)];
    //cog3.png settings.png
    UIBarButtonItem *showSettingsBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Settings-blue-32.png"] style:UIBarButtonItemStylePlain target:self action:@selector(goToSettings:)];
    UIBarButtonItem *showBookmarksBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks  target:self action:@selector(goToBookmarks:)];
//    UIBarButtonItem *showAllTabsBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-stacks"] style:UIBarButtonItemStylePlain target:self action:@selector(showAllTabs:)];
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:5];
    [buttons addObject:barButtonItem];
    [buttons addObject:flexibleSpace];
    [buttons addObject:jiraButton];
    [buttons addObject:flexibleSpace];
    [buttons addObject:showFeedbackBarButton];
    [buttons addObject:flexibleSpace];
    [buttons addObject:showSettingsBarButton];
    [buttons addObject:flexibleSpace];
    [buttons addObject:showBookmarksBarButton];
 //   [buttons addObject:flexibleSpace];
 //   [buttons addObject:showAllTabsBarButton];
    self.navigationController.toolbar.clipsToBounds = YES;
    [self setToolbarItems:buttons];
}

// Initial setting of the webview.
-(void)setWebviewUsingString:(NSString*)URLStr
{
    NSLog(@"Setting WebView using string parameter");
    
    NSMutableString *urlMutableHttp = [[NSMutableString alloc] initWithString:@"http://"];
    NSURL *url;
    if(![URLStr hasPrefix:@"http://"] && ![URLStr hasPrefix:@"https://"])
    {
        [urlMutableHttp appendString:URLStr];
        url=[NSURL URLWithString:urlMutableHttp];
    }
    else
    {
        url=[NSURL URLWithString:URLStr];
    }
    NSURLRequest*request=[NSURLRequest requestWithURL:url];
    /*
    [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] setDelegate:self];
    [((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).scrollView setDelegate:self];
    [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] setScalesPageToFit:YES];
    */
     [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] loadRequest:request];
    
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webV
{
    //dimensionOfWebview = webV.frame;
//    NSLog(@"X : %f Y : %f Width : %f, Height : %f",dimensionOfWebview.origin.x,dimensionOfWebview.origin.y,dimensionOfWebview.size.width,dimensionOfWebview.size.height);
    NSLog(@"In web view did finish load");
/*    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6
    if((webV.canGoBack) && (webV.canGoForward))
    {
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,back,forward,nil]];
        [self.navigationItem setLeftItemsSupplementBackButton:NO];
        [backButton setHidden:NO];
        [forwardButton setHidden:NO];
    }
    else if((webV.canGoBack) && !(webV.canGoForward))
    {
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,back,nil]];
        [self.navigationItem setLeftItemsSupplementBackButton:NO];
        [backButton setHidden:NO];
        [forwardButton setHidden:YES];
    }
    else if(!(webV.canGoBack) && (webV.canGoForward))
    {
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,forward,nil]];
        [self.navigationItem setLeftItemsSupplementBackButton:NO];
        [backButton setHidden:YES];
        [forwardButton setHidden:NO];
    }
    else
    {
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:nil]];
        [self.navigationItem setLeftItemsSupplementBackButton:YES];
        [backButton setHidden:YES];
        [forwardButton setHidden:YES];
    }*/
    
    
    
    [self resetNavigationBarOnReload:webV];
    [self.locationField setText:[(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] stringByEvaluatingJavaScriptFromString:@"window.location.href"]];
    [self getDetailsOfWebViews];
    
    
    
    
    
 /*   NSURLRequest *urlrequest = [webV request];
    NSURL *url = [urlrequest URL];
    NSString *urlString = [url absoluteString];
    NSLog(@"URL String : %@",urlString);
    [self.locationField setText:urlString];*/
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSMutableURLRequest *request;
    NSLog(@"Error code : %@",error);
    NSString *searchTerm  = [self.locationField.text stringByAddingPercentEscapesUsingEncoding:
                             NSASCIIStringEncoding];
    
    NSLog(@"%@",searchTerm);
    if(error.code < -999 || error.code == 101 || error.code==102)
    {
        NSLog(@"not here");
        userDefaults = [NSUserDefaults standardUserDefaults];
        
        int defaultSearchEngine = [[userDefaults objectForKey:@"Search_Engine"] integerValue];
        if(defaultSearchEngine == 0)
        {
            //Google
 //           NSLog(@"Search Term:%@", searchTerm);
            NSString *searchRequest = [[NSString alloc] initWithFormat:@"%@%@", @"https://www.google.com/search?q=", searchTerm];
            request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:searchRequest]];
            [((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]) loadRequest:request];
        }
        else if(defaultSearchEngine == 1)
        {
            //Yahoo
 //           NSLog(@"Search Term:%@", searchTerm);
            NSString *searchRequest = [[NSString alloc] initWithFormat:@"%@%@", @"http://www.search.yahoo.com/search?q=", searchTerm];
            request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:searchRequest]];
            [((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]) loadRequest:request];
        }
        else if (defaultSearchEngine == 2)
        {
            //Bing
   //         NSLog(@"Search Term:%@", searchTerm);
            NSString *searchRequest = [[NSString alloc] initWithFormat:@"%@%@", @"http://www.bing.com/search?q=", searchTerm];
            request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:searchRequest]];
            [((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]) loadRequest:request];
        }
    }
}

/*
// Handles the case to find when webview is scrolled up and it is scrolled down
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([scrollView isEqual:((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).scrollView])
    {
        float contentHeight = scrollView.contentSize.height;
        float height = scrollView.frame.size.height;
        float offset = scrollView.contentOffset.y;
        
        if(offset <= 1)
        {
            [self handleSwipeDown];
        }
        else if(height+offset <= contentHeight)
        {
            [self handleSwipeUp];
        }
    }
    else
    {
  //      NSLog(@"WebView is not equal to ScrollView");
    }
}
*/
// Handles swipe up on the webview
-(void)handleSwipeUp
{
    if(!self.navigationController.navigationBarHidden)
    {
 //       NSLog(@"Swiped up");
 //       NSLog(@"Before X : %f Y : %f Width : %f, Height : %f",dimensionOfWebview.origin.x,dimensionOfWebview.origin.y,dimensionOfWebview.size.width,dimensionOfWebview.size.height);
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        [self.navigationController setToolbarHidden:YES animated:YES];
        [UIView animateWithDuration:0.3 animations:^{
            if([((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]) isEqual:self.webView])
            {
                [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] setFrame:CGRectMake(0, 20, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.width, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.height)];
            }
            else
            {
                UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
                if (UIInterfaceOrientationIsPortrait(interfaceOrientation))
                {
                    [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] setFrame:CGRectMake(0, 20, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.width, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.height+44)];
                }
                else
                {
                    [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] setFrame:CGRectMake(0, 20, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.width, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.height+32)];
                }
            }
        }];
       // dimensionOfWebview = ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame;
  //      NSLog(@"After X : %f Y : %f Width : %f, Height : %f",dimensionOfWebview.origin.x,dimensionOfWebview.origin.y,dimensionOfWebview.size.width,dimensionOfWebview.size.height);
    }
    //[self.navigationController setToolbarHidden:NO];
}

// Handles swipe down on the webview
-(void)handleSwipeDown
{
    if(self.navigationController.navigationBarHidden)
    {
 //       NSLog(@"Swiped Down");
       // dimensionOfWebview = ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame;
 //       NSLog(@"Before X : %f Y : %f Width : %f, Height : %f",dimensionOfWebview.origin.x,dimensionOfWebview.origin.y,dimensionOfWebview.size.width,dimensionOfWebview.size.height);
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [self.navigationController setToolbarHidden:NO animated:YES];
        [UIView animateWithDuration:0.3 animations:^{
            
            UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
            if (UIInterfaceOrientationIsPortrait(interfaceOrientation))
            {
                if([((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]) isEqual:self.webView])
                {
                    [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] setFrame:CGRectMake(0,64, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.width, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.height)];
                }
                else
                {
                    [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] setFrame:CGRectMake(0,64, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.width, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.height-44)];
                }
            }
            else
            {
                if([((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]) isEqual:self.webView])
                {
                    [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] setFrame:CGRectMake(0,52, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.width, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.height)];
                }
                else
                {
                    [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] setFrame:CGRectMake(0,52, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.width, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.height-32)];
                }
            }
        }];
       // dimensionOfWebview = ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame;
 //       NSLog(@"After X : %f Y : %f Width : %f, Height : %f",dimensionOfWebview.origin.x,dimensionOfWebview.origin.y,dimensionOfWebview.size.width,dimensionOfWebview.size.height);
    }
    //[self.navigationController setToolbarHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    //self.webView.delegate=nil;
    // Dispose of any resources that can be recreated.
}

// To close the first responder : keypad
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [locationField resignFirstResponder];
}

// Function called when camera button is clicked
- (IBAction)goToAnnotateImage:(UILongPressGestureRecognizer *)sender {
    if(sender.state == UIGestureRecognizerStateBegan)
    {
        NSLog(@"Long presses annotate image");
        
        UIStoryboard *iPhoneStoryBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        EditScreenshotsViewController *editSSVC = [iPhoneStoryBoard instantiateViewControllerWithIdentifier:@"EditScreenshots"];
        if(!self.navigationController.navigationBarHidden)
        {
                   NSLog(@"Hide Navigation bar");
            [self.navigationController setNavigationBarHidden:YES animated:YES];
            [UIView animateWithDuration:0.3 animations:^{
                
            }];
        }
        [self.navigationController pushViewController:editSSVC animated:YES];
        

        
        
        
    
    }
}

-(void) goToImageAnnotation{
    UIStoryboard *iPhoneStoryBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    
    EditScreenshotsViewController *editSSVC = [iPhoneStoryBoard instantiateViewControllerWithIdentifier:@"EditScreenshots"];
    if(!self.navigationController.navigationBarHidden)
    {
        NSLog(@"Hide Navigation bar");
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        [UIView animateWithDuration:0.3 animations:^{
            
        }];
    }
    [self.navigationController pushViewController:editSSVC animated:YES];
}

-(IBAction)saveImageInFolder:(id)sender
{
    NSLog(@"Short presses save image in folder");
    UIImage *largeImage = [self getImageOfWebView:(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]];
    CGRect cropRect = CGRectMake(0, 0, largeImage.size.width, largeImage.size.height-108);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([largeImage CGImage], cropRect);
    // or use the UIImage wherever you like
    UIImage *image = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    NSData *png = UIImagePNGRepresentation(image);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Screenshots"];
    NSError *error = nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString *imageName1 = @"image";
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"_YYYYMMdd_HHmmSS"];
    NSString *dateString = [dateFormat stringFromDate:date];
    NSString *fullfilename = [imageName1 stringByAppendingString:dateString];
    NSString *fullNameWithExtn = [fullfilename stringByAppendingString:@".png"];
    NSString *filePath = [dataPath stringByAppendingPathComponent:fullNameWithExtn];
    [png writeToFile:filePath atomically:YES];
    NSLog(@"%@ path", dataPath);
    UIView *flashView = [[UIView alloc] initWithFrame:self.view.frame];
    [flashView setBackgroundColor:[UIColor whiteColor]];
    //[flashView setAlpha:0.5f];
    [self.webView addSubview:flashView];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:1.0f
         
                         animations:^{
                             
                             [flashView setAlpha:0.f];
                             
                         }
         
                         completion:^(BOOL finished){
                             
                             [flashView removeFromSuperview];
                             
                         }
         
         ];
        
    });
}

// Function called when the show feedbacks button is clicked
- (IBAction)goToShowFeedbacks:(id)sender {
    NSLog(@"Show feedbacks");

    /* HEE JIN */
    UIStoryboard *iPhoneStoryBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    ShowFeedbackViewController *showfeedbackViewController = [iPhoneStoryBoard instantiateViewControllerWithIdentifier:@"Feedbacks"];
    if(self.navigationController.navigationBarHidden)
    {
        //        NSLog(@"Navigation bar is hidden");
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [UIView animateWithDuration:0.3 animations:^{
            
        }];
    }
    else
    {
        //       NSLog(@"Navigation bar is not hidden");
    }
    [self.navigationController pushViewController:showfeedbackViewController animated:YES];

    /** HEE JIN **/

}

// Function called when settings button is clicked
- (IBAction)goToSettings:(id)sender {
//    NSLog(@"Settings");
    UIStoryboard *iPhoneStoryBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    SettingsViewController *settingsViewController = [iPhoneStoryBoard instantiateViewControllerWithIdentifier:@"Settings"];
    if(self.navigationController.navigationBarHidden)
    {
//        NSLog(@"Navigation bar is hidden");
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [UIView animateWithDuration:0.3 animations:^{
        
        }];
    }
    else
    {
 //       NSLog(@"Navigation bar is not hidden");
    }
    [self.navigationController pushViewController:settingsViewController animated:YES];
}

// Function called when go to bookmarks button is clicked
- (IBAction)goToBookmarks:(id)sender {
    NSLog(@"Bookmarks");
    NSString *other1 = @"Add Bookmark";
    NSString *other2 = @"Show Bookmarks";
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@""
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, nil];
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [actionSheet showInView:self.view];
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if([buttonTitle isEqualToString:@"Add Bookmark"])
    {
//        NSLog(@"Add Bookmark pressed");
        [self addBookmark];
    }
    else if([buttonTitle isEqualToString:@"Show Bookmarks"])
    {
//        NSLog(@"Show Bookmark pressed");
        [self showBookmark];
    }
    else if([buttonTitle isEqualToString:@"Cancel"])
    {
//        NSLog(@"Cancel Bookmark Action sheet");
    }
}

-(void)addBookmark
{
    self.webView = (UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView];
    NSString *currentURL = [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] stringByEvaluatingJavaScriptFromString:@"window.location.href"];
    NSLog(@"%@",currentURL);
    NSString *URLTitle = [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] stringByEvaluatingJavaScriptFromString:@"document.title"];
    NSLog(@"%@",URLTitle);
    NSURL *currURL = [NSURL URLWithString:currentURL];
    
    addBookmarkViewController = [[AddBookmarkViewController alloc] init];
    [addBookmarkViewController setBookmark:URLTitle url:currURL];
    [self presentViewController:addBookmarkViewController animated:YES completion:nil];
    
    
}

-(void)showBookmark
{
    self.webView = (UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView];
    NSString *currentURL = [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] stringByEvaluatingJavaScriptFromString:@"window.location.href"];
    NSLog(@"%@",currentURL);
    NSString *URLTitle = [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] stringByEvaluatingJavaScriptFromString:@"document.title"];
    NSLog(@"%@",URLTitle);
    NSURL *currURL = [NSURL URLWithString:currentURL];

    showBookmarkViewController = [[ShowBookmarkViewController alloc] init];
    [showBookmarkViewController setDelegate:self];
    [showBookmarkViewController setBookmark:URLTitle url:currURL];
    [self presentViewController:showBookmarkViewController animated:YES completion:nil];
   
    
}

// This function is used for getting the URL from the show bookmarks view controller when
// an URL is pressed there.
-(void)addURLViewController:(id)controller didFinishEnteringURL:(NSString *)url
{
 //   NSLog(@"From show bookmarks..");
//    [self setURLString:url];
    NSLog(@"Number of tabs open : %d",[arrayOfWebViews count]);
    if([arrayOfWebViews count] < 16)
    {
        UIWebView *newWebView = [[UIWebView alloc] initWithFrame:self.webView.frame];
        newWebView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        newWebView.scalesPageToFit = YES;
        [arrayOfWebViews addObject:newWebView];
        currentIndexOfWebView = [arrayOfWebViews indexOfObject:newWebView];
        [self setWebviewUsingString:url];
        [self.webView removeFromSuperview];
        [self.view addSubview:[arrayOfWebViews objectAtIndex:currentIndexOfWebView]];
        [self resetNavigationBarOnReload:(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]];
        [self.locationField setText:[(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] stringByEvaluatingJavaScriptFromString:@"window.location.href"]];
    }
    else
    {
        [self setWebviewUsingString:url];
        [self resetNavigationBarOnReload:(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]];
        [self.locationField setText:[(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] stringByEvaluatingJavaScriptFromString:@"window.location.href"]];
    }
}

// This function is called after the new tab is pressed in the tabbed browser
-(void)newTabCarouselViewController:(id)controller didCreateANewTab:(NSInteger)index
{
//    NSLog(@"This function is called after the new tab is pressed in the tabbed browser");
//    NSLog(@"Number of items in array : %d",[arrayOfWebViews count]);
     UIWebView *newWebView = [[UIWebView alloc] initWithFrame:self.webView.frame];
    newWebView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [newWebView setDelegate:self];
    [newWebView.scrollView setDelegate:self];
    [newWebView setScalesPageToFit:YES];
    [arrayOfWebViews addObject:newWebView];
//     NSLog(@"Index of new web view : %d",[arrayOfWebViews indexOfObject:newWebView]);
    currentIndexOfWebView = [arrayOfWebViews indexOfObject:newWebView];
    [self goToHomePage];
    //[self.webView removeFromSuperview];
    
    [self.view addSubview:[arrayOfWebViews objectAtIndex:currentIndexOfWebView]];
    NSLog(@"NEW TAB VIEW %d",[[self.view subviews] indexOfObject:[arrayOfWebViews objectAtIndex:currentIndexOfWebView]]);
}

// This function is called after an exiting tab is selected in the tabbed browser
-(void)selectTabCarouselViewController:(id)controller didSelectTabInCarousel:(NSInteger)index
{
//    NSLog(@"Index of item before clicked : %d",[self currentIndexOfWebView]);
//    NSLog(@"This function is called after an exiting tab is selected in the tabbed browser");
    [self setCurrentIndexOfWebView:index];
//    NSLog(@"Index of item after clicked : %d",[self currentIndexOfWebView]);
    //[self.webView removeFromSuperview];
    [self.view bringSubviewToFront:[arrayOfWebViews objectAtIndex:currentIndexOfWebView]];
    NSLog(@"SELECT TAB VIEW %d",[[self.view subviews] indexOfObject:[arrayOfWebViews objectAtIndex:currentIndexOfWebView]]);
    [self resetNavigationBarOnReload:(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]];
    [self.locationField setText:[(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] stringByEvaluatingJavaScriptFromString:@"window.location.href"]];
}

-(void)resetNavigationBarOnReload:(UIWebView *)webV
{
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6
    if((webV.canGoBack) && (webV.canGoForward))
    {
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,back,forward,nil]];
        [self.navigationItem setLeftItemsSupplementBackButton:NO];
        [backButton setHidden:NO];
        [forwardButton setHidden:NO];
    }
    else if((webV.canGoBack) && !(webV.canGoForward))
    {
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,back,nil]];
        [self.navigationItem setLeftItemsSupplementBackButton:NO];
        [backButton setHidden:NO];
        [forwardButton setHidden:YES];
    }
    else if(!(webV.canGoBack) && (webV.canGoForward))
    {
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,forward,nil]];
        [self.navigationItem setLeftItemsSupplementBackButton:NO];
        [backButton setHidden:YES];
        [forwardButton setHidden:NO];
    }
    else
    {
        [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:nil]];
        [self.navigationItem setLeftItemsSupplementBackButton:YES];
        [backButton setHidden:YES];
        [forwardButton setHidden:YES];
    }
}

-(void)deleteTabCarouselViewController:(id)controller didDeleteTabInCarousel:(NSInteger)index
{
    NSLog(@"Delete tab at index : %d",index);
    UIWebView *tmpWebview = (UIWebView *) [arrayOfWebViews objectAtIndex:index];
    [tmpWebview removeFromSuperview];
    tmpWebview.delegate=nil;
    tmpWebview.scrollView.delegate = nil;
    tmpWebview=nil;
    [arrayOfWebViews removeObjectAtIndex:index];
    [webViewDetailsArray removeObjectAtIndex:index];
    if(index < currentIndexOfWebView)
    {
        currentIndexOfWebView -= 1;
    }
    else if(index == currentIndexOfWebView)
    {
        if(currentIndexOfWebView == 0)
        {
            currentIndexOfWebView = [arrayOfWebViews count]-1;
        }
        else
        {
            currentIndexOfWebView = currentIndexOfWebView-1;
        }
    }
}

-(void)doneTabCarouselViewController:(id)controller
{
    NSLog(@"DONE BUTTON %d",[[self.view subviews] indexOfObject:[arrayOfWebViews objectAtIndex:currentIndexOfWebView]]);
    [self.view bringSubviewToFront:[arrayOfWebViews objectAtIndex:currentIndexOfWebView]];
    [self resetNavigationBarOnReload:(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]];
    [self.locationField setText:[(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] stringByEvaluatingJavaScriptFromString:@"window.location.href"]];
}

-(UIImage *)getImageOfWebView:(UIWebView *)webV
{
//    NSLog(@"In get image of web view");
    CGRect tmpFrame = webV.frame;
    
    // set new Frame
    CGRect aFrame = CGRectMake(0, 0, webV.frame.size.width, webV.frame.size.height+64);
    webV.frame = aFrame;
    
    // do image magic
    UIGraphicsBeginImageContext([self.view sizeThatFits:[[UIScreen mainScreen] bounds].size]);
    
    CGContextRef resizedContext = UIGraphicsGetCurrentContext();
    [webV.layer renderInContext:resizedContext];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //   UIImage *scaledImage = [UIImage imageWithCGImage:[image CGImage] scale:(image.scale * 2.0) orientation:image.imageOrientation];
    
    // reset Frame of view to origin
    webV.frame = tmpFrame;
    return image;
}

-(void)getDetailsOfWebViews
{
    //NSLog(@"get details");
    webViewDetails = nil;
     webViewDetails = [[WebViewDetails alloc] init];
    [webViewDetails setWebViewTitle:[((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]) stringByEvaluatingJavaScriptFromString:@"document.title"]];
    [webViewDetails setWebViewImage:[self getImageOfWebView:((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView])]];
    if([webViewDetailsArray count] <= currentIndexOfWebView)
    {
        [webViewDetailsArray insertObject:webViewDetails atIndex:currentIndexOfWebView];
    }
    else
    {
        WebViewDetails *prev = [webViewDetailsArray objectAtIndex:currentIndexOfWebView];
        [webViewDetailsArray replaceObjectAtIndex:currentIndexOfWebView withObject:webViewDetails];
        prev = nil;
    }
/*    for(int i=0;i<[arrayOfWebViews count];i++)
    {
        webViewDetails = [[WebViewDetails alloc] init];
        [webViewDetails setWebViewTitle:[((UIWebView *)[arrayOfWebViews objectAtIndex:i]) stringByEvaluatingJavaScriptFromString:@"document.title"]];
        [webViewDetails setWebViewImage:[self getImageOfWebView:((UIWebView *)[arrayOfWebViews objectAtIndex:i])]];
        [webViewDetailsArray insertObject: webViewDetails atIndex:i];
    }*/
}

// Function called when show all tabs button is clicked
- (IBAction)showAllTabs:(id)sender {
 //   [carouselViewController resetTabs];
    carouselViewController = nil;
    carouselViewController = [[ICarouselViewController alloc] init];
    [carouselViewController.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [carouselViewController setParentView:self.view];
 //   NSLog(@"%@",[((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]) stringByEvaluatingJavaScriptFromString:@"document.title"]);
 //   NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:self.webView, nil];
 //   [carouselViewController insertTab:arrayOfWebViews];
 //   [carouselViewController insertTab:arrayOfWebViews webViewDetails:[self getDetailsOfWebViews]];
    [carouselViewController insertTab:webViewDetailsArray];
 //   [carouselViewController insertTab:array];
    carouselViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [carouselViewController setDelegate:self];
    [self presentViewController:carouselViewController animated:YES completion:nil];
}

-(void)refresh
{
    [((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]) reload];
    NSLog(@"Webview reloaded");
}

-(void)cancel
{
    NSLog(@"Cancel search");
    [self.navigationController.navigationBar endEditing:YES];
    [self.autocompleteTableView setHidden:YES];
    UIBarButtonItem *showAllTabsBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-stacks"] style:UIBarButtonItemStylePlain target:self action:@selector(showAllTabs:)];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6
    [self.navigationItem setRightBarButtonItems: [NSArray arrayWithObjects:negativeSpacer,showAllTabsBarButton,negativeSpacer,nil]];
}

-(void)searchAutoCompleteEntriesWithSubstring:(NSString*)substring
{
    [autocompleteArray removeAllObjects];
    for(NSString *currentString in urlsInDB)
    {
        NSLog(@"Current String : %@",currentString);
 //       NSLog(@"Substring : %@",substring);
        
        NSRange substringRangeLowerCase = [currentString rangeOfString:[substring lowercaseString]];
        NSRange substringRangeUpperCase = [currentString rangeOfString:[substring uppercaseString]];
 //       NSLog(@"%d",substringRangeUpperCase.length);
 //       NSLog(@"%d",substringRangeLowerCase.length);
        
        if(substringRangeLowerCase.length != 0 || substringRangeUpperCase.length !=0)
        {
            [autocompleteArray addObject:currentString];
        }
        [autocompleteTableView reloadData];
    }
}

#pragma -- table view delegate methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSLog(@"Number of URLs in Database : %d",[urlsInDB count]);
//    NSLog(@"Number of URLs in Autocomplete array : %d",[autocompleteArray count]);
    return [autocompleteArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    if([autocompleteArray count] > 0)
    {
 //       NSLog(@"%@",[autocompleteArray objectAtIndex:indexPath.row]);
        [cell.textLabel setText:[autocompleteArray objectAtIndex:indexPath.row]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    [self.locationField setText:selectedCell.textLabel.text];
    [self.locationField resignFirstResponder];
    [self.autocompleteTableView setHidden:YES];
//    NSLog(@"Selected Text : %@",[indexPath description]);
//    NSLog(@"Selected Index : %i",[indexPath row]);
    [self setWebviewUsingString:selectedCell.textLabel.text];
    [self resetNavigationBarOnReload:(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]];
    [self.locationField setText:[(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] stringByEvaluatingJavaScriptFromString:@"window.location.href"]];
}

#pragma -- text delegate methods
/* This function is called when the text field is being edited to type in the URL. http:// is defaulted as the user can just type www.<>.com */
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [urlsInDB removeAllObjects];
    NSString *selectQuery = [NSString stringWithFormat:@"SELECT PastURLs FROM HistoryOfURLs"];
    sqlite3_stmt *statement;
    const char *dbPath = [databasePath UTF8String];
    if(sqlite3_open(dbPath, &DB) == SQLITE_OK)
    {
 //       NSLog(@"Database opened succesfully");
        if(sqlite3_prepare_v2(DB,[selectQuery UTF8String], -1, &statement, nil)==SQLITE_OK)
        {
            while (sqlite3_step(statement)==SQLITE_ROW)
            {
                char *pastURLs = (char *)sqlite3_column_text(statement,0);
                if(pastURLs != NULL)
                {
                    NSString *field = [[NSString alloc] initWithUTF8String:pastURLs];
                    [urlsInDB addObject:field];
                }
            }
        }
    }
    sqlite3_close(DB);
//    NSLog(@"Database closed");
    [self.view addSubview:autocompleteTableView];
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:nil]];
    [self.navigationItem setLeftItemsSupplementBackButton:NO];
    [backButton setHidden:YES];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:nil]];
    [forwardButton setHidden:YES];
    
    UIBarButtonItem *cancelButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    [self.navigationItem setRightBarButtonItem:cancelButtonItem];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self cancel];
}

// This function is used to load the URL typed in the search bar to the webview
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self setWebviewUsingString:textField.text];
    const char *dbPath = [databasePath UTF8String];
    if(sqlite3_open(dbPath, &DB) == SQLITE_OK)
    {
 //       NSLog(@"Database opened succesfully");
        NSString *textURL = locationField.text;
        NSString *insertQuery = [NSString stringWithFormat:@"INSERT INTO HistoryOfURLs (PastURLs) VALUES (\"%@\")",textURL];
//        NSLog(@"%@",insertQuery);
        char *errMsg;
        if(![urlsInDB containsObject:textURL])
        {
            if(sqlite3_exec(DB,[insertQuery UTF8String],NULL,NULL,&errMsg)!=SQLITE_OK)
            {
                sqlite3_close(DB);
                NSAssert(0,@"Couldn't insert");
            }
            else
            {
  //              NSLog(@"URL Inserted");
            }
        }
        sqlite3_close(DB);
  //      NSLog(@"Database Closed");
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self.autocompleteTableView setHidden:NO];
    NSString *subString = [NSString stringWithString:textField.text];
    subString = [subString stringByReplacingCharactersInRange:range withString:string];
    [self searchAutoCompleteEntriesWithSubstring:subString];
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if(toInterfaceOrientation == UIInterfaceOrientationPortrait)
    {
        
 //       NSLog(@"Rotate to Portrait");
        //reload carousel items for portrait mode
    }
    else
    {
  //      NSLog(@"Rotate to Landscape");
        //reload carousel item for landscape mode
    }
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if(fromInterfaceOrientation == UIInterfaceOrientationPortrait)
    {
        
 //       NSLog(@"Rotate from Portrait");
      // dimensionOfWebview = ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame;
 //       NSLog(@"B4 X : %f Y : %f Width : %f, Height : %f",dimensionOfWebview.origin.x,dimensionOfWebview.origin.y,dimensionOfWebview.size.width,dimensionOfWebview.size.height);
        if(![((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]) isEqual:self.webView])
        {
            [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] setFrame:CGRectMake(0,52, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.width, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.height+24)];
 //           NSLog(@"AF X : %f Y : %f Width : %f, Height : %f",dimensionOfWebview.origin.x,dimensionOfWebview.origin.y,dimensionOfWebview.size.width,dimensionOfWebview.size.height);
        }
    }
    else{
        
//        NSLog(@"Rotate from Landscape");
       // dimensionOfWebview = ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame;
//        NSLog(@"B4 X : %f Y : %f Width : %f, Height : %f",dimensionOfWebview.origin.x,dimensionOfWebview.origin.y,dimensionOfWebview.size.width,dimensionOfWebview.size.height);
        //reload carousel item for landscape mode
        if(![((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]) isEqual:self.webView])
        {
            [(UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView] setFrame:CGRectMake(0,52, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.width, ((UIWebView *)[arrayOfWebViews objectAtIndex:currentIndexOfWebView]).frame.size.height-24)];
 //           NSLog(@"AF X : %f Y : %f Width : %f, Height : %f",dimensionOfWebview.origin.x,dimensionOfWebview.origin.y,dimensionOfWebview.size.width,dimensionOfWebview.size.height);
        }
    }
}

@end

/* REFERENCE CODE FOR FUTURE USE ON SWIPE GESTURES
-(void)setSwipeGestureRecognizer
 {
 
 swipeRecognizerUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeUp)];
 swipeRecognizerUp.numberOfTouchesRequired = 1;
 swipeRecognizerUp.direction = UISwipeGestureRecognizerDirectionUp;
 swipeRecognizerUp.delegate = self;
 [self.webView.scrollView addGestureRecognizer:swipeRecognizerUp];
 [self.webView.scrollView.panGestureRecognizer requireGestureRecognizerToFail:swipeRecognizerUp];
 swipeRecognizerUp.cancelsTouchesInView = YES;
 
 swipeRecognizerDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeDown)];
 swipeRecognizerDown.numberOfTouchesRequired = 1;
 swipeRecognizerDown.direction = UISwipeGestureRecognizerDirectionDown;
 swipeRecognizerDown.delegate = self;
 [self.webView.scrollView addGestureRecognizer:swipeRecognizerDown];
 [self.webView.scrollView.panGestureRecognizer requireGestureRecognizerToFail:swipeRecognizerDown];
 swipeRecognizerDown.cancelsTouchesInView = YES;
 
 }*/

/*- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
 {
 if(gestureRecognizer == swipeRecognizerUp) {
 return webViewScrolledToBottom;
 } else if(gestureRecognizer == swipeRecognizerDown) {
 return webViewScrolledToTop;
 }
 
 return false;
 }
 
 - (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
 shouldRecognizeSimultaneouslyWithGestureRecognizer:
 (UIGestureRecognizer *)otherGestureRecognizer
 {
 return YES;
 }
 
 */

/* 
 Code to cancel Scroll View :-
 
 UIScrollView *scroll = [[webView subviews] lastObject];
 if([scroll isKindOfClass:[UIScrollView class]])
 {
 scroll = (UIScrollView*)scroll;
 [scroll setScrollEnabled:YES];
 }*/