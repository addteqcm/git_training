//
//  AppDelegate.h
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 8/23/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
