//
//  FeedbackLeftPanelViewController.m
//  Azura iOS7
//
//  Created by HeeJinChoi on 10/29/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "FeedbackLeftPanelViewController.h"
#import "ShowFeedbackViewController.h"
//#import "FeedbackCenterViewController.h"
#import "Projects.h"

#define PROJECT @"Project"
@interface FeedbackLeftPanelViewController ()
@end

@implementation FeedbackLeftPanelViewController
@synthesize searchProjArray;
@synthesize firsttime;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.projTable setBackgroundColor:self.view.backgroundColor];
    [_searchBarProject setDelegate:self];
    firsttime=true;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
  return @"Select Project";
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(firsttime){
        ShowFeedbackViewController *con = (ShowFeedbackViewController *) self.parentViewController;
        //return [con.center.projectsArray count];
        searchProjArray = [NSMutableArray arrayWithArray:con.center.projectsArray];
        firsttime = false;
    }
    
    return [searchProjArray count];
}


-(void) viewWillAppear:(BOOL)animated{
    /*
    if(self.secondLeftView.finish==false){
        NSLog(@"It is not end of choosing");
    }else{
        
        // the end of choosing.
        // hide left menu
     
        ShowFeedbackViewController *con = (ShowFeedbackViewController *)self.parentViewController;
        con.leftone=false;
        con.newissue=true;
        
        con.buttonProject.title=PROJECT;
        [con.left.view removeFromSuperview];
        self.secondLeftView.finish = false;

    }*/
}


-(UITableViewCell *) tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellidentifier = @"SimpleTableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    
    if(cell==nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    //ShowFeedbackViewController *con = (ShowFeedbackViewController *) self.parentViewController;
    //Projects *proj = con.center.projectsArray[[indexPath row]];
    Projects *proj =searchProjArray[[indexPath row]];
    cell.textLabel.text = proj.projectname;
    cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:proj.icon]]];
    cell.backgroundColor = self.view.backgroundColor;
    cell.textLabel.textColor = [UIColor blackColor];
    UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 14.0 ];
    cell.textLabel.font  = myFont;
    return cell;
}

- (BOOL) searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    // expand menu view as full screen
    [_delegate expandViewForSearch];
    return YES;
}

-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [_delegate shrinkView];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.searchDisplayController.searchResultsTableView){
        [_delegate ProjectSelectedFromSearch:[indexPath row]];
    }else{
        [_delegate ProjectSelected:[indexPath row]];
    }
    [self.searchDisplayController.searchBar resignFirstResponder];
    
    
}

-(void) searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller{
    ShowFeedbackViewController *con = (ShowFeedbackViewController *) self.parentViewController;
     self.searchProjArray = [NSMutableArray arrayWithArray:con.center.projectsArray];
    [self.projTable reloadData];
    
    [_delegate shrinkView];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString{
    
    ShowFeedbackViewController *con = (ShowFeedbackViewController *) self.parentViewController;
    [self.searchProjArray removeAllObjects];
    if([searchString length]!=0){
        
        for(Projects *p in con.center.projectsArray){
            if([p.projectname rangeOfString:searchString options:NSCaseInsensitiveSearch].location != NSNotFound){
                [self.searchProjArray addObject:p];
            }
        }
        
        
    }else{
        //NSLog(@"Do not search");
        self.searchProjArray = [NSMutableArray arrayWithArray:con.center.projectsArray];
    }
    [self.projTable reloadData];
    return YES;
}


@end
