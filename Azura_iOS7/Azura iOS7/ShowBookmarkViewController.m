//
//  ShowBookmarkViewController.m
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/3/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "ShowBookmarkViewController.h"

@interface ShowBookmarkViewController ()

@end

@implementation ShowBookmarkViewController

@synthesize doneButtonItem;
@synthesize bookmark;
@synthesize tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.tableView setDelegate:self];
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, 320, self.view.frame.size.height)  style:UITableViewStyleGrouped];
    [self.tableView setDataSource:self];
    [self.tableView setScrollEnabled:YES];
    
    naviBarObj = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 20, 320, 44)];
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 40, 320, 44)];
    [searchBar setDelegate:self];
    
    searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    [self.searchDisplayController setDelegate:self];
    [self.searchDisplayController setSearchResultsDataSource:self];
    [self.searchDisplayController setSearchResultsDelegate:self];
    [self.tableView setTableHeaderView:searchBar];
    
    UIBarButtonItem *editItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(editAction)];
    doneButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneAction)];
    
    UINavigationItem *navigItem = [[UINavigationItem alloc] initWithTitle:@"Show Bookmarks"];
    [navigItem setRightBarButtonItem:doneButtonItem];
    [navigItem setLeftBarButtonItem:editItem];
    
    [naviBarObj setItems:[NSArray arrayWithObjects:navigItem, nil]];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *bookmarks = [userDefaults objectForKey:@"bookmarks"];
    if(bookmarks)
    {
        bookmarksArray = [[NSMutableArray alloc] initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:bookmarks]];
    }
    else
    {
        bookmarksArray = [[NSMutableArray alloc] initWithCapacity:1];
    }
    
	searchText = [[NSString alloc] init];
    searchArray = [[NSMutableArray alloc] init];
    
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.tableView setAllowsSelectionDuringEditing:YES];
    
    [self.searchDisplayController setDelegate:self];
    [self.searchDisplayController setSearchResultsDataSource:self];
    [self.searchDisplayController setSearchResultsDelegate:self];
    
    [searchBar setDelegate:self];
    
    [naviBarObj setFrame:CGRectMake(0,20,self.view.frame.size.width,44)];
    [tableView setFrame:CGRectMake(0,60,self.view.frame.size.width,self.view.frame.size.height)];
    
    [naviBarObj setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [self.view addSubview:tableView];
    [self.view insertSubview:naviBarObj aboveSubview:tableView];
    
    NSIndexPath *tableSelection = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:tableSelection animated:NO];
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    CGRect frame = naviBarObj.frame;
    
    if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
    {
        frame.size.height = 44;
    }
    else
    {
        frame.size.height = 44;
    }
    naviBarObj.frame = frame;
}

-(void)doneAction
{
    [self saveBookmarks];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)editAction
{
    [self.tableView setEditing:YES animated:YES];
    [self.navigationItem setRightBarButtonItem:nil];
    
    UIBarButtonItem *doneEditingItem = [[UIBarButtonItem alloc] initWithTitle:@"Finish" style:UIBarButtonItemStylePlain target:self action:@selector(doneEditingAction)];
    UINavigationItem *navigItem = [[UINavigationItem alloc] initWithTitle:@"Show Bookmarks"];
    [navigItem setLeftBarButtonItem:doneEditingItem];
    [naviBarObj setItems:[NSArray arrayWithObjects:navigItem, nil]];
}

-(void)doneEditingAction
{
    UIBarButtonItem *editItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(editAction)];
    doneButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneAction)];
    UINavigationItem *navigItem = [[UINavigationItem alloc] initWithTitle:@"Show Bookmarks"];
    [navigItem setRightBarButtonItem:doneButtonItem];
    [navigItem setLeftBarButtonItem:editItem];
    [naviBarObj setItems:[NSArray arrayWithObjects:navigItem, nil]];
    [self.tableView setEditing:NO animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self setDoneButtonItem:nil];
    [self setBookmark:nil];
    searchDisplayController = nil;
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.tableView == searchDisplayController.searchResultsTableView)
    {
        return [searchArray count];
    }
    else
    {
        return [bookmarksArray count];
    }
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete)
    {
        [bookmarksArray removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *labelCellIdentifier = @"LabelCell";
    UITableViewCell *cell;
    cell = [self.tableView dequeueReusableCellWithIdentifier:labelCellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:labelCellIdentifier];
    }
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    [cell setEditingAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    if(self.tableView == self.searchDisplayController.searchResultsTableView)
    {
        bookmark = [searchArray objectAtIndex:indexPath.row];
    }
    else
    {
        bookmark = [bookmarksArray objectAtIndex:indexPath.row];
    }
    [cell.textLabel setText:bookmark.name];
    return cell;
}

-(void)filterContentForSearchText:(NSString *)searchtext scope:(NSString*)scope
{
    //clear the filtered array
    [searchArray removeAllObjects];
    
    for(BookMarkObject *bookmarkObj in bookmarksArray)
    {
        NSString *searchString = [NSString stringWithFormat:@"%@",bookmarkObj.name];
        NSRange range = [searchString rangeOfString:searchtext options:NSCaseInsensitiveSearch];
        
        if(range.location != NSNotFound)
        {
            [searchArray addObject:bookmarkObj];
            [searchDisplayController.searchResultsTableView reloadData];
        }
        NSLog(@"Search Array : %@",searchArray);
    }
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:nil];
    if([[searchString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length])
    {
        [self setTableView:searchDisplayController.searchResultsTableView];
    }
    else
    {
        [self setTableView:tableView];
    }
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    return YES;
}

-(void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    [self.tableView setContentOffset:CGPointMake(0,44.f) animated:YES];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchbar
{
    [searchbar resignFirstResponder];
}

-(void)searchBar:(id)sender
{
    [searchDisplayController setActive:YES animated:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.tableView.editing)
    {
        editBookmarkViewController = [[EditBookmarkViewController alloc] init];
        [self presentViewController:editBookmarkViewController animated:YES completion:nil];
        [editBookmarkViewController setBookmark:[bookmarksArray objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:editBookmarkViewController animated:YES];
    }
    else
    {
        NSLog(@"%i",indexPath.row);
        bookmark = [bookmarksArray objectAtIndex:indexPath.row];
        NSLog(@"%@",bookmark.name);
        NSLog(@"%@",bookmark.url.host);
        [self.delegate addURLViewController:self didFinishEnteringURL:[bookmark.url absoluteString]];
        [self saveBookmarks];
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    BookMarkObject *bookmarkToMove = [bookmarksArray objectAtIndex:sourceIndexPath.row];
    [bookmarksArray removeObjectAtIndex:sourceIndexPath.row];
    [bookmarksArray insertObject:bookmarkToMove atIndex:destinationIndexPath.row];
    [self saveBookmarks];
}

-(void)saveBookmarks
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[NSKeyedArchiver archivedDataWithRootObject:bookmarksArray] forKey:@"bookmarks"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)setBookmark:(NSString *)aName url:(NSURL *)aURL
{
    bookmark = [[BookMarkObject alloc] initWithName:aName andURL:aURL];
}

@end
