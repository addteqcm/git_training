//
//  EditScreenshotsViewController.m
//  Azura iOS7
//
//  Created by HeeJinChoi on 1/15/14.
//  Copyright (c) 2014 addteq. All rights reserved.
//

#import "EditScreenshotsViewController.h"
#import "ShowFeedbackViewController.h"
#import "GzColors.h"

#define kActionSheetColor       100
#define kActionSheetTool        101

#define kSelectedSS @"selectedSS"
#define kDeletedSS @"deletedSS"
#define kFeedbackImages @"feedbackImages"


@interface EditScreenshotsViewController ()

@end

@implementation EditScreenshotsViewController
@synthesize openListFlag;
@synthesize openToolFlag;
@synthesize addSSVC;
@synthesize slideMenuWidth;

@synthesize mainImage;
@synthesize mainView;

@synthesize radialMenu;
@synthesize drawingView;
@synthesize skyblue;
@synthesize greenColor;
@synthesize filename;
@synthesize arrayDeleted;
@synthesize arraySelected;

@synthesize lineInDrawingView=_lineInDrawingView;

@synthesize curIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    slideMenuWidth = 240;
    
    UIStoryboard *iphonestoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    addSSVC = [iphonestoryboard instantiateViewControllerWithIdentifier:@"addScreenshots"];
    addSSVC.delegate = self;
    [self addChildViewController:addSSVC];
    
    CGRect addFrame = self.addSSVC.view.frame;
    addFrame.size.width = slideMenuWidth;
    addFrame.origin.x = -slideMenuWidth;
    self.addSSVC.view.frame = addFrame;
    [self.wholeView addSubview:addSSVC.view];
    
    // load feedback images
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    addSSVC.arrayThumbnails  = [NSMutableArray arrayWithArray:[defaults objectForKey:kFeedbackImages]];
    
    [addSSVC.thumbnailList reloadData];
    
    if([addSSVC.arrayThumbnails count]!=0){
        
        [self setCurIndex:0];
        
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *dataPath = [docDir stringByAppendingPathComponent:@"/Screenshots"];
        filename = [addSSVC.arrayThumbnails objectAtIndex:0];
        NSString *filePath = [dataPath stringByAppendingPathComponent:filename];
        self.mainImage = [[UIImageView alloc]initWithImage:[UIImage imageWithContentsOfFile:filePath]];
        
    }else{
        [self setCurIndex:-1];
        self.mainImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
    }
    CGRect imageFrame = self.mainImage.frame;
    
    self.mainView = [[JMCSketchContainerView alloc] initWithFrame:imageFrame];
    [self.mainView addSubview:self.mainImage];
    [self.imageScrollView addSubview:self.mainView];
    
    openListFlag = false;
    openToolFlag = false;
    
    UITapGestureRecognizer *tapGR;
    tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGR.numberOfTapsRequired = 1;
    [self.centerView addGestureRecognizer:tapGR];
    
    
    self.imageScrollView.contentSize = CGSizeMake(0, 520);
   
    radialMenu = [[ALRadialMenu alloc] init];
    radialMenu.delegate = self;
    
    drawingView = [[ACEDrawingView alloc]initWithFrame:imageFrame];
    drawingView.delegate = self;
    
    drawingView.lineWidth=5;
    skyblue = [UIColor colorWithRed:0.0f green:122.0/255.0f blue:1.0f alpha:1.0f];
    greenColor = [UIColor colorWithRed:0.0f green:132.0/255.0f blue:0.0f alpha:1.0f];
    drawingView.lineColor = skyblue;
    //self.colorPickerButton.backgroundColor = skyblue;
    [drawingView setting:self.mainImage];
    drawingView.cropView = self.mainView;
    
    if([addSSVC.arrayThumbnails count]!=0){
        [self.mainView addSubview:drawingView];
    }
   
    [self.imageScrollView setCanCancelContentTouches:NO];
    self.imageScrollView.maximumZoomScale = 8.0;
    self.imageScrollView.minimumZoomScale = self.imageScrollView.frame.size.width / 320;
    self.imageScrollView.delaysContentTouches = YES;
    [self.imageScrollView setScrollEnabled:NO];
    
    
    
}

-(void) selectImageAtIndex:(int)index{
    NSLog(@"select one index %d", index);
    if(index != self.curIndex){
        [self.drawingView deleteOne:0];
    }
    
    [self setCurIndex:index];
    
    
    self.filename = [addSSVC.arrayThumbnails objectAtIndex:index];
    
    [self setImageWithResizing];
    
    [self openList:nil];
}

-(void)deleteImageAtIndex:(int)index{
    
    // After deleting image, change the mainview image
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:addSSVC.arrayThumbnails forKey:kFeedbackImages];
    [defaults synchronize];
    

    NSLog(@"delete it");
    [self.drawingView deleteOne:0];
    if([addSSVC.arrayThumbnails count]==0){
        // if user does not have any image, show nothing
        self.mainImage.image = nil;
        [self.drawingView removeFromSuperview];
        [self setCurIndex:-1];
        
        
    }else{
         [self.mainView addSubview:drawingView];
        // if user has some image
        if(self.curIndex == index){
            // deleted image is same image as currently selected.
            // change image
            
            
            if([addSSVC.arrayThumbnails count]==index){
                filename = [addSSVC.arrayThumbnails objectAtIndex:(index-1)];
                [self setCurIndex:(index-1)];
            }else{
                filename = [addSSVC.arrayThumbnails objectAtIndex:index];
                [self setCurIndex:index];
            }
            [self setImageWithResizing];
        }
        
    }
    
}

-(void) setImageWithResizing{
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dataPath = [docDir stringByAppendingPathComponent:@"/Screenshots"];
    NSString *filePath = [dataPath stringByAppendingPathComponent:filename];
    
    UIImage *image = [UIImage imageWithContentsOfFile:filePath];
    
    
    CGRect imageFrame = self.mainImage.frame;
    imageFrame.size.width = image.size.width;
    imageFrame.size.height = image.size.height;
    [self.mainImage setFrame:imageFrame];
    [self.drawingView setFrame:imageFrame];
    [self.mainImage setImage:image];
}


-(void) viewWillDisappear:(BOOL)animated{
    [self.navigationController.toolbar setHidden:NO];
}

-(void) viewDidAppear:(BOOL)animated{
    
    
}



-(void) viewDidLayoutSubviews{
    
    CGSize boundsSize = self.centerView.frame.size;
    CGRect tFrame = self.addSSVC.thumbnailList.frame;
    tFrame.size.height = boundsSize.height;
    self.addSSVC.slideMenuHeight = boundsSize.height;
    self.addSSVC.thumbnailList.frame = tFrame;
    
    /*
    //CGSize boundsSize = self.centerView.frame.size;
    CGRect frameToCenter = self.mainView.frame;
    if(boundsSize.width>frameToCenter.size.width){
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) * 0.5f;
    }else{
        frameToCenter.origin.x = 0;
    }
    self.mainView.frame = frameToCenter;
     */
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
    if(openListFlag){
        
        [self openList:nil];
    }
}

-(void) viewWillAppear:(BOOL)animated{
   
    [self.navigationController.navigationBar setHidden:YES];
    //[self.navigationController.toolbar setHidden:YES];
    [self.centerBottomView setHidden:YES];
    [self.drawingView deleteOne:0];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    arraySelected =[NSMutableArray arrayWithArray:[defaults objectForKey:kSelectedSS]];
    arrayDeleted = [NSMutableArray arrayWithArray:[defaults objectForKey:kDeletedSS]];
    
    if([arraySelected count]!=0 || [arrayDeleted count]!=0){
        if([arrayDeleted count]!=0){
            NSLog(@"not null %@", arrayDeleted);
            for(int i =0; i< [arrayDeleted count]; i++){
                NSString *added = [arrayDeleted objectAtIndex:i];
                NSString *exist = @"";
                for(int j=0; j<[addSSVC.arrayThumbnails count]; j++){
                    exist = [addSSVC.arrayThumbnails objectAtIndex:j];
                    if([exist isEqualToString:added]){
                        [addSSVC.arrayThumbnails removeObject:exist];
                        NSLog(@"remove %d", [addSSVC.arrayThumbnails count]);
                    }
                }
            }
            [arrayDeleted removeAllObjects];
            [defaults setObject:arrayDeleted forKey:kDeletedSS];
        }
    
        if([arraySelected count]!=0){
            // add images to feedback images
            for(int i =0; i< [arraySelected count]; i++){
                int flag=0;
                NSString *added = [arraySelected objectAtIndex:i];
                NSString *exist = @"";
                for(int j=0; j<[addSSVC.arrayThumbnails count]; j++){
                    exist = [addSSVC.arrayThumbnails objectAtIndex:j];
                    if([exist isEqualToString:added]){
                        flag=1;
                    }
                }
                if(flag==0){
                    [addSSVC.arrayThumbnails addObject:added];
                }
            }
            [arraySelected removeAllObjects];
            [defaults setObject:arraySelected forKey:kSelectedSS];
        }
        
        [defaults setObject:addSSVC.arrayThumbnails forKey:kFeedbackImages];
        [defaults synchronize];
        [addSSVC.thumbnailList reloadData];
        
        if([addSSVC.arrayThumbnails count]!=0){
            [self.mainView addSubview:drawingView];
            filename = [addSSVC.arrayThumbnails objectAtIndex:[addSSVC.arrayThumbnails count]-1];
            [self setCurIndex:([addSSVC.arrayThumbnails count]-1)];
            [self setImageWithResizing];
        }else{
            self.mainImage.image = nil;
             [self.drawingView removeFromSuperview];
        }
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)openTool:(id)sender {
    [radialMenu buttonsWillAnimateFromButton:self.toolButton withFrame:CGRectMake(130, 10, 200, 25) inView:self.centerView];
}

- (IBAction)openList:(id)sender {
    //NSLog(@"open");
    
    if([radialMenu.items count]){
        [radialMenu buttonsWillAnimateFromButton:self.toolButton withFrame:CGRectMake(130, 10, 200, 25) inView:self.centerView];
        [self.drawingView setExclusiveTouch:NO];
    }else{
        
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect cTop = self.centerTopView.frame;
            CGRect addFrame = addSSVC.view.frame;
            CGRect imageScroll = self.imageScrollView.frame;
        
            if(openListFlag){
                [self.drawingView setHidden:NO];
                cTop.origin.x = 0;
                imageScroll.origin.x = 0;
                addFrame.origin.x = -slideMenuWidth;
                openListFlag = false;
            
                addSSVC.view.layer.masksToBounds=NO;
                addSSVC.view.layer.cornerRadius = 0;
                addSSVC.view.layer.shadowOffset = CGSizeMake(5, 20);
                addSSVC.view.layer.shadowRadius = 0;
                addSSVC.view.layer.shadowOpacity = 0;
            
                [self.toolButton setEnabled:YES];
                [self.colorPickerButton setEnabled:YES];
                [self.navigationController.toolbar setHidden:NO];

            }else{
            
                 NSLog(@"open it");
                
                [self.drawingView setHidden:YES];
                
                cTop.origin.x = slideMenuWidth;
                imageScroll.origin.x = slideMenuWidth;
                addFrame.origin.x = 0;
                openListFlag = true;
            
                addSSVC.view.layer.masksToBounds=NO;
                addSSVC.view.layer.cornerRadius = 3;
                addSSVC.view.layer.shadowOffset = CGSizeMake(5, 20);
                addSSVC.view.layer.shadowRadius = 3;
                addSSVC.view.layer.shadowOpacity = 0.5;
            
                [self.toolButton setEnabled:NO];
                [self.colorPickerButton setEnabled:NO];
                [self.navigationController.toolbar setHidden:YES];
                //if(openToolFlag){
                //    [self openTool:nil];
                //}
            
            }
            self.centerTopView.frame= cTop;
            self.imageScrollView.frame = imageScroll;
            self.addSSVC.view.frame = addFrame;
       
        }];
    
    }
    
    
}

- (IBAction)pickColors:(id)sender {
    if (!self.wePopoverController) {
		
		ColorViewController *contentViewController = [[ColorViewController alloc] init];
        contentViewController.delegate = self;
		self.wePopoverController = [[WEPopoverController alloc] initWithContentViewController:contentViewController];
		self.wePopoverController.delegate = self;
		self.wePopoverController.passthroughViews = [NSArray arrayWithObject:self.navigationController.navigationBar];
		
        CGRect newframe = self.colorPickerButton.frame;
        //newframe.origin.x += 50;
        newframe.origin.y += 20;
        
        
		[self.wePopoverController presentPopoverFromRect:newframe
                                                  inView:self.view
                                permittedArrowDirections:(UIPopoverArrowDirectionUp|UIPopoverArrowDirectionDown)
                                                animated:YES];
        
	} else {
		[self.wePopoverController dismissPopoverAnimated:YES];
		self.wePopoverController = nil;
	}
}
#pragma mark -
#pragma mark WEPopoverControllerDelegate implementation

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)thePopoverController {
	//Safe to release the popover here
	self.wePopoverController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)thePopoverController {
	//The popover is automatically dismissed if you click outside it, unless you return NO here
	return YES;
}

-(void) colorPopoverControllerDidSelectColor:(NSString *)hexColor{
    
    if([hexColor isEqualToString:@"0xFF000000"]){
        // black
        [self.colorPickerButton setImage:[UIImage imageNamed:@"droplet_black.png"] forState:UIControlStateNormal];
        self.drawingView.lineColor = [UIColor blackColor];
    }else if([hexColor isEqualToString:@"0xFF008000"]){
        // green
       [self.colorPickerButton setImage:[UIImage imageNamed:@"droplet_green.png"] forState:UIControlStateNormal];
        self.drawingView.lineColor = greenColor;
    }else if([hexColor isEqualToString:@"0xFF007AFF"]){
        // skyblue
        [self.colorPickerButton setImage:[UIImage imageNamed:@"droplet_skyblue.png"] forState:UIControlStateNormal];
        self.drawingView.lineColor = skyblue;
    }else if([hexColor isEqualToString:@"0xFFFF0000"]){
        // red
        [self.colorPickerButton setImage:[UIImage imageNamed:@"droplet_red.png"] forState:UIControlStateNormal];
        self.drawingView.lineColor = [UIColor redColor];
    }
    
   
    [self.wePopoverController dismissPopoverAnimated:YES];
    self.wePopoverController = nil;
}

- (IBAction)deleteCurImage:(id)sender {
    
    // clear all the images
    [self.drawingView deleteOne:0];
}


#pragma mark - UIAlertViewDelegate Methods

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Save"]){
        
        UIGraphicsBeginImageContext(self.mainImage.image.size);
        [self.mainImage.image drawAtPoint:CGPointZero];
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        [self.drawingView.layer renderInContext:context];
        UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSString *documentsDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *pngFilePath = [NSString stringWithFormat:@"%@/Screenshots/%@",documentsDirPath, filename];
        [UIImagePNGRepresentation(screenshot) writeToFile:pngFilePath atomically:YES];
        
        [addSSVC.thumbnailList reloadData];
    }
}


- (IBAction)undo:(id)sender {
    [self.drawingView undoLatestStep];
    //[self.drawingView setNeedsDisplay];
}

- (IBAction)save:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save" message:@"Do you want to save this image?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
    [alert show];
}

- (IBAction)redo:(id)sender {
    [self.drawingView redoLatestStep];
    //[self.drawingView setNeedsDisplay];
}

- (IBAction)done:(id)sender {
    NSLog(@"Go to Feedback");
    // store all the information and go to the feedback view
    NSMutableArray *selectedimages = [[NSMutableArray alloc] init];
    
    if([addSSVC.arrayThumbnails count]!=0){
        
        for(int i =0; i< [addSSVC.arrayThumbnails count]; i++){
            NSString *name = [addSSVC.arrayThumbnails objectAtIndex:i];
            [selectedimages addObject:name];
        }
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:selectedimages forKey:kFeedbackImages];
        [defaults synchronize];
        
        
    }
    
    UIStoryboard *iPhoneStoryBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    ShowFeedbackViewController *showfeedbackViewController = [iPhoneStoryBoard instantiateViewControllerWithIdentifier:@"Feedbacks"];
    if(self.navigationController.navigationBarHidden)
    {
        //        NSLog(@"Navigation bar is hidden");
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [UIView animateWithDuration:0.3 animations:^{
            
        }];
    }
    else
    {
        //       NSLog(@"Navigation bar is not hidden");
    }
    [self.navigationController pushViewController:showfeedbackViewController animated:YES];

}

-(void)handleTap:(UITapGestureRecognizer *)sender{
    
    if(sender.state == UIGestureRecognizerStateEnded){
        if(openListFlag==true){
            [self openList:nil];
        }
        
    }
}

#pragma mark - radial menu delegate methods
- (NSInteger) numberOfItemsInRadialMenu:(ALRadialMenu *)radialmenu {
    return 8;
}

- (NSInteger) arcSizeForRadialMenu:(ALRadialMenu *)radialmenu {
    return 180;
	
}


- (NSInteger) arcRadiusForRadialMenu:(ALRadialMenu *)radialmenu {
	return 120;
}

- (UIImage *) radialMenu:(ALRadialMenu *)radialmenu imageForIndex:(NSInteger) index {
	
    if (index == 1) {
        return [UIImage imageNamed:@"new_pen.png"];
    } else if (index == 2) {
        //return [UIImage imageNamed:@"minus.png"];
        return [UIImage imageNamed:@"new_line_40by40.png"];
    } else if (index == 3) {
        //return [UIImage imageNamed:@"checkbox-unchecked.png"];
        return [UIImage imageNamed:@"new_rect_40by40.png"];
    } else if (index == 4) {
        return [UIImage imageNamed:@"new_arrow.png"];
    } else if (index == 5) {
        //return [UIImage imageNamed:@"radio-unchecked.png"];
        return [UIImage imageNamed:@"new_circle_40by40.png"];
    } else if (index == 6) {
        return [UIImage imageNamed:@"eye_not_see.png"];
    } else if(index == 7){
        return [UIImage imageNamed:@"crop.png"];
    } else if(index == 8){
        return [UIImage imageNamed:@"move.png"];
    }
	
    return nil;
    
}

- (void) radialMenu:(ALRadialMenu *)radialMenu didSelectItemAtIndex:(NSInteger)index {
    
    
    if (index == 1) {
        self.drawingView.drawTool = ACEDrawingToolTypePen;
        [self.radialMenu itemsWillDisapearIntoButton:self.toolButton];
    } else if (index == 2) {
        self.drawingView.drawTool = ACEDrawingToolTypeLine;
        [self.radialMenu itemsWillDisapearIntoButton:self.toolButton];
    } else if (index == 3) {
        self.drawingView.drawTool = ACEDrawingToolTypeRectagleStroke;
        [self.radialMenu itemsWillDisapearIntoButton:self.toolButton];
    } else if (index == 4) {
        self.drawingView.drawTool=ACEDrawingToolTypeArrow;
        [self.radialMenu itemsWillDisapearIntoButton:self.toolButton];
    } else if (index == 5) {
        self.drawingView.drawTool = ACEDrawingToolTypeEllipseStroke;
        [self.radialMenu itemsWillDisapearIntoButton:self.toolButton];
    } else if (index == 6) {
        self.drawingView.drawTool = ACEDrawingToolTypeRectagleFill;
        [self.radialMenu itemsWillDisapearIntoButton:self.toolButton];
    }else if (index == 7) {
        self.drawingView.drawTool = ACEDrawingToolTypeCrop;
        [self.radialMenu itemsWillDisapearIntoButton:self.toolButton];
    }else if(index==8){
        self.drawingView.drawTool = ACEDRawingToolTypeMove;
        [self.radialMenu itemsWillDisapearIntoButton:self.toolButton];
    }
}




@end
