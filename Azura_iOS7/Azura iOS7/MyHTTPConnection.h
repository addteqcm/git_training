//
//  MyHTTPConnection.h
//  Azura
//
//  Created by Pinkel Gurung on 3/27/13.
//  Copyright (c) 2013 Pinkel Gurung. All rights reserved.
//


#import "HTTPConnection.h"

@class MultipartFormDataParser;

@interface MyHTTPConnection : HTTPConnection  {
    MultipartFormDataParser *parser;
	NSFileHandle *storeFile;	
	NSMutableArray *uploadedFiles;
}
@end
