//
//  ShowBookmarkViewController.h
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/3/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BookMarkObject.h"
#import "EditBookmarkViewController.h"

@protocol ShowBookmarkViewControllerDelegate <NSObject>
-(void)addURLViewController:(id)controller didFinishEnteringURL:(NSString *)url;
@end

@interface ShowBookmarkViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UISearchBarDelegate, UISearchDisplayDelegate>
{
    UIBarButtonItem *doneButtonItem;
    NSMutableArray *bookmarksArray;
    NSMutableArray *searchArray;
    BookMarkObject *bookmark;
    UINavigationBar *naviBarObj;
    
    EditBookmarkViewController *editBookmarkViewController;
    UISearchBar *searchBar;
    UISearchDisplayController *searchDisplayController;
    
    NSString *searchText;
}

@property (retain,nonatomic) UIBarButtonItem *doneButtonItem;
@property (retain,nonatomic) BookMarkObject *bookmark;
@property (retain,nonatomic) UITableView *tableView;
@property (nonatomic, weak) id <ShowBookmarkViewControllerDelegate> delegate;

-(void)setBookmark:(NSString *)aName url:(NSURL *)aURL;
-(void)saveBookmarks;

@end
