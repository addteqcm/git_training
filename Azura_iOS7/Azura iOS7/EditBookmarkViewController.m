//
//  EditBookmarkViewController.m
//  Azura iOS7
//
//  Created by Nandini Sundara Raman on 9/3/13.
//  Copyright (c) 2013 addteq. All rights reserved.
//

#import "EditBookmarkViewController.h"

@interface EditBookmarkViewController ()
-(void) save;
@end

@implementation EditBookmarkViewController
@synthesize bookmark;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Load tableview
	self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, 320, self.view.frame.size.height) style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    // load navigation bar
    naviBarObj = [[UINavigationBar alloc] initWithFrame:CGRectMake(0,20,self.view.frame.size.width,44)];
    
    // Set Orientation adjustments for the bookmark tableview
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(UIInterfaceOrientationIsLandscape(orientation))
    {
        naviBarObj.frame = CGRectMake(0,20,self.view.frame.size.height,44);
        self.tableView.frame = CGRectMake(0, 60, self.view.frame.size.height,self.view.frame.size.width);
    }
    else
    {
        naviBarObj.frame = CGRectMake(0,20, self.view.frame.size.width, 44);
        self.tableView.frame = CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height);
    }
    
    // Add the table view to the view
    [self.view addSubview:self.tableView];
    [self.view insertSubview:naviBarObj aboveSubview:self.tableView];
    
    // Add bar button items to the navigation bar
    backButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(backButtonClicked)];
    
    UINavigationItem *navigItem = [[UINavigationItem alloc] init];
    navigItem.leftBarButtonItem = backButton;
    naviBarObj.items = [NSArray arrayWithObjects:navigItem, nil];
    
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(UIInterfaceOrientationIsLandscape(orientation))
    {
        naviBarObj.frame = CGRectMake(0, 20, self.view.frame.size.height, 44);
        self.tableView.frame = CGRectMake(0, 60, self.view.frame.size.height, self.view.frame.size.width);
    }
    else
    {
        naviBarObj.frame = CGRectMake(0, 20, self.view.frame.size.width, 44);
        self.tableView.frame = CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height);
    }
    naviBarObj.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.tableView];
    [self.view insertSubview:naviBarObj aboveSubview:self.tableView];
    [self.tableView reloadData];
    
    [nameTextField becomeFirstResponder];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation  duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:orientation duration:duration];
    CGRect frame = naviBarObj.frame;
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        frame.size.height=44;
    } else {
        frame.size.height=44;
    }
    naviBarObj.frame = frame;
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    // Force resignFirstResponder for edited textfield
    [self.view endEditing:YES];
    
    [self save];
    
    [super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void) backButtonClicked {
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)save
{
    [nameTextField resignFirstResponder];
    [bookmark setName:nameTextField.text];
    [bookmark setUrl:[NSURL URLWithString:urlTextField.text]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self setBookmark:nil];
    // Dispose of any resources that can be recreated.
}

#pragma -- table view delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
        {
            return 2;
            break;
        }
        default:
        {
            break;
        }
    }
    return 0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *UrlCellIdentifier = @"urlCell";
    static NSString *TextFieldCellIdentifier = @"textFieldCell";
    UITableViewCell *cell = nil;
    
    // Set up the cell...
    switch (indexPath.section)
    {
        case 0:
        {
            switch (indexPath.row)
            {
                case 0:
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:TextFieldCellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TextFieldCellIdentifier];
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                        
                        nameTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 10, 280, 30)];
                        nameTextField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
                        nameTextField.adjustsFontSizeToFitWidth = YES;
                        nameTextField.textColor = [UIColor colorWithRed:50.0/255 green:79.0/255 blue:133.0/255 alpha:1.0];
                        nameTextField.keyboardType = UIKeyboardTypeDefault;
                        nameTextField.returnKeyType = UIReturnKeyDone;
                        nameTextField.backgroundColor = [UIColor clearColor];
                        nameTextField.autocorrectionType = UITextAutocorrectionTypeNo; // no auto correction support
                        nameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone; // no auto capitalization support
                        nameTextField.textAlignment = NSTextAlignmentLeft;
                        nameTextField.delegate = self;
                        nameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
                        
                        [nameTextField setEnabled: YES];
                        
                        [cell addSubview:nameTextField];
                        
                        
                    }
                    nameTextField.text = bookmark.name;
                    break;
                }
                case 1:
                {
                    cell = [tableView dequeueReusableCellWithIdentifier:UrlCellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:UrlCellIdentifier];
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                        
                        urlTextField = [[UITextField alloc] initWithFrame:CGRectMake(20, 10, 280, 30)];
                        urlTextField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
                        urlTextField.adjustsFontSizeToFitWidth = YES;
                        urlTextField.minimumFontSize = 14.0;
                        urlTextField.font = [UIFont systemFontOfSize:17.0];
                        urlTextField.textColor = [UIColor colorWithRed:50.0/255 green:79.0/255 blue:133.0/255 alpha:1.0];
                        urlTextField.keyboardType = UIKeyboardTypeURL;
                        urlTextField.returnKeyType = UIReturnKeyDone;
                        urlTextField.backgroundColor = [UIColor clearColor];
                        urlTextField.autocorrectionType = UITextAutocorrectionTypeNo; // no auto correction support
                        urlTextField.autocapitalizationType = UITextAutocapitalizationTypeNone; // no auto capitalization support
                        urlTextField.textAlignment = NSTextAlignmentLeft;
                        urlTextField.delegate = self;
                        urlTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
                        [urlTextField setEnabled: YES];
                        
                        [cell addSubview:urlTextField];
                        
                        
                    }
                    urlTextField.text = [bookmark.url absoluteString];
                    break;
                }
                default:
                {
                    break;
                }
            }
            break;
        }
        default:
        {
            break;
        }
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    bookmark.name = nameTextField.text;
    bookmark.url = [NSURL URLWithString:urlTextField.text];
    
    [self.navigationController popViewControllerAnimated:YES];
    return NO;
}

@end
