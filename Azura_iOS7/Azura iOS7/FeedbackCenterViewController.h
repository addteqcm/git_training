//
//  FeedbackCenterViewController.h
//  Azura iOS7
//
//  Created by HeeJinChoi on 2/12/14.
//  Copyright (c) 2014 addteq. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AVFoundation/AVFoundation.h>

#import "FeedbackLeftPanelViewController.h"
#import "FeedbackRightViewController.h"
#import "FeedbackLeftTwoViewController.h"
#import "ScreenshotsViewController.h"
#import "AFNetworking.h"
#import "KeychainItemWrapper.h"
#import "UIBubbleTableView.h"
#import "MBProgressHUD.h"
#import "JMCRecorder.h"

@protocol FeedbackCenterViewControllerDelegate <NSObject>

@optional
- (void) movePanelLeft;
- (void) movePanelRight;
- (void) rotatePanel;
- (void) rotatePanelFromSearch;
- (void) goBackToProject;
- (void) expandPanel;
- (void) shrinkPanel;

@required
- (void)movePanelToOriginalPosition;

@end


@interface FeedbackCenterViewController : UIViewController <UIActionSheetDelegate, UITextFieldDelegate, UIBubbleTableViewDataSource,UIAlertViewDelegate, AVAudioRecorderDelegate, FeedbackLeftPanelViewControllerDelegate, FeedbackLeftTwoViewControllerDelegate, FeedbackRightViewControllerDelegate, ScreenshotsViewControllerDelegate>


@property (strong, nonatomic) NSMutableArray *projectsArray;
@property (strong, nonatomic) AFHTTPClient *client;

@property (strong, nonatomic) IBOutlet UILabel *feedbackTitle;


@property (strong, nonatomic) NSString *serverUrl;
@property (strong, nonatomic) KeychainItemWrapper *keychainItem;

@property (strong, nonatomic) NSUserDefaults *defaults;

@property (weak, nonatomic) id<FeedbackCenterViewControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIView *mainview;
@property (strong, nonatomic) IBOutlet UIView *centerview;

@property (strong, nonatomic) IBOutlet UITableView *maintableview;
@property (strong, nonatomic) IBOutlet UIView *mainsubview;

@property (strong, nonatomic) IBOutlet UIBubbleTableView *bubbleTable;
@property (strong, nonatomic) IBOutlet UITextField *textField;

@property (strong, nonatomic) UIActionSheet *actionSh;

@property (strong, nonatomic) NSMutableArray *bubbleData;
@property (strong, nonatomic) NSMutableArray *arrayOfComments;

@property (strong, nonatomic) AFXMLRequestOperation *operation;

@property (nonatomic) CGRect origin;
@property (nonatomic) CGRect textViewOrigin;
@property (nonatomic) int previousFlag;
@property (nonatomic) UIColor *color;
@property (nonatomic) int bottomInset;
@property (strong, nonatomic) MBProgressHUD *hud;

@property (strong, nonatomic) NSString *issuekey;
@property (strong, nonatomic) NSString *issuetitle;

@property (nonatomic) BOOL changeFlag;

@property (strong, nonatomic) NSString *user;

@property (strong, nonatomic) NSMutableArray *files;
@property (strong, nonatomic) NSMutableArray *fileCreated;
@property (strong, nonatomic) NSMutableArray *fileAttachedFlag;
@property (strong, nonatomic) NSMutableArray *imageFileNames;


@property (strong, nonatomic) JMCRecorder* recorder;
@property (strong, nonatomic) NSData *audiofile;
@property (nonatomic) NSInteger audioCount;



@property (strong, nonatomic) IBOutlet UIButton *leftButton;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;

@property (nonatomic) int feedbackStateInt;
@property (strong, nonatomic) NSString *feedbackWordsString;

@property (nonatomic) NSInteger selectedProject;
@property (nonatomic) NSInteger selectedIssueType;

@property (nonatomic, strong) NSMutableArray *screenshotList;


- (void) getComments:(NSString *)key;

- (void)handleTap:(UITapGestureRecognizer *)sender;
- (IBAction)moveLeftPanel:(id)sender;
- (IBAction)moveRightPanel:(id)sender;
- (IBAction)showActionsheet:(id)sender;
- (IBAction)sendFeedback:(id)sender;
- (IBAction)showHideRightPanel:(id)sender;
- (IBAction)showHideLeftPanel:(id)sender;

- (void) getProjects;

- (void) sendWithSummary:(NSString *) summaryMsg withDescription:(NSString *) msg withDate:(NSDateFormatter *) format;
@end
